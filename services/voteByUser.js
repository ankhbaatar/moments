const options = require('../helpers/mysqlConnection');

module.exports.findOne = async (where, req, next) => {
    const conn = await options.connection(req.auth.dbKey +'_public');
    try {
        const [rows, fields] = await conn.execute('SELECT * FROM `vote_byuser_k`'+ (where ? ' WHERE '+ where : ''));
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.findAll = async (req, res, next) => {
    const conn = await options.connection(req.auth.dbKey +'_public');
    try {
        const [rows, fields] = await conn.execute('SELECT * FROM `vote_byuser_k`');
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.insert = async (field, value, req) => {
    const conn = await options.connection(req.auth.dbKey +'_public');
    try {
        const [rows, fields] = await conn.execute('INSERT INTO `vote_byuser_k` ('+ field +') VALUES ('+ value +')');
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.update = async (set, where, req) => {
    const conn = await options.connection(req.auth.dbKey +'_public');
    try {
        const [rows, fields] = await conn.execute('UPDATE `vote_byuser_k` SET '+ set +' WHERE '+ where);
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.destroy = async (where, req, next) => {
    const conn = await options.connection(req.auth.dbKey +'_public');
    try {
        const [rows, fields] = await conn.execute('DELETE FROM `vote_byuser_k` WHERE '+ where);
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};