const options = require('../helpers/mysqlConnection');

module.exports.findOne = async (where = '') => {
    const conn = await options.connection('ablesoft');
    try {
        const [rows, fields] = await conn.execute('SELECT * FROM `notify_i`' + (where ? ' WHERE '+ where : ''));
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.findAll = async (req, res, next) => {
    const conn = await options.connection('ablesoft');
    try {
        const [rows, fields] = await conn.execute('SELECT * FROM `notify_i`');
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.insert = async (req, res, next) => {
   
};

module.exports.update = async (req, res, next) => {
    
};

module.exports.destroy = async (req, res, next) => {
    
};