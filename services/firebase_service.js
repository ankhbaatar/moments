const axios = require('axios');

module.exports.send = async (notify, ndata, tokens) => {
    const message = {
        registration_ids: tokens,
        priority: 'high',
        data: ndata,
        notification: notify,
    };

    axios({
        method: 'POST',
        url: process.env.FIREBASE_URL,
        data: message,
        headers: {
            'Content-Type': 'application/json',
            Authorization: `key=${process.env.FIREBASE_KEY}`,
        },
    });
};
