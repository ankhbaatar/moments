const axios = require('axios');
const moment = require('moment');
const config = require('../config/config');
const { encStr, getAgeFromRegister } = require('../helpers/globalFunction');
const cloudUser = require('./cloudUser');
const cloudHRWorker = require('./cloudHRWorker');
const cloudHRHuman = require('./cloudHRHuman');

module.exports.getUserInfo =async (userIds, accessToken, req) => {
    const { redis } =req.app.get('redis');
    const result =[];
    userIds =userIds.toString().split(',');
    for(let i=0; i <userIds.length; i++)  {
        let userInfo ={};
        if(userIds[i])  {
            let workerUserId =0;
            const redisWorkerInfo =await redis.get(`_able_worker_i-${userIds[i]}`);
            if(redisWorkerInfo)  {
                userInfo['id'] =redisWorkerInfo.id;
                userInfo['system_name'] =redisWorkerInfo.system_name;
                userInfo['app_name'] =redisWorkerInfo.app_name;
                userInfo['com_id'] =redisWorkerInfo.com_id;
                userInfo['dep_id'] =redisWorkerInfo.dep_id;
                workerUserId =redisWorkerInfo.user_id;
            }
            else  {
                const workerResult =await cloudHRWorker.findOne('`id`='+ userIds[i], req);
                userInfo['id'] =workerResult[0].id;
                userInfo['system_name'] =workerResult[0].system_name;
                userInfo['app_name'] =workerResult[0].app_name;
                userInfo['com_id'] =workerResult[0].com_id;
                userInfo['dep_id'] =workerResult[0].dep_id;
                workerUserId =(workerResult.length ? workerResult[0].user_id : 0);
                await redis.set(`_able_worker_i-${userIds[i]}`, workerResult[0] || null);
            }
            const redisUserInfo =await redis.get(`_user_i-${userIds[i]}`);
            const redisHumanInfo =await redis.get(`_human_i-${userIds[i]}`);

            if(redisUserInfo)  {
                const hasUserIcon = redisUserInfo && redisUserInfo.user_icon !== '';
                const userIcon = hasUserIcon ? redisUserInfo.user_icon : redisWorkerInfo.photo;
                const base64Str = hasUserIcon
                    ? encStr(`userIcon/m${userIcon}`)
                    : encStr(`human_resource/${userIcon}`);
                userInfo['user_icon'] =`${config.storageHost}/main.php?g=bG9ZhEltYWdl&path=${base64Str}&cloudDir=1&accessToken=${accessToken}`;
            }
            else  {
                const userResult =await cloudUser.findOne('`user_id`='+ workerUserId, req);
                const hasUserIcon = userResult[0] && userResult[0].user_icon !== '';
                const userIcon = hasUserIcon ? userResult[0].user_icon : redisWorkerInfo.photo;
                const base64Str = hasUserIcon
                    ? encStr(`userIcon/m${userIcon}`)
                    : encStr(`human_resource/${userIcon}`);
                userInfo['user_icon'] =`${config.storageHost}/main.php?g=bG9ZhEltYWdl&path=${base64Str}&cloudDir=1&accessToken=${accessToken}`;
                await redis.set(`_user_i-${userIds[i]}`, userResult[0] || null);
            }

            if(redisHumanInfo)  {
                userInfo['gender'] =redisHumanInfo.gender;
                userInfo['age'] =getAgeFromRegister(redisHumanInfo.reg_number);
            }
            else  {
                const humanResult =await cloudHRHuman.findOne('`id`='+ workerUserId, req);
                userInfo['gender'] =humanResult[0].gender;
                userInfo['age'] =getAgeFromRegister(humanResult[0].reg_number);

                await redis.set(`_human_i-${userIds[i]}`, humanResult[0] || null);
            }
        }
        else  {
            userInfo['id'] =0;
            userInfo['system_name'] ='Систем';
            userInfo['app_name'] ='Тодорхойгүй';
            userInfo['user_icon'] ='';
            userInfo['gender'] =null;
            userInfo['age'] =0;
        }
        result.push(userInfo);
    }
    // const result =await axios
    //                     .get(`${process.env.USER_API}/users/userInfo?userId=${userIds}`, {
    //                         headers: {
    //                             'Accept': 'application/json',
    //                             'x-access-token': `${accessToken}`
    //                         },
    //                         timeout: 1000
    //                     })
    //                     .then(response => {
    //                         return response.data
    //                     }).then(responseJson=>{
    //                         return responseJson.data;
    //                     })
    //                     .catch(async error => {
    //                         const errors = await redis.get('getPostError');
    //                         const logs = [];
    //                         if(errors)  {
    //                             const objectArray = Object.entries(errors);
    //                             objectArray.map(([key, value]) => {
    //                                 logs.push(value);
    //                             });
    //                         }
    //                         logs.push({myHID: req.auth.myHID, type:'userInfo', error: error, date: moment().format('YY/MM/DD : HH:mm')});

    //                         await redis.set('getPostError', logs);
    //                         console.error(error);
    //                     });
    if(userIds.length === 1)  return result[0]
    else  return result;
}