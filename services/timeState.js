const options = require('../helpers/mysqlConnection');

module.exports.findOne = async (where, req, next) => {
    const conn = await options.connection(req.auth.dbKey +'_time_access');
    try {
        const [rows, fields] = await conn.execute('SELECT * FROM `time_state_i`'+ (where ? ' WHERE '+ where : ''));
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.findAll = async (where, req, next) => {
    const conn = await options.connection(req.auth.dbKey +'_time_access');
    try {
        const [rows, fields] = await conn.execute('SELECT * FROM `time_state_i`'+ (where ? ' WHERE '+ where : ''));
        conn.end();
        return rows;
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.insert = async (field, value, req) => {
    const conn = await options.connection(req.auth.dbKey +'_time_access');
    try {
        const [rows, fields] = await conn.execute('INSERT INTO `time_state_i` ('+ field +') VALUES ('+ value +')');
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.update = async (set, where, req) => {
    const conn = await options.connection(req.auth.dbKey +'_time_access');
    try {
        const [rows, fields] = await conn.execute('UPDATE `time_state_i` SET '+ set +' WHERE '+ where);
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.destroy = async (where, req, next) => {
    const conn = await options.connection(req.auth.dbKey +'_time_access');
    try {
        const [rows, fields] = await conn.execute('DELETE FROM `time_state_i` WHERE '+ where);
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.count = async (where, req, next) => {
    const conn = await options.connection(req.auth.dbKey +'_time_access');
    try {
        const [rows, fields] = await conn.execute('SELECT COUNT(`id`) as count FROM `time_state_i`'+ (where ? ' WHERE '+ where : ''));
        conn.end();
        return rows[0].count;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};