const comment = require('./comment');
const plus = require('./plus');
const commentResponse = require('../api/responses/comment_response');

module.exports.getCommentInfo = async (commentResult, req) => {
    let comments =[];
    for(let i=0; i <commentResult.length; i++)  {
        var cmt =commentResult[i];
        const plusCnt =await plus.findAll('`item_id`='+ cmt.id +' AND `type`="comment"', req),
              cmtResResult =await commentResponse(cmt, req),
              replyCnt =await comment.count('`reply_id`='+ cmt.id +' AND `pro`="'+ cmt.pro +'" AND `module`="'+ cmt.module +'"', req);

        cmtResResult['totalReplyCnt'] =replyCnt;
        cmtResResult['plusCnt'] =plusCnt.length;
        cmtResResult['hasMe'] =false;
        for(let j =0; j <plusCnt.length; j++)  {
            if(plusCnt[j].user_id == req.auth.myHID)  {
                cmtResResult['hasMe'] =true;
                break;
            }
        };
        comments.push(cmtResResult);
    }
    return comments;
};