const options = require('../helpers/mysqlConnection');

module.exports.findOne = async (req, res, next) => {};

module.exports.findAll = async (req, res, next) => {
    const conn = await options.connection('ablesoft');
    try {
        const [rows, fields] = await conn.execute('SELECT * FROM `notify_byuser_k`');
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.insert = async (req, res, next) => {
   
};

module.exports.update = async (req, res, next) => {
    
};

module.exports.destroy = async (req, res, next) => {
    
};

module.exports.count = async (where, req, next) => {
    const conn = await options.connection(req.auth.dbKey);
    try {
        const [rows, fields] = await conn.execute('SELECT COUNT(`id`) as count FROM `notify_byuser_k`'+ (where ? ' WHERE '+ where : ''));
        conn.end();
        return rows[0].count;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};