const options = require('../helpers/mysqlConnection');

module.exports.findOne = async (where, req, next) => {
    const conn = await options.connection(req.auth.dbKey);
    try {
        const [rows, fields] = await conn.execute('SELECT * FROM `new_items_k`'+ (where ? ' WHERE '+ where : ''));
        conn.end();
        return rows;
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.findAll = async (where, req, next) => {
    const conn = await options.connection(req.auth.dbKey);
    try {
        const [rows, fields] = await conn.execute('SELECT * FROM `new_items_k`'+ (where ? ' WHERE '+ where : ''));
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.insert = async (field, value, req) => {
    const conn = await options.connection(req.auth.dbKey);
    try {
        const [rows, fields] = await conn.execute('INSERT INTO `new_items_k` ('+ field +') VALUES ('+ value +')');
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.update = async (set, where, req) => {
    const conn = await options.connection(req.auth.dbKey);
    try {
        const [rows, fields] = await conn.execute('UPDATE `new_items_k` SET '+ set +' WHERE '+ where);
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.destroy = async (where, req, next) => {
    const conn = await options.connection(req.auth.dbKey);
    try {
        const [rows, fields] = await conn.execute('DELETE FROM `new_items_k` WHERE '+ where);
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.count = async (where, req, next) => {
    const conn = await options.connection(req.auth.dbKey);
    try {
        const [rows, fields] = await conn.execute('SELECT COUNT(`pro`) as count FROM `new_items_k`'+ (where ? ' WHERE '+ where : ''));
        conn.end();
        return rows[0].count;   
    } catch (error) {
        console.log('db', req.auth.dbKey);
        console.log(error);
        conn.end();
        return null;
    }
};