const { getUserInfo } =require('../services/getUserInfo');
const voteAnswer = require('./voteAnswer');
const voteByUser = require('./voteByUser');

module.exports.getAnswerInfo = async (itemId, isBasic, req) => {
    const answers =[];
    const voteAnswerResult =await voteAnswer.findOne('`mid` ='+ itemId +' ORDER BY `id`', req);
    for(let j =0; j <voteAnswerResult.length; j++)  {
        let hasMe =false;
        const answer =voteAnswerResult[j],
            users =[];
        const localVote =await voteByUser.findOne('`answer_id`='+ answer.id +' AND `vote_id`='+ itemId +' ORDER BY `date` ASC', req);
        let start =0,
            end =localVote.length;
        if(end >5)  start =localVote.length -5;
        for(let k =start; k <end; k++)  {
            let answerUser =await getUserInfo(localVote[k].user_id, req.auth.accessToken, req);
            users.push({
                path: answerUser.user_icon,
                gender: (answerUser.gender ? 'male' : 'female'),
                age: answerUser.age,
                overHint: answerUser.system_name
            });
        }

        for(let x =0; x <localVote.length; x++)  {
            if(!hasMe && localVote[x].user_id == req.auth.myHID) hasMe =true;
        }
        let answerInfo ={
            Id: answer.id,
            voteCnt: localVote.length,
            hasMe: hasMe
        };
        if(localVote.length)  {
            answerInfo['userCnt'] =localVote.length;
            answerInfo['users'] =users;
        }
        if(isBasic)  answerInfo['value'] =answer.body;
        answers.push(answerInfo);
    }

    return answers;
};