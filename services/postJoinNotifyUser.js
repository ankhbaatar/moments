const options = require('../helpers/mysqlConnection');

module.exports.findOne = async (req, res, next) => {};

module.exports.findAll = async (req, res, next) => {
    const conn = await options.connection(req.auth.dbKey);
    try {
        const table = req.query.table || 'notify_byuser_k';
        if(req.query.limit)  var limit = parseInt(req.query.limit) *3;
        if(req.query.notIn)  var notIn = '`id` NOT IN ('+ req.query.notIn.join(',') +') AND';
        if(req.query.pro)  var proWhere = 'p.`pro`="'+ req.query.pro +'" AND';
        if(req.query.mod)  var modWhere = 'p.`module`="'+ req.query.mod +'" AND';
        const [rows, fields] = await conn.execute('SELECT *, p.`id` AS postId FROM `'+ req.query.table +'` AS nu INNER JOIN `postByNotify` AS pn ON nu.`notify_id` =pn.`notifyId` INNER JOIN `posts` AS p ON pn.`postId` = p.`id` WHERE '+ (req.query.notIn ? notIn : '') +' '+ (req.query.pro ? proWhere : '') +' '+ (req.query.mod ? modWhere : '') +' (nu.`user_id` = '+ req.auth.myHID +' OR p.`authorId` = '+ req.auth.myHID +') GROUP BY p.`id` ORDER BY p.`updatedAt` DESC LIMIT '+ limit +',3');
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.findAllCnt = async (req, table, next) => {
    const conn = await options.connection(req.auth.dbKey);
    try {
        if(req.query.pro)  var proWhere = 'p.`pro`="'+ req.query.pro +'" AND';
        if(req.query.mod)  var modWhere = 'p.`module`="'+ req.query.mod +'" AND';
        const [rows, fields] = await conn.execute('SELECT *, p.`id` AS postId FROM `'+ table +'` AS nu INNER JOIN `postByNotify` AS pn ON nu.`notify_id` =pn.`notifyId` INNER JOIN `posts` AS p ON pn.`postId` = p.`id` WHERE '+ (req.query.pro ? proWhere : '') +' '+ (req.query.mod ? modWhere : '') +' (nu.`user_id` = '+ req.auth.myHID +' OR p.`authorId` = '+ req.auth.myHID +') GROUP BY p.`id` ORDER BY p.`updatedAt` DESC');
        conn.end();
        return rows.length;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.insert = async (req, res, next) => {
   
};

module.exports.update = async (req, res, next) => {
    
};

module.exports.destroy = async (req, res, next) => {
    
};