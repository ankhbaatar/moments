const moment = require('moment');
const options = require('../helpers/mysqlConnection');

module.exports.findOne = async (where, dbKey, next) => {
    const conn = await options.connection(dbKey);
    try {
        const [rows, fields] = await conn.execute('SELECT * FROM `posts`'+ (where ? ' WHERE '+ where : ''));
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.findAll = async (dbKey, res, next) => {
    const conn = await options.connection(dbKey);
    try {
        const [rows, fields] = await conn.execute('SELECT * FROM `posts`');
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.insert = async (field, value, dbKey) => {
    const conn = await options.connection(dbKey);
    try {
        let now =moment().format('YYYY-MM-DD HH:mm:ss');
        const [rows, fields] = await conn.execute('INSERT INTO `posts` ('+ field +',`createdAt`,`updatedAt`) VALUES ('+ value +', "'+ now +'", "'+ now +'")');
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.update = async (set, where, dbKey) => {
    const conn = await options.connection(dbKey);
    try {
        set += (set !== '' ? ',' : '') +'`updatedAt`="'+ moment().format('YYYY-MM-DD HH:mm:ss') +'"';
        const [rows, fields] = await conn.execute('UPDATE `posts` SET '+ set +' WHERE '+ where);
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.destroy = async (where, dbKey, next) => {
    const conn = await options.connection(dbKey);
    try {
        const [rows, fields] = await conn.execute('DELETE FROM `posts` WHERE '+ where);
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.count = async (where, dbKey, next) => {
    const conn = await options.connection(dbKey);
    try {
        const [rows, fields] = await conn.execute('SELECT COUNT(`id`) as count FROM `posts`'+ (where ? ' WHERE '+ where : ''));
        conn.end();
        return rows[0].count;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};