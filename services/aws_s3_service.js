const fs = require('fs');
const aws = require('aws-sdk');

const ID = process.env.S3_BUCKET_ID;
const SECRET = process.env.S3_BUCKET_SECRET;
const BUCKET_NAME = process.env.S3_BUCKET_NAME;
// const SESSION_TOKEN = 'dzeko';

const s3bucket = new aws.S3();

aws.config.update({
    accessKeyId: ID,
    secretAccessKey: SECRET,
    region: 'ap-northeast-2',
});

module.exports.upload = async (folder = 'questions', file) => {
    // const body = fs.createReadStream(file.path);

    const params = {
        ACL: 'public-read',
        ContentType: file.contentType,
        Bucket: BUCKET_NAME,
        Key: `${folder}/${Date.now()}_${file.originalname}`, // file name you want to save as
        Body: file.buffer,
    };

    return s3bucket
        .upload(params)
        .promise()
        .then(data => {
            return data.Location;
        })
        .catch(err => {
            console.log(err);
            throw err;
        });
};

// module.exports.remove = async name => {
//     const params = {
//         Bucket: BUCKET_NAME,
//         Delete: {
//             Objects: [{ Key: 'dribbble_artboard.png' }],
//         },
//     };

//     return s3
//         .deleteObjects(params)
//         .promise()
//         .then(data => {
//             return data.Location;
//         })
//         .catch(err => {
//             throw err;
//         });
// };

// const fileFilter = (req, file, cb) => {
//     if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
//         cb(null, true);
//     } else {
//         cb(new Error("Invalid file type, only JPEG and PNG is allowed!"), false);
//     }
// };
//
// const upload = multer({
//     fileFilter,
//     storage: multerS3({
//         acl: "public-read",
//         s3,
//         bucket: BUCKET_NAME,
//         contentType: multerS3.AUTO_CONTENT_TYPE,
//         metadata: (req, file, cb) => {
//             cb(null, { fieldName: file.fieldname });
//         },
//         key: (req, file, cb) => {
//             cb(null, Date.now().toString());
//         }
//     })
// });
