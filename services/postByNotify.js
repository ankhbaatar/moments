const options = require('../helpers/mysqlConnection');

module.exports.findOne = async (where, dbKey, next) => {
    const conn = await options.connection(dbKey);
    try {
        const [rows, fields] = await conn.execute('SELECT * FROM `postByNotify`'+ (where ? ' WHERE '+ where : ''));
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.findAll = async (where, dbKey, next) => {
    const conn = await options.connection(dbKey);
    try {
        const [rows, fields] = await conn.execute('SELECT * FROM `postByNotify`'+ (where ? ' WHERE '+ where : ''));
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.insert = async (field, value, dbKey) => {
    const conn = await options.connection(dbKey);
    try {
        const [rows, fields] = await conn.execute('INSERT INTO `postByNotify` ('+ field +') VALUES ('+ value +')');
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.update = async (set, where, dbKey) => {
    const conn = await options.connection(dbKey);
    try {
        const [rows, fields] = await conn.execute('UPDATE `postByNotify` SET '+ set +' WHERE '+ where);
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.destroy = async (where, dbKey, next) => {
    const conn = await options.connection(dbKey);
    try {
        const [rows, fields] = await conn.execute('DELETE FROM `postByNotify` WHERE '+ where);
        conn.end();
        return rows;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};

module.exports.count = async (where, dbKey, next) => {
    const conn = await options.connection(dbKey);
    try {
        const [rows, fields] = await conn.execute('SELECT COUNT(`id`) as count FROM `postByNotify`'+ (where ? ' WHERE '+ where : ''));
        conn.end();
        return rows[0].count;   
    } catch (error) {
        console.log(error);
        conn.end();
        return null;
    }
};