module.exports = {
    extends: ['airbnb-base', 'plugin:prettier/recommended'],
    plugins: ['prettier'],
    root: true,
    rules: {
        'prettier/prettier': 'warn',
        'no-unused-vars': 'warn',
        'consistent-return': 'off',
        'no-underscore-dangle': 'off',
        'no-plusplus': 'off',
        'no-await-in-loop': 'off',
        eqeqeq: [2, 'allow-null'],
    },
    overrides: [
        {
            files: ['tests/*.js', 'seeders/*.js'],
            rules: {
                'no-undef': 'off',
            },
        },
    ],
};
