const Mongoose = require('mongoose');
const mongoose = require('./mongoose');

const NAME = 'seenUsers';

const factorialSchema = new Mongoose.Schema(
    {
        _id: { type: Mongoose.Schema.ObjectId },
        userId: { type: Number },
        comId: { type: Number },
        items: [
            {
                id: { type: String },
                pro: { type: String },
                mod: { type: String },
                date: { type: Date }
            }
        ],
    },
    {
        timestamps: true,
    }
);

module.exports = {
    name: NAME,
    model: mongoose.model(NAME, factorialSchema),
};
