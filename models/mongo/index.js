/* eslint-disable global-require */
/* eslint-disable import/no-dynamic-require */
/* eslint-disable no-console */
const fs = require('fs');
const path = require('path');

const basename = path.basename(__filename);
const db = {};
fs.readdirSync(__dirname)
    .filter(file => {
        return file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js';
    })
    .forEach(file => {
        if (file === 'mongoose.js') return;
        const result = require(path.join(__dirname, file));
        db[result.name] = result;
    });

module.exports = db;
