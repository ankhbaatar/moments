const Mongoose = require('mongoose');
const mongoose = require('./mongoose');

const NAME = 'postByUser';

const factorialSchema = new Mongoose.Schema(
    {
        _id: { type: Mongoose.Schema.ObjectId },
        userId: { type: Number },
        comId: { type: Number },
        postId: { type: Number },
        lastAction: {
            type: {
                actionKey: String,
                authorId : Number,
                actionId: Number,
                actionItemId: Number
            },
            default: null
        }
    },
    {
        timestamps: true,
    }
);

module.exports = {
    name: NAME,
    model: mongoose.model(NAME, factorialSchema),
};
