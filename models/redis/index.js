/* eslint-disable camelcase */
const promiseFactory = require('q').Promise;
const redis = require('promise-redis')(promiseFactory);

const { unserialize } = require('php-unserialize');
const { serialize } = require('php-serialize');

module.exports = config => {
    const client = redis.createClient(config);
    client.on('connect', () => {
        console.log(`REDIS CONNECT SUCCESSFUL. ${new Date().toString()}`);
    });
    client.on('error', err => {
        console.log(`REDIS CONNECTION ERROR: ${err}. ${new Date().toString()}`);
    });
    client.on('end', () => {
        console.log(`REDIS CONNECTION CLOSED.${new Date().toString()}`);
    });

    // redis.get funciton override
    (getFn => {
        client.get = (key, is_check_expire) => {
            return getFn
                .call(client, key)
                .then(res => {
                    if (!res) return null;
                    res = unserialize(res);
                    if (is_check_expire) {
                        return res.expired_time > time() ? res.value : null;
                    }
                    return res.value;
                })
                .catch(err => {
                    console.log(err);
                });
        };
    })(client.get);

    // redis.set funciton override
    (setFn => {
        client.set = (key, val, exp) => {
            const t = time();
            exp = exp || 86400;
            val = JSON.stringify(val);
            val = JSON.parse(val);

            val = serialize({
                value: val,
                write_time: t,
                expired_in: exp,
                expired_time: t + exp,
            });
            return setFn.call(client, key, val);
        };
    })(client.set);

    function time() {
        return Math.floor(Date.now() / 1000);
    }
    return client;
};
