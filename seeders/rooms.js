const { ObjectId } = require('mongodb');
const clc = require('cli-color');

const errorLog = clc.red.bold;
const successLog = clc.green;

const datas = [
    {
        _id: new ObjectId(),
        name: "Э.Содбилэг, Д.Цолмон",
        path: "",
        lastFrom: 1004497,
        lastMsg: "a9 cb ",
        lastMsgType: 0,
        authorId: 1004497,
        date: 1613579800614,
        isOnline: false,
        isHide: false,
        hasNew: true,
        isDelete: false,
        isDeletedDate: { type: Date },
        deleteMsgIds: { type: Array },
        msgs: [
            {
                _id:new ObjectId(),
                from : 1004497,
                msg : "a7 c3 d2 c6 ",
                date :'2021-02-17T16:00:30.855+00:00',
                msgType : 0,
            },
            {
                _id:new ObjectId(),
                from : 1004497,
                msg : "a9 cb ",
                date :'2021-02-17T16:36:40.617+00:00',
                msgType : 0
            },
        ],
        users: [
            {
                _id:new ObjectId(),
                id:1004497,
                name:"Э.Содбилэг",
                last_seen:1613577630854,
                isOnline : false,
                isAdmin:false,
            },
            {
                _id:new ObjectId(),
                id: 1000024,
                name:'Д.Цолмон',
                last_seen: 1613585147400,
                isOnline: true,
                isAdmin: true,
            },
        ]
    },
];

module.exports.create = model => {
    model
        .insertMany(datas)
        .then(() => {
            console.log(successLog('users data inserted')); // Success
        })
        .catch(e => {
            console.log(errorLog(e)); // Failure
        });
};
