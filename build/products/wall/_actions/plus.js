

setModuleAction(function plus(data)  {
		var body ={
				pro: data.pro,
				mod: data.mod,
				itemId: data.itemId,
				type: data.type
			},
			method =(data.add ? 'POST' : 'DELETE');

		if(data.type =='post')  body['authorId'] =data.authorId;

		fetchRequest(ApiServer +'/plus',  {
			method: method,
			body: JSON.stringify(body),
			cookies: ['accessToken'],
			onResponse: onResult
		});

		function onResult(res)  {
			if(method =='POST' && data.type =='post')  {
				res.data.notify['typeModule'] ='sendNotify';
				var notify =[res.data.notify];
				let origin =window.location.ancestorOrigins[0];
				window.parent.postMessage(notify, origin);
			};
			var isObject =typeof res.data.length =='object';
			var response =isObject ? 0 : res.data.length;
			data.setCount(response);
		};
});
