

setModuleAction(function getData(page, searchData, tableName)  {
		var WallLayout =adom("#WallLayout:0"),
			Posts =WallLayout.select("PostBox", 1),
			NoPost =WallLayout.select("NoPost:0", 1),
			loading =WallLayout.select(".loading", 1),
			limit =page ? page: 0;

		var oldRootData =getModuleData('root');

		if(!page && Posts.length >0 && (oldRootData && !oldRootData.isBackUp))  Posts.remove();
		if(NoPost.exist())  NoPost.remove();

		if(!loading.length)  WallLayout.append('<div class="loading">', ["Ачаалж байна ..."]);
		setModuleData('root', {
			isLoading: true
		});

		var searchWhere ='';
		if(searchData)  {
			if(searchData.pro)  searchWhere +='&pro='+ searchData.pro;
			if(searchData.mod)  searchWhere +='&mod='+ searchData.mod;
		};

		if(!tableName) 	tableName ='notify_byuser_k';
		if(oldRootData && oldRootData.table)  tableName =oldRootData.table;
		

		fetchRequest(ApiServer +'/post?table='+ tableName +'&limit='+ limit + searchWhere,  {
			method: "GET",
			cookies: ['accessToken'],
			onResponse: dispachData
		});

		function dispachData(res)  {
			WallLayout.select(".loading", 1).remove();
			let postData =[],
				notIn =[];

			res.data.datas.each(function(row)  {
				var post = {
					isExtended: row.isExtended,
					pro: row.pro,
					encPro: row.encPro,
					module: row.module,
					encModule: row.encModule,
					itemId: row.itemId,
					title: row.title,
					description: row.description,
					labels: row.groupName,
					authorId: row.authorInfo.id,
					authorName: row.authorInfo.system_name,
					authorApp: row.authorInfo.app_name,
					authorComName: row.authorInfo.company_name,
					authorIcon: {path: row.authorInfo.user_icon, gender: (row.authorInfo.gender ? 'male' : 'female'), age: row.authorInfo.age},
					date: row.easyDate,
					fullDate: row.fullDate,
					images: row.images,
					imageCnt: row.imageCnt,
					files: row.files,
					actionBar: row.actionBar,
					comments: row.comments,
					holder: row.holder,
					plusCnt: row.plusCnt,
					commentTotalCnt: row.commentCnt,
					totalReplyCnt: row.commentRootCnt,
					upReplyCnt: row.upReplyCnt,
					seen: row.seen
				};

				if(row.voteInfo)  post['poll'] =row.voteInfo;
				if(row.hasMe)  post['hasMe'] =row.hasMe;
				if(row.targetId)  post['targetId'] =row.targetId;
				if(row.replyAll)  post['replyAll'] =row.replyAll;
				if(row.mailUsers)  post['mailUsers'] =row.mailUsers;
				if(row.docsHolder)  post['docsHolder'] =row.docsHolder;
				if(row.autoNumber)  post['autoNumber'] =row.autoNumber;
				if(row.senderName)  post['senderName'] =row.senderName;
				if(row.shiftInfo)  post['shiftInfo'] =row.shiftInfo;
				if(row.shiftCnt)  post['shiftCnt'] =row.shiftCnt;
				if(row.isAnswer)  post['isAnswer'] =row.isAnswer;
				if(row.pastDay)  post['pastDay'] =row.pastDay;
				if(row.restDay)  post['restDay'] =row.restDay;
				if(row.answerDate)  post['answerDate'] =row.answerDate;
				if(row.week)  post['week'] =row.week;
				if(row.sharedUser)  post['sharedUser'] =row.sharedUser;
				if(row.sharedUserCnt)  post['sharedUserCnt'] =row.sharedUserCnt;
				if(row.timeRequestType)  post['timeRequestType'] =row.timeRequestType;
				if(row.requestDate)  post['requestDate'] =row.requestDate;
				if(row.requestStatus)  post['requestStatus'] =row.requestStatus;
				if(row.requestDays)  post['requestDays'] =row.requestDays;
				if(row.reqSTime)  post['reqSTime'] =row.reqSTime;
				if(row.reqETime)  post['reqETime'] =row.reqETime;
				postData.push(post);
				notIn.push(row.postId);
			});

			var rootData ={
				page: res.data.page >0 ? res.data.page : 0,
				isLoading: false,
				theEnd: res.data.theEnd,
				notIn: notIn,
				table: res.data.table,
				isBackUp: res.data.isBackUp
			};

			if(searchData)  rootData['searchData'] =searchData;

			setModuleData('root', rootData);

			execAction('root/wall', 'drawPosts', [postData]);
		};
});
