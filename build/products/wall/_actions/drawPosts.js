

setModuleAction(function drawPosts(posts)  {
        var WallLayout =adom('#WallLayout:0');

        var rootData =getModuleData('root');

        var staticPost1 = {
            itemId: 0,
            isExtended: false,
            pro: 'public',
            module: 'news',
            actionTitle: "Мэдээ нэмлээ",
            title: 'Эйблийг гар утаснаасаа ...',
            description: '<b style="color: #fd3237;">Able</b> шинэ оныг тохиолдуулан Able Moments, Able Chat гар утасны аппликейшнуудын туршилтын хувилбарыг хэрэглээнд гаргалаа.<br/><br/><b style="color: #e92ebf;">Able Moments application</b> нь Эйбл системийн өнцөг булан бүрт хийгдэж буй үйл явдлыг нэг цэгт төвлөрүүлэн гар утаснаасаа цаг алдалгүй шийдвэрлэх, удирдах боломжийг танд олгоно.<br/><br/><b style="color: #fd3237;">Able Chat application</b> нь чатын үндсэн үйлдлүүдээс гадна илгээсэн чаттай танилцсан хэрэглэгчдийг жагсаалтаар харах, хонх цохих, хавсралтууд цэснээс файл, зураг, линк, шуурхай мессежийг ялган харах, түүнээс хайлт хийх зэрэг онцлог үйлдлүүдтэй.<br/><br/><b style="color: #e92ebf;">Шинэ оны мэнд хүргэе!</b>',
            images: [{itemId: 1, path: FrontEndPath +"/_layouts/Root/apps.jpg?2"}],
            imageCnt: 1,
            labels: ['Able', 'Working online'],
            authorId: 0,
            authorName: 'Able',
            authorApp: 'Working online',
            authorComName: 'Эйблсофт',
            authorIcon: '',
            commentTotalCnt:0,
            totalReplyCnt: 0,
            plusCnt: 0,
            date: "30 мин",
            actionFullDate: "21/06/18 - 18:36",
            fullDate: "21/06/18 - 18:36",
            readOnly: true
        };

        // var staticPost2 ={
        //     itemId: 0,
        //     isExtended: false,
        //     pro: 'public',
        //     module: 'news',
        //     actionTitle: "Мэдээ нэмлээ",
        //     title: 'Able Chat Beta аппликэйшн',
        //     description: '<b style="color: fd3237">Able Chat</b> аппликэйшн ашигласнаар файл, зураг, линк, аудио чат илгээх, илгээсэн чаттай танилцсан хэрэглэгчдийг жагсаалтаар харах, өрөө үүсгэн чатлах, дэмжих, хонхоор дохио өгөх, чатад хариулах, чатыг дамжуулах, устгах зэрэг үйлдлүүдийг хийж, хавсралтууд цэснээс хайлт хийж мэдээллээ олох боломжтой.<br/><br/><b style="color: #e92ebf">Шинэ оны мэнд хүргэе!</b>',
        //     images: [{itemId: 1, path: FrontEndPath +"/_layouts/Root/chatApp.jpg"}],
        //     imageCnt: 1,
        //     labels: ['Able', 'Working online'],
        //     authorId: 0,
        //     authorName: 'Able',
        //     authorApp: 'Working online',
        //     authorComName: 'Эйблсофт',
        //     authorIcon: '',
        //     commentTotalCnt:0,
        //     totalReplyCnt: 0,
        //     plusCnt: 0,
        //     date: "30 мин",
        //     actionFullDate: "21/06/18 - 18:36",
        //     fullDate: "21/06/18 - 18:36",
        //     readOnly: true
        // };

        // var staticPost2 ={
        //     itemId: 0,
        //     isExtended: false,
        //     pro: 'public',
        //     module: 'news',
        //     actionTitle: "Мэдээ нэмлээ",
        //     title: '<b style="color: #005ea1;">Able Alerts</b> - ийг танилцуулж байна',
        //     description: '<b style="color: #f54747;">Able</b> <b>11</b> жилийн ойгоо тохиолдуулан шинэ бүтээгдэхүүнээр бэлэг барьж байна.<br/><br/><b style="color: #cc66cc;">Able</b> - ийн хаанаас ч та ажлаа сануулна...<br/><br/>Системд төлөвлөсөн чухал ажил, мэдээллээ мессеж, чат, дуудлага, поп ап, и-мэйл зэрэг функцээс сонгон өөртөө болон бусдад сануулах боломжтой. Ашиглах зааврыг <a target="_blank" href="https://web.able.mn/help/Alerts">эндээс</a> харна уу!<br/><br/><b style="color: #ff6600;">Able</b> гэрээ байгуулан хамтран ажиллаж байгаа нийт байгууллагууддаа энэ оныг дуустал 100 мессеж + 20 дуудлагын санамж үүсгэх эрх бэлэглэж байна.',
        //     images: [{itemId: 1, path: FrontEndPath +"/_layouts/Root/alertPost.jpg?3"}],
        //     imageCnt: 1,
        //     labels: ['Able', 'Working online'],
        //     authorId: 0,
        //     authorName: 'Able',
        //     authorApp: 'Working online',
        //     authorComName: 'Эйблсофт',
        //     authorIcon: '',
        //     commentTotalCnt:0,
        //     totalReplyCnt: 0,
        //     plusCnt: 0,
        //     date: "30 мин",
        //     actionFullDate: "21/06/18 - 18:36",
        //     fullDate: "21/06/18 - 18:36",
        //     readOnly: true
        // };

        var staticPosts =[];

        var originUrl =window.location.ancestorOrigins[0];

        // staticPosts.push(staticPost1);

        if(rootData && rootData.hasOwnProperty('searchData'))  staticPosts =[];

        if(!posts.length && !staticPosts.length)  {
			WallLayout.append('<NoPost>');
			return;
		};

        if(staticPosts.length && rootData && rootData.page ==1)  {
            staticPosts.each(function(sPost)  {
                WallLayout.append('<PostBox>', sPost);
            });
        };

        posts.each(function(post)  {
			var onTest =false;
			// post.description ="Сайн байцгаана уу?-≤≤-div-≥≥--»-Мэдээллийн технологийн эко системийн багийн-»- шинэ жилд Эйбл Софт ХХК-ий багт оролцох тул 2021 оны 12 сарын 26-ны өдрийн ажлын төлөвлөгөөндөө оруулна уу.-≤≤-/div-≥≥--≤≤-div-≥≥--≤≤-br-≥≥--≤≤-/div-≥≥--≤≤-div-≥≥--≤≤-br-≥≥--≤≤-/div-≥≥-";
			// post.description ="-≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥--≤≤-b-≥≥-Энхжин:-Ω--≤≤-/b-≥≥--≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥-1. Moments апп дээр шинэ дизайны угсралт,бэлдэц эхэлсэн-Ω--≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥--≤≤-br-≥≥--≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥--≤≤-b-≥≥-Цогтбаяр:-Ω--≤≤-/b-≥≥--≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥-1. Норматив удааширалыг шийдэх ажил хийгдсэн-≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥--≤≤-br-≥≥--≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥--≤≤-b-≥≥-Тэмүүлэн:-≤≤-/b-≥≥--Ω--≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥-1. Хяналт Шинжилгээ үнэлгээний - Home цонхыг сайжруулсан.-≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥-2. Ablemail -ийн box-г боловсруулаад хөгжүүлэлтийн-Ω-Орчин бэлдэж хөгжүүлэлтийн тест -г эхлүүлсэн.-Ω--≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥-3. Момэнтс -ийн api хуудас тусд нь гаргах ажил дээр нэг өдөр суусан. Нормативийн алдаа засвар болон loadnext болгох ажлуудыг-≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥--≤≤-br-≥≥--≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥--≤≤-b-≥≥-Содбилэг:-Ω--≤≤-/b-≥≥--≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥-1. Вэб чатын дизайны ажил-Ω-дээр суусан.-≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥-2.-Ω-Алдаа үйлчилгээн дээр туслалцаа зөвлөгөө өгсөн.-≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥-3. -≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-span style=-»-font-family: Arial; font-size: 12px;-»--≥≥-SmS manager-≤≤-/span-≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥--Ω-бокс дээр-Ω-зөвлөгчөөр-≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-span style=-»-font-size: 12px; font-family: Arial;-»--≥≥--Ω-ажилсан.-≤≤-/span-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥--≤≤-br-≥≥--≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥--≤≤-b-≥≥-Тунгалаг:-Ω--≤≤-/b-≥≥--≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥-1. Тоон гарын үсэг хаагдсан.-Ω--≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥-2. Хяналт шинжилгээ үнэлгээ боксын ажил эхлүүлсэн-≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥--≤≤-br-≥≥--≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥--≤≤-b-≥≥-Баянзул:-Ω--≤≤-/b-≥≥--≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥-1. 12 сарын 13-наас 12 сарын 17 хүртэл SmS manager боксоо хаасан.-Ω--≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥-2. Moments-ыг app хувилбарт оруулах бокс руугаа ороод эхний шатандаа явж байгаа.-≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥--≤≤-br-≥≥--≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥--≤≤-b-≥≥-Батболд:-Ω--≤≤-/b-≥≥--≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥-1. Гадаад өргөдөл гомдлын боксыг дуусгасан тестээ эхэлсэн.-≤≤-/span-≥≥--≤≤-br-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»-font-family: Arial; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal;-»--≥≥--≤≤-br-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥--≤≤-b-≥≥-Мөнх-очир:-≤≤-/b-≥≥--≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-br-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 12px;-»--≥≥-1.-Ω--≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-span style=-»-background-color: rgb(229, 233, 236);-»--≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 13px;-»--≥≥-AbleChat апп-н-Ω-алдаа-Ω--≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/span-≥≥--≤≤-font face=-»-Arial-»--≥≥--≤≤-span style=-»-font-size: 13px;-»--≥≥-засвар-≤≤-/span-≥≥--≤≤-/font-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-span style=-»-background-color: rgb(229, 233, 236); font-family: Arial; font-size: 13px;-»--≥≥-2.-Ω--≤≤-/span-≥≥--≤≤-span style=-»-background-color: rgb(229, 233, 236); font-family: Arial; font-size: 13px;-»--≥≥-AbleChat апп-н-≤≤-/span-≥≥--≤≤-span style=-»-background-color: rgb(229, 233, 236); font-family: Arial; font-size: 13px;-»--≥≥--Ω-дизайны box дээр ажилласан-≤≤-/span-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-span style=-»-background-color: rgb(229, 233, 236); font-family: Arial; font-size: 13px;-»--≥≥--≤≤-br-≥≥--≤≤-/span-≥≥--≤≤-/div-≥≥--≤≤-div style=-»--»--≥≥--≤≤-span style=-»-background-color: rgb(229, 233, 236); font-family: Arial; font-size: 13px;-»--≥≥--≤≤-div-≥≥--≤≤-b-≥≥-Анхбаатар:-Ω--≤≤-/b-≥≥--≤≤-/div-≥≥--≤≤-div-≥≥-1. Чаттай холбоотой алдаа үйлчилгээн дээр ажилсан.-≤≤-/div-≥≥--≤≤-/span-≥≥--≤≤-span style=-»-background-color: rgb(229, 233, 236); font-family: Arial; font-size: 13px;-»--≥≥--≤≤-div-≥≥-2. Чат апп-н шинээр өрөө үүсгэх ажилыг дуусгасан-≤≤-/div-≥≥--≤≤-/span-≥≥--≤≤-span style=-»-background-color: rgb(229, 233, 236); font-family: Arial; font-size: 13px;-»--≥≥--≤≤-div-≥≥-3. Шинэ чат нээсэн газрын хандалтын тоог гаргасан.-≤≤-/div-≥≥--≤≤-/span-≥≥--≤≤-span style=-»-background-color: rgb(229, 233, 236);-»--≥≥--≤≤-div style=-»-font-family: Arial; font-size: 13px;-»--≥≥-4. Storage-тэй холбоотой ажилууд дээр оролцсон-≤≤-/div-≥≥--≤≤-div style=-»-font-family: Arial; font-size: 13px;-»--≥≥--≤≤-br-≥≥--≤≤-/div-≥≥--≤≤-div style=-»-font-family: Arial; font-size: 13px;-»--≥≥--≤≤-b-≥≥-Даваарагчаа:-≤≤-/b-≥≥--≤≤-br-≥≥--≤≤-/div-≥≥--≤≤-div style=-»-font-family: Arial; font-size: 13px;-»--≥≥-1.-Ω-Их засвар-г гадаад өөр байгууллагуудад нээх ажилыг хийсэн.-≤≤-/div-≥≥--≤≤-div style=-»-font-family: Arial; font-size: 13px;-»--≥≥-2.-Ω-Алдаа үйлчилгээн дээр туслалцаа үзүүлсэн.-≤≤-/div-≥≥--≤≤-/span-≥≥--≤≤-/div-≥≥-";
			if(post.description && (post.pro =='public' || post.pro =='mail'))  {
				let str =post.description;
				str =str.replace(/-ΩΩ-/gi, '"');
				str =str.replace(/-Ω-/gi, ' ');
				str =str.replace(/-»-/gi, '"');
				str =str.replace(/-^-/gi, "'");
				str =str.replace(/-§-/gi, '&');
				str =str.replace(/-ŧ-/gi, '+');
				str =str.replace(/-±-/gi, '#');
				str =str.replace(/-≤-/gi, '&lt;');
				str =str.replace(/-≥-/gi, '&gt;');
				str =str.replace(/-≤≤-br/gi, '<br/');
				str =str.replace(/-≤≤-/gi, '<');
				str =str.replace(/-≥≥-/gi, '>');
				str =str.replace(/-ì-/gi, '\\');
				post.description =str;
			};

            var props = {
                isExtended: post.isExtended,
                seen: post.seen,
                pro: post.pro,
                encPro: post.encPro,
                module: post.module,
                encModule: post.encModule,
                itemId: post.itemId,
                title: post.title,
                description: post.description,
                labels: post.labels,
                authorId: post.authorId,
                authorName: post.authorName,
                authorComName: post.authorComName,
                authorApp: post.authorApp,
                authorIcon: post.authorIcon,
                date: post.date,
                fullDate: post.fullDate,
				commentTotalCnt: post.commentTotalCnt,
				totalReplyCnt: post.totalReplyCnt,
                holder: post.holder,
                plusCnt: post.plusCnt,
                upReplyCnt: post.upReplyCnt,
				temp: post.temp  //Зөвхөн frontEnd орчин бүрдүүлэх обьект
            };

            if(post.images.length)  props['images'] =post.images;
            if(post.images.length)  props['imageCnt'] =post.imageCnt;
            if(post.files.length)  props['files'] =post.files;
            if(post.poll)  props['poll'] =post.poll;
            if(post.comments)  props['comments'] =post.comments;
            if(post.hasMe)  props['hasMe'] =post.hasMe;
            if(post.targetId)  props['targetId'] =post.targetId;
            if(post.replyAll)  props['replyAll'] =post.replyAll;
            if(post.mailUsers)  props['mailUsers'] =post.mailUsers;
            if(post.docsHolder)  props['docsHolder'] =post.docsHolder;
            if(post.autoNumber)  props['autoNumber'] =post.autoNumber;
            if(post.senderName)  props['senderName'] =post.senderName;
            if(post.shiftInfo)  props['shiftInfo'] =post.shiftInfo;
            if(post.shiftCnt)  props['shiftCnt'] =post.shiftCnt;
            if(post.isAnswer)  props['isAnswer'] =post.isAnswer;
            if(post.pastDay)  props['pastDay'] =post.pastDay;
            if(post.restDay)  props['restDay'] =post.restDay;
            if(post.answerDate)  props['answerDate'] =post.answerDate;
            if(post.week)  props['week'] =post.week;
            if(post.sharedUser)  props['sharedUser'] =post.sharedUser;
            if(post.sharedUserCnt)  props['sharedUserCnt'] =post.sharedUserCnt;
            if(post.timeRequestType)  props['timeRequestType'] =post.timeRequestType;
            if(post.requestDate)  props['requestDate'] =post.requestDate;
            if(post.requestStatus)  props['requestStatus'] =post.requestStatus;
            if(post.requestDays)  props['requestDays'] =post.requestDays;
            if(post.reqSTime)  props['reqSTime'] =post.reqSTime;
            if(post.reqETime)  props['reqETime'] =post.reqETime;
            if(post.actionBar)  {
                var actionAuthorInfo =post.actionBar.actionAuthorInfo,
                    actionTitle ='';
                if(post.actionBar.title =='editNews')  actionTitle ='Мэдээ заслаа'; else
                if(post.actionBar.title =='editForum')  actionTitle ='Хэлэлцүүлэг заслаа'; else
                if(post.actionBar.title =='editFile')  actionTitle ='Файл заслаа'; else
                if(post.actionBar.title =='editImage')  actionTitle ='Зураг заслаа'; else
                if(post.actionBar.title =='editVote')  actionTitle ='Санал асуулга заслаа'; else
                if(post.actionBar.title =='addComment' && post.pro !='docs')  actionTitle ='Сэтгэгдэл нэмлээ'; else
                if(post.actionBar.title =='addComment' && post.pro =='docs')  actionTitle ='Тэмдэглэл нэмлээ'; else
                if(post.actionBar.title =='addPlus')  actionTitle ='Дэмжлээ'; else
                if(post.actionBar.title =='addComplain')  actionTitle ='Өргөдөл шилжүүллээ'; else
                if(post.actionBar.title =='addReceived')  actionTitle ='Бичиг шилжүүллээ'; else
                if(post.actionBar.title =='addTimeRequest')  actionTitle ='Хүсэлтийг шилжүүллээ'; else
                if(post.actionBar.title =='approveTimeRequest')  actionTitle ='Хүсэлтийг баталлаа'; else
                if(post.actionBar.title =='cardClose' && post.module =='complain')  actionTitle ='Өргөдлийг хаалаа'; else
                if(post.actionBar.title =='cardClose' && post.module =='received')  actionTitle ='Бичгийг хаалаа'; else
                if(post.actionBar.title =='addAnswer')  actionTitle ='Санал өглөө';

                props['actionTitle'] =actionTitle;
                props['actionAuthorName'] =actionAuthorInfo.system_name;
                props['actionAuthorApp'] =actionAuthorInfo.app_name;
                props['actionAuthorIcon'] =actionAuthorInfo.user_icon;
                props['actionDate'] =post.actionBar.date;
                props['actionFullDate'] =post.actionBar.actionFullDate;
            }
            else  {
                if(post.pro =='mail' && post.module =='write')  props['actionTitle'] ='Танд захидал илгээлээ'; else
                if(post.pro =='time_access' && post.module =='timeRequest')  props['actionTitle'] ='Танд хүсэлт илгээлээ'; else
                if(post.pro =='docs' && post.module =='complain')  props['actionTitle'] ='Өргөдөл нэмлээ'; else
                if(post.pro =='docs' && post.module =='received')  props['actionTitle'] ='Бичиг нэмлээ'; else
                if(post.pro =='public' && post.module =='news')  props['actionTitle'] ='Мэдээ нэмлээ'; else
                if(post.pro =='public' && post.module =='forum')  props['actionTitle'] ='Хэлэлцүүлэг нэмлээ'; else
                if(post.pro =='public' && post.module =='file')  props['actionTitle'] ='Файл нэмлээ'; else
                if(post.pro =='public' && post.module =='gallery')  props['actionTitle'] ='Зураг нэмлээ'; else
                if(post.pro =='public' && post.module =='vote')  props['actionTitle'] ='Санал асуулга нэмлээ';
            };

            WallLayout.append('<PostBox>', props);
        });
});
