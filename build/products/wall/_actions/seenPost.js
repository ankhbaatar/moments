

setModuleAction(function seenPost(data)  {

		fetchRequest(ApiServer +'/seen',  {
			method: "POST",
            body: JSON.stringify(data),
			cookies: ['accessToken'],
            onResponse: onResult
		});

        function onResult(res)  {
            // window.parent.postMessage([{typeModule: 'newState'}], 'http://localhost/able_2021');
			// window.parent.postMessage([{typeModule: 'newState'}], 'https://time.able.mn');
			let origin =window.location.ancestorOrigins[0];
			window.parent.postMessage([{typeModule: 'newState'}], origin);
        }
});
