

setModuleAction(function loadNext(val)  {
		var userId = ableCore.urlData.split("="),
			notIn =getModuleData('root').notIn;

		sendRequest(ApiServer +"/postList/read?data="+ userId[1] +"&type=second&rows="+ val +'&notIn='+ notIn, {
			method: "POST",
			onResponse: dispatchData
		});

		function dispatchData(data)  {
			var pbs =[];
			data.each(function(row)  {
				pbs.push({
					name: row.listData[0].title,
					description: row.listData[0].body,
					access: row.listData[0].access,
					autorId: row.listData[0].autorId,
					isInit: row.isInit,
					itemId: row.itemId,
					files: row.files,
					sharedList: row.sharedList,
					photos: row.photos,
					header: {
						mod: row.boxType,
						pro: row.pro,
						userName: row.autorInfo[0].sysName,
						appName: row.autorInfo[0].appName,
						headText: row.title,
						isInit: row.isInit,
						userIcon: row.userIcon,
						addDate: row.listData[0].date
					},
					group: {
						group: row.group
					},
					plus: row.plusCnt,
					answerList: {
						itemId: row.itemId,
						userId: userId[1],
						items: row.answerList,
						statusType: row.listData[0].status,
						voteType: row.listData[0].voteType,
						voteCount: row.voteCount,
						duration: row.listData[0].duration,
						comId: row.listData[0].comId,
						endDate: row.listData[0].date
					},
					action: {
						actionData: row.action
					},
					commentItems: row.comments,
					commentUserId: userId[1]
				});
			});

			var List =adom("#RootLayout:0 div:0");	
			pbs.each(function(item)  {
				List.append('<PostBox>', item);
			});
			
			if(data =='') adom("#RootLayout:0 .loading").html('');
		};
});
