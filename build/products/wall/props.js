

    wall = {
        $actions: [
			noBackend ? ".._actions/static/getData.js" : ".._actions/getData.js",
			noBackend ? ".._actions/static/readMore.js" : ".._actions/comments/readMore.js",
			".._actions/drawPosts.js",
			".._actions/plus.js",
			noBackend ? ".._actions/static/sendComment.js" : ".._actions/comments/sendComment.js",
			".._actions/comments/deleteComment.js",
			".._actions/vote/sendVote.js",
			".._actions/slideShow.js",
			".._actions/seenPost.js"
		],
		dataTube: "getData",
		layout: {
			path: ".._layouts/Wall"
		}
    };

