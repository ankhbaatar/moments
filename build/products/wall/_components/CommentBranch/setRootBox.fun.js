

function fun()  {
	return function setRootBox(eachData)  {  //Root мөчир бол шууд ReplyBox дангаараа зурагдах ёстой
		box.addClass('root');
		box.data(extend(eachData, RootData = {  //Child comp-уудад ашиглах датааг зөвхөн root мөчирт үүсгэнэ
			holder: own.holder,
			onPlusClick: own.onPlusClick || function(){},
			onPlusCountClick: own.onPlusCountClick || function(){},
			onSendComment: own.onSendComment || function(){},
			onRemoveComment: own.onRemoveComment || function(){},
			targetId: own.targetId
		}));

		box.html('<div class="CBcontent root">', [
			['<div class="CBreplyBox root">', [
				['<ComposeBox class="root">', { holder: own.holder }]
			]]
		]);
	};

};
