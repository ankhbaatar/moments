

function fun()  {
	return function setComm(eachData)  {
		RootData =box.parents("CommentBranch.root:0").data();
		var isRootChild =box.parent().hasClass('root');  //Root мөчрийн хүүхэд эсэх
		if(!isRootChild)  //Root мөчрийн хүүхдүүд биш буюу Reply comment бол
			box.addClass('replyComm');

		if(own.animate)  box.addClass('CBprepare');
		box.html([
			['<div class="CBtree '+ (isRootChild ? 'root' : '') +'">', [
				['<div class="authorBox">', [(RootData.targetId ==own.Id ? 
					['<div class="CBsign">', [
						['<div>']
					]] : []),
					['<UserIconTemp>', mkAuthIconProps()]
				]]
			]],
			['<div class="CBcontent">', [
				['<CommBox>', {
					comment: own.comment,
					authorName: own.author.name,
					date: own.date,
					fullDate: own.fullDate
				}],
				['<Footer>', {
					Id: own.Id,
					plusCnt: own.plusCnt,
					hasMe: own.hasMe
				}], (own.totalReplyCnt >0 ? 
				['<div class="CBreplyBox">'] : [])
			]]
		]);

		box.data(extend(eachData, {  //Root мөчирт байх боломжгүй зөвхөн дэдүүдэд байх функцуудыг set хийнэ
			update: update,
			remove: remove,
			mkLinePath: mkLinePath,
			mkLinePathUps: mkLinePathUps
		}));

		CommBox =box.select("CommBox:0", 2);
		Footer =CommBox.next();
		animate();


		function mkAuthIconProps()  {
			var props = {
				unit: isMobile ? "vw" : "px",
				size: isMobile ? 9 : 35,
				bw: isMobile ? 0.2 : 1
			};

			if(box.hasClass('replyComm'))  props.size =isMobile ? 7 : 22;
			if(own.author.hasOwnProperty('icon'))  props =extend(props, own.author.icon);
			return props;
		};

		function animate()  {  //Comment-ийг animate хийж гаргаж ирнэ
			if(!own.animate)  return;
			var newh =CommBox.parent().outerHeight();  //offset height

			box.height(newh -30);
			box.animate({ height: newh }, 150, function()  {
				box.removeClass('CBprepare');
				box.removeAttr('style');  //element style-аар өгсөн height-ийг устгана
				if(own.onComplete)  own.onComplete();
			});
		};
	};

};
