

	require([
		".._components/PlusBox",
		"./Footer.css", (isMobile ? 
		"./Footer.mobile.css" : "")
	]);

function component()  {
	return function FooterComponent(box, option)  {
		var own =this,
			RootData =box.parents("CommentBranch.root:0").data(),  //CommentBranch comp box
			MenuBox =null,
			MoreMenu =null;

		this.Id =8;
		this.plusCnt =3;
		this.hasMe =false;  //Тус хэрэглэгч нэмэх өгсөн эсэх
		own =extend(own, option);


		function init()  {
			box.html([
				['<PlusBox>', {
					itemId: own.Id,
					count: own.plusCnt,
					hasMe: own.hasMe,
					onPlusClick: RootData.onPlusClick,
					onPlusCountClick: RootData.onPlusCountClick
				}],
				['<div class="CBFmenuBox id'+ own.Id +'">', [
					['<div class="CBFmenuItem reply">', ["Хариулах"]],
					['<div class="CBFmoreMenu">', [
						['<svg width="5" height="4" viewBox="0 0 5 4">', [
							['<circle cx="2" cy="2" r="1.5">']
						]],
						['<svg width="5" height="4" viewBox="0 0 5 4">', [
							['<circle cx="2" cy="2" r="1.5">']
						]]
					]]
				]]
			]);

			MenuBox =box.select(".CBFmenuBox:0");
			MoreMenu =MenuBox.select(".CBFmoreMenu:0");
		};

		function mkMoreMenu()  {  //Нэмэлт менюг байгуулна
			window.addEventListener('mousedown', mouseDown);
			MoreMenu.addClass('on');
			MoreMenu.hide();
			MenuBox.append([
				['<div class="CBFmenuItem edit authorMenu">', ["Засах"]],
				['<div class="CBFmenuItem remove authorMenu">', ["Устгах"]]
			]);

			MenuBox.select(".CBFmenuItem.edit").click(edit);
			MenuBox.select(".CBFmenuItem.remove").click(remove);
		};

		function hideMoreMenu()  {  //Нэмэлт менюг хураана
			window.removeEventListener('mousedown', mouseDown);
			MenuBox.select(".authorMenu").remove();
			MoreMenu.removeClass('on');
			MoreMenu.show();
		};

		function mouseDown(e)  {
			var onOutside =false,
				target =adom(e.target);

			onOutside =target.parents(".CBFmenuBox.id"+ own.Id +":0").length ==0;
			if(onOutside)  hideMoreMenu();
		};

		function event()  {
			MenuBox.select(".CBFmenuItem.reply").click(reply);
			MoreMenu.mouseDown(mkMoreMenu);
		};


		function reply()  {
			// var RootBox =box.parents("CommentBranch.root:0");
			// var ComposeBox =RootBox.select("ComposeBox:0");
			// ComposeBox.each(function(item)  {
				// let isRoot =item.hasClass('root');
				// if(!isRoot)  item.remove();
			// });

			// if(ComposeBox.length >0)  {
				// var dta =ComposeBox.parents("CommentBranch:0").data();
				// ComposeBox.remove();
				// console.log('data: ', dta);
				// if(dta.mkLinePath)  dta.mkLinePath('on cancel compose');  //update line path
			// };
			// mkLinePathUps();

			var ReplyBox =null,
				Pars =box.parents("CommentBranch"),
				pHolderName ="";
				parentId =own.Id;

			if(Pars.length ==4)  {  /*Гүний хязгаар тулсан бол шинэ мөчир үүсгэхгүй буюу 
				parent Comment Box-ийн ReplyBox - ийг зааж өгнө */
				pHolderName =box.prev().select(".CBCBauthorName:0").html();
				ReplyBox =box.parents(".CBreplyBox:0");
				parentId =parseInt(Pars.nodeList[1].attr('id'));
				console.log('parentId: '+ parentId);
			}
			else  {
				ReplyBox =box.next();
				if(ReplyBox.length ==0)  {
					box.after('<div class="CBreplyBox">');
					ReplyBox =box.next();
				};
			};

			var ComposeBox =ReplyBox.select("ComposeBox:0", 1);
			if(ComposeBox.length >0)  {
				let Textarea =ComposeBox.select("textarea");
				if(pHolderName !="")  Textarea.val(pHolderName);
				Textarea.focus();
				return;
			};

			ReplyBox.append('<ComposeBox>', {
				// pId: own.Id,
				pId: parentId,
				holder: RootData.holder,
				parentHolderName: pHolderName,
				animate: true
			});
		};

		function edit()  {
			var CommBox =box.prev();
			CommBox.after('<ComposeBox>', {
				Id: own.Id,
				holder: RootData.holder,
				value: CommBox.select(".CBCBcomment").html(),
				height: CommBox.outerHeight(),
				onCancel: function()  {
					CommBox.show();
				}
			});

			CommBox.hide();
		};

		function remove()  {
			var text ="Та сонгогдсон сэтгэгдлийг устгах гэж байна!<br/>";
				text +="Хэрэв устгавал түүний бүх хариултууд хамт устгагдана<br/>";
				text +="гэдгийг анхаарна уу! Та үнэхээр устгах уу?";

			adom("body").prepend('<ConfirmBox>', {
				question: text,
				onAgree: execute
			});

			function execute()  {
				if(!RootData.hasOwnProperty('onRemoveComment'))  return;
				var MyCommBox =box.parent().parent(),
					exec =MyCommBox.data().remove;  //Тус Footer comp-ийг агуулж байгаа CommBranch-ийн өөрийгөө устгах функц
				RootData.onRemoveComment(own.Id, exec);
			};
		};

		init();
		event();
	};

};
