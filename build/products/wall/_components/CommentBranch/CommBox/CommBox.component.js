

	require("./CommBox.css");

function component()  {
	return function CommBoxComponent(box, option)  {
		var own =this,
			DateBox =null;

		this.authorName ="";
		this.date ="";
		this.fullDate ="";
		this.comment ="";
		own =extend(own, option);


		function init()  {
			var Parent =box.parents("CommentBranch:0"),
				delta =Parent.position().right -box.position().left,
				maxw =delta -21,  //21: spacing + border of parent
				hasMore =false,  //Comment read more link авах эсэх
				introCnt =0;

			box.html([
				['<div class="CBCBheader">', [
					['<div class="CBCBauthorName">', [own.authorName]],
					['<div class="CBCBdate">', [own.date]]
				]],
				['<div class="CBCBcomment" style="max-width:'+ maxw +'px">', [own.comment, function(ch, ind, tree)  {
					if(introCnt ==150)  {  //Комментийг intro болгон тасалж авах тэмдэгтийн хязгаар
						hasMore =(own.comment.length >ind +1);
						return "break";
					};
					introCnt +=1;
				}]]
			]);

			var Node =box.select(".CBCBcomment:0", 1);
			if(hasMore)  mkSmartView(Node, own.comment);

			DateBox =box.select(".CBCBdate:0");
			box.data({
				update: update
			});
		};

		function mkSmartView(TextNode, value)  {
			var fnode =TextNode.nodeList[0];
			mkSeeMore();


			function mkSeeMore()  {
				TextNode.append(' ..');
				TextNode.append('<span class="seeMore link">', ['Цааш үзэх']);
				TextNode.select(".seeMore:0").click(seeMore);
			};

			function seeMore()  {
				delete fnode.noParse;  //Html parse хийгдсэн төлвийг устгаж дахин parse хийх боломж үүсгэнэ
				TextNode.html(value, "Text");
			};
		};

		function update(newProps)  {
			own =extend(own, newProps);
			DateBox.html(own.date);
			box.select(".CBCBcomment:0").html(own.comment);
		};

		function event()  {
			DateBox.overHint({
				hint: own.fullDate,
				side: 'right'
			});
		};

		init();
		event();
	};

};
