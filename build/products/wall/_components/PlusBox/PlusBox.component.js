

	require([
		"./PlusBox.css",
		"./LargePlus.css",
		"./HasPlus.css"
	]);

function component()  {
	return function PlusBoxComponent(box, option)  {
		var own =this,
			IconBox =null,
			CountBox =null;

		this.itemId =0;  //Тухайн plus-ийг эзэмшигч item-ийн Id
		this.itemType ='comment';  //docs, news, forum гэх мэт
		this.count =8;
		this.hasMe =false;  /*Тус хэрэглэгч нэмэх өгсөн эсэх
			.onPlusClick: (Function)  Plus дээр дарахад ажиллах гадаад функц
			.onPlusCountClick: (Function) Тоон дээр дарахад ажиллах гадаад функц */
		this.size ="small";
		own =extend(own, option);


		function init()  {
			if(!own.count)  own.count =0;
			else  own.count =parseInt(own.count);

			box.addClass(own.size);
			box.html('<div class="PBicon">', [
				['<svg viewBox="0 0 8 8">', [
					['<polygon class="PBplus" points="0,3 3,3 3,0 5,0 5,3 8,3 8,5 5,5 5,8 3,8 3,5 0,5">'],
					['<polygon class="PBminus" points="0,3 8,3 8,5 0,5">']
				]]
			]);

			if(own.count >0)  setCountBox();
			if(own.hasMe)  box.addClass('hasMe');
			IconBox =box.select(".PBicon:0");
			CountBox =(own.count >0) ? IconBox.next() : null;
			IconBox.overHint({
				hint: (own.hasMe) ? "Цуцлах" : "Дэмжих",
				side: (own.hasMe) ? "left" : "right"
			});
		};

		function setCountBox()  {
			box.addClass('hasPlus');
			box.append('<div class="PBcount">', [
				['<div>', [own.count]]
			]);

			CountBox =box.select(".PBcount:0");
		};

		function removeCountBox()  {
			box.removeClass('hasMe hasPlus');
			CountBox.remove();
			CountBox =null;
		};

		function setCount(newCount)  {
			var way ="";
			if(newCount >own.count)  way ="up";  else
			if(newCount <own.count)  way ="down";
			if(way =="")  return false;

			own.count =newCount;
			if(way =="up")  {
				own.hasMe =true;
				IconBox.overHint({
					hint: "Цуцлах",
					side: "left"
				});

				if(!CountBox)  setCountBox();
				box.addClass('hasMe');
				up();
			}
			else
			if(way =="down")  {
				own.hasMe =false;
				IconBox.overHint({
					hint: "Дэмжих",
					side: "right"
				});

				box.removeClass('hasMe');
				down();
			};


			function up()  {  //Count-ийг нэмэгдүүлэх animate хийнэ
				CountBox.append('<div>', [own.count]);
				CountBox.select("div:0").animate({ marginTop: -13 }, 120, function()  {
					CountBox.select("div:0").remove();
				});
			};

			function down()  {  //Count-ийг хасах animate хийнэ
				CountBox.prepend('<div style="margin-top:-13px">', [own.count]);
				CountBox.select("div:0").animate({ marginTop: 0 }, 120, function()  {
					CountBox.select("div:1").remove();
					if(own.count <=0)  removeCountBox();
				});
			};
		};

		function event()  {
			if(CountBox)  {
				CountBox.mouseOver(function(){  IconBox.addClass('onCountOver');  });
				CountBox.mouseOut(function(){  IconBox.removeClass('onCountOver');  });
			};

			if(own.hasOwnProperty('onPlusCountClick') && CountBox)	CountBox.click(function()  {
				own.onPlusCountClick(own.itemId, own.itemType);
			});

			if(own.hasOwnProperty('onPlusClick'))  IconBox.click(function()  {
				own.onPlusClick(setCount, {
					itemId: own.itemId,
					itemType: own.itemType,
					add: !own.hasMe
				});
			});
		};

		init();
		event();
	};

};
