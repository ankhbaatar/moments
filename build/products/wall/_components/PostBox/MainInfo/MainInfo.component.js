

	require([
		"_components/Able/Label",
		"./MainInfo.css", (isMobile ? 
		"./MainInfo.mobile.css" : "")
	]);

function component()  {
	return function MainInfoComponent(box, option)  {
		var own =this,
			Title =null,
			Desc =null;

		this.title ="Компаний Facebook хуудаст пост оруулах санаануудаа хуваалцаарай";
		this.description ="Эйбл төглийг хаана хийх вэ? Санал хуваагдсан учир санлаа өгөөрэй";
		this.labels = [
			"Албан ажлын",
			"Дизайны"
		];
		own =extend(own, option);


		function init()  {
			var titleLen =own.title ? own.title.length : 0,
				descLen =own.description ? own.description.length : 0,
				hasMoreTitle =false,  //Intro-д үндсэн description бүтэн багтаагүй more link-тэй байх эсэх
				hasMore =false,  //Intro-д үндсэн description бүтэн багтаагүй more link-тэй байх эсэх
				cntTitle =1;
				cnt =1;

			var PostBox =box.parents("PostBox:0").data(),
				pro =PostBox.pro,
				brCnt =0;

			box.html([
				['<div class="mainTitle">', [own.title, function(ch, ind, tree)  {
					if(cntTitle ==150)  {  //Description-оос Intro-г тасалж авах тэмдэгтийн хязгаар
						hasMoreTitle =(titleLen >ind +1);
						return "break";
					};
					cntTitle +=1;
				}]],
				['<div class="description">', [own.description, (PostBox.readOnly ? function(){} : function(ch, ind, tree)  {  //Ийшээ нэг бол word эсвэл tag орж ирнэ
					// console.log(ind +':', ch);
					if(typeof ch =='object')  {
						if(ch.key =='br')  {
							if(tree.length ==0)  return "skip";  //Tree хоосон байхад хамгийн эхний <br> - уудыг алгасна
							if(brCnt >=2)  return "skip";  //Дараалсан 2 <br> - ийг зөвшөөрч 3 дахиас алгасна
							brCnt +=1;
						}
						else  brCnt =0;
						return;
					};

					if(pro =="mail" && ch =="-")  {  /*Mail програмд ------ буюу дараалсан зураасуудыг таньж 
						өмнүүр нь тасална. Энэ гарын үсгийн хэсгийг Intro-д харуулахгүйн тулд юм */
						let me =own.description;
						if(me[ind +1] =="-" && me[ind +2] =="-" && me[ind +3] =="-" && me[ind +4] =="-" && me[ind +5] =="-")  {
							hasMore =(descLen >ind +5);
							return "break";
						};
					};

					if(cnt ==300)  {  //Description-оос Intro-г тасалж авах тэмдэгтийн хязгаар
						hasMore =(descLen >ind +1);
						return "break";
					};
					cnt +=1;

					if(pro =="blabla" && ch =='onEnd')  {  /*Parse функц дуусаад үр дүнгээ ирүүлсэн бол 
						орж ирсэн DOM tree-г төгсгөлөөс trim хийнэ. Төгсгөлдөө хичнээн олон tag байсан ч 
						ямар нэг text node байхгүй бол устгана */
						// Энэ функц Үржээгийн захидлыг төгсгөлөөс trim() хийж чадахгүй байна. Зөвхөн гүн 0 байхад pop хийх хангалтгүйн байна
						var newTree =ind,
							rmLength =0;  //Remove length буюу newTree-г хойноос нь устгах урт

						check(newTree.reverse(), 0);
						newTree.reverse();
						if(rmLength >0)  for(let i=0; i<rmLength; i++)  {
							newTree.pop();
							return newTree;
						};


						function check(items, i)  {
							let isContinue =true;  //Recurse тасрах нөхцөл бүрдээгүй цааш үргэлжлэх эсэх
							items.each(function(item, m)  {
								// console.log(mkSpace(i) +':'+ i, item, m);
								let type =typeof item;
								if(type =='string')  //String буюу text node байгаа бол recurse-ийг зогсооно
									if(item !='div' && item !='br' && item !='byHtmlParse')  //Текст нь стандарт биш бол word гэж үзнэ
										isContinue =false;

								if(Array.isArray(item))  //Item array бол recurse цааш явна
									isContinue =check(item, i+1);

								if(i ==0)  rmLength =m;
								if(!isContinue)  return false;
							});

							return isContinue;
						};

						function mkSpace(len)  {
							let space ="";
							for(let j=0; j<len; j++)  {
								space +='	';
							};

							return space;
						};
					};
				})]]
			]);

			Title =box.select(".mainTitle:0", 1);
			Desc =box.select(".description:0", 1);
			if(hasMoreTitle)  mkSmartView(Title, own.title, 'large');
			if(hasMore)  mkSmartView(Desc, own.description);
			resizeImages();  //Description intro-д зураг байвал өргөнийг тохируулна

			if(!own.labels)  return;
			if(own.labels.length >0)  {
				let Labels =adom('<div class="labels">');
				box.select(".mainTitle:0").after(Labels);
				own.labels.each(function(value)  {
					Labels.append('<Label name:"'+ value +'">');
				});
			};
		};

		function mkSmartView(TextNode, value, size)  {
			var intro =TextNode.html(),
				fnode =TextNode.nodeList[0];
			mkSeeMore();


			function mkSeeMore()  {
				TextNode.append(' ..');
				TextNode.append('<span class="seeMore dlink '+ size +'">', ['Цааш үзэх']);
				TextNode.select(".seeMore:0").click(seeMore);
			};

			function seeMore()  {
				delete fnode.noParse;  //Html parse хийгдсэн төлвийг устгаж дахин parse хийх боломж үүсгэнэ
				TextNode.html(value, "Text");
				TextNode.append(' ..');
				TextNode.append('<span class="seeLess dlink '+ size +'">', ['Хураах']);
				TextNode.select(".seeLess:0").click(seeLess);
				resizeImages();
			};

			function seeLess()  {
				delete fnode.noParse;  //Html parse хийгдсэн төлвийг устгаж дахин parse хийх боломж үүсгэнэ
				TextNode.html(intro, "Text");
				mkSeeMore();
			};
		};

		function resizeImages()  {  //Description-д зураг орж ирсэн бол өргөнийг тохируулна
			var boxr =box.position().right,
				Images =Desc.select("img");

			Images.load(function()  {
				var Img =adom(this),
					imgr =Img.position().right;

				if(imgr >boxr)  {  //Тус зураг маскаас хальж байгаа бол
					let w =boxr -Img.position().left;
					Img.width(w);
				};
			});

			Images.style("cursor", "pointer");
			Images.click(function()  {
				var target =adom(this).attr('src');
				window.open(target);
			});
		};

		init();
	};

};
