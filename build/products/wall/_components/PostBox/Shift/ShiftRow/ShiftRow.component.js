

	require([
        "_components/Able/BlueButton",
		"_components/Able/UserIconTemp",
		"./ShiftRow.css", (isMobile ? 
		"./ShiftRow.mobile.css" : "")
	]);

function component()  {
	return function ShiftRowComponent(box, option)  {
		var own =this;
		this.userInfo ={
			path: '',
			name: 'Г.Цогтбаяр',
			app: 'Хөгжүүлэгч'
		};
		this.receivedDate ='Дөнгөж сая';
		this.receivedDateHint ='Хүлээн авсан 21/10/06 - 08:31'; /*
			.ruleData (Object)
			.closedData (Object)*/
		this.cardClose =false;
		this.lastShift =false;
		this.holderMe =false;
		this.superUser =false;
		own =extend(own, option);


		function init()  {
			var RootData =box.parents('PostBox:0', 4).data();
			var userIconSize =(isMobile ? 'mlarge' : 'medium');
			box.html([
				['<div class="userIcon">', [
					['<UserIconTemp>', {
						path: own.userInfo.path.path,
						gender: own.userInfo.path.gender,
						age: own.userInfo.path.age,
						overHint: own.userInfo.path.overHint,
						unit: isMobile ? "vw" : "px",
						size: isMobile ? 9 : 35,
						bw: isMobile ? 0.2 : 1
					}],
					['<div class="lines">']
				]],
				['<div class="card">', [
					['<div class="userName">', [own.userInfo.name]],
					['<div class="appName">', [own.userInfo.app]],
					['<div class="rdate">', [own.receivedDate]]
				]]
            ]);

			var lines =box.select('.userIcon:0 .lines:0'),
				card =box.select('.card:0', 1);

			if(own.cardClose)  {
				card.append('<div class="closed '+ (own.closedData.isLate ? 'isLate' : '') +'">', [
					['<div class="title">', [
						['<div class="text">', [own.closedData.title]],
						['<div class="icon">', [
							['<svg width="12px" height="14px" viewBox="0 0 12 14">', [
								['path fill="#ffffff" d="M10.499,6h-0.5V3c0-1.657-1.343-3-3-3h-2c-1.657,0-3,1.343-3,3v3H1.5C0.671,6,0,6.671,0,7.5v4.998'+
								'c0,0.829,0.671,1.5,1.5,1.5h8.999c0.829,0,1.5-0.671,1.5-1.5V7.5C11.999,6.671,11.327,6,10.499,6z M3.999,3c0-0.551,0.449-1,1-1h2'+
								'c0.551,0,1,0.449,1,1v3h-4V3z"']
							]]
						]]
					]],
					['<div class="closedInfo">', (RootData.pro =='docs' ? 
						mkDocClose() : (RootData.pro =='time_access' ? 
						mkTimeClose() : [])
					)]
				]);

				lines.append([
					['<div class="line">'],
					['<div class="arrow">']
				]);

				var Line =lines.select(".line:0"),
					pos =card.select(".closed:0 .title:0").position(),
					tt =pos.top,  //titleBar top
					lt =Line.position().top,  //line top
					ah =lines.select(".arrow:0").outerHeight(),  //arrow height
					th =pos.height;  //titleBar height
				Line.style('height', tt -lt +(th -ah)/2);

				if(RootData.pro =='time_access')  {
					var closeBox =card.select('.closed:0'),
						title =closeBox.select('.title:0');

					closeBox.style('background-color', '#ddedfd');
					closeBox.style('border-color', '#89b7d8');
					title.style('background-color', '#016cba');
					title.style('border-color', '#016cba');
				};
			}
			else  {
				if(own.lastShift)  {
					if(own.holderMe)  {
						var closeBtnText =(RootData.pro =='docs' ? 'Хаах' : (RootData.pro =='time_access' ? 'Батлах' : ''));
						card.append('<div class="buttons">', [
							['BlueButton class="shift" name:"Шилжүүлэх"'], ((RootData.pro =='time_access' && own.superUser) || RootData.pro =='docs' ?
							['BlueButton class="cardClose" name:"'+ closeBtnText +'"', [
								['<svg viewBox="0 0 12 14">', [
									['path fill="#ffffff" d="M10.499,6h-0.5V3c0-1.657-1.343-3-3-3h-2c-1.657,0-3,1.343-3,3v3H1.5C0.671,6,0,6.671,0,7.5v4.998'+
									'c0,0.829,0.671,1.5,1.5,1.5h8.999c0.829,0,1.5-0.671,1.5-1.5V7.5C11.999,6.671,11.327,6,10.499,6z M3.999,3c0-0.551,0.449-1,1-1h2'+
									'c0.551,0,1,0.449,1,1v3h-4V3z"']
								]]
							]] : [])
						]);

						lines.append([
							['<div class="line">'],
							['<div class="arrow">']
						]);

						var Line =lines.select(".line:0"),
							pos =card.select(".buttons:0 BlueButton:0").position(),
							tt =pos.top,  //titleBar top
							lt =Line.position().top,  //line top
							ah =lines.select(".arrow:0").outerHeight(),  //arrow height
							th =pos.height;  //titleBar height
						Line.style('height', tt -lt +(th -ah)/2);

						let origin =window.location.ancestorOrigins[0];
						var RootData =box.parents('PostBox:0', 4).data();

						card.select('.shift').click(function()  {
							var data =[{
								typeModule: 'shift',
								pro: RootData.pro,
								mod: RootData.encModule,
								itemId: RootData.itemId
							}];
							window.parent.postMessage(data, origin);
						});

						card.select('.cardClose').click(function()  {
							var data =[{
								typeModule: 'close',
								pro: RootData.pro,
								mod: RootData.encModule,
								itemId: RootData.itemId
							}];

							if(RootData.pro =='time_access')  data['type'] =box.parents('Shift:0',2).data().timeRequestType;
			
							// window.parent.postMessage(data, 'http://localhost/able_2021');
							// window.parent.postMessage(data, 'https://time.able.mn');
							window.parent.postMessage(data, origin);
						});
					}
				}
				else  {
					lines.append([
						['<div class="line">'],
                        ['<div class="circle">'],
                        ['<div class="line">']
					]);

					card.append('<div class="rule">', [
									['<div class="title">', [own.ruleData.title]],
									['<div class="note">', [own.ruleData.note]],
									['<div class="date">', [own.ruleData.ruleDate]]
								]);

					var lineH =(box.position().bottom - box.select('UserIconTemp:0', 2).position().bottom -3)/2;
					if(isMobile)  lineH =(box.position().bottom - box.select('UserIconTemp:0', 2).position().bottom -15)/2;

					lines.select('.line', 1).style('height', lineH +'px');
				}
			}

			card.select('.rdate:0', 1).overHint({
				hint: own.receivedDateHint,
				side: "right",
				noWrap: true
			});


			function mkDocClose()  {
				return [
					['<div class="row">', [
						['<div class="text">', ['Хариу өгсөн огноо']],
						['<div class="data">', ['<b>'+ own.closedData.answeredDate +'</b> '+ own.closedData.answeredWeek]]
					]],
					['<div class="row">', [
						['<div class="text">', ['Хариулсан хэлбэр']],
						['<div class="data">', ['<b>'+ own.closedData.answeredType +'</b>']]
					]],
					['<div class="row">', [
						['<div class="text">', ['Хэрхэн шийдвэрлэсэн']],
						['<div class="data">', ['<b>'+ own.closedData.closedNote +'</b>']]
					]],
					['<div class="row">', [
						['<div class="text">', ['Картыг хаасан']],
						['<div class="data">', ['<b>'+ own.userInfo.name +'</b> - '+ own.userInfo.app]]
					]]
				];
			};

			function mkTimeClose()  {
				return [
					['<div class="row">', [
						['<div class="text">', ['Баталсан огноо']],
						['<div class="data">', ['<b>'+ own.closedData.answeredDate +'</b> '+ own.closedData.answeredWeek]]
					]],
					['<div class="row">', [
						['<div class="text">', ['Хэрхэн шийдвэрлэсэн']],
						['<div class="data">', ['<b>'+ own.closedData.closedNote +'</b>']]
					]],
					['<div class="row">', [
						['<div class="text">', ['Баталсан ажилтан']],
						['<div class="data">', ['<b>'+ own.userInfo.name +'</b> - '+ own.userInfo.app]]
					]]
				];
			}
		};

		init();
	};

};
