

	require([
		"_components/Able/BlueButton",
		"_components/Able/UserInfo",
		"./ShiftRow",
		"./Shift.css", (isMobile ? 
		"./Shift.mobile.css" : "")
	]);

function component()  {
	return function ShiftComponent(box, option)  {
		var own =this;
		this.shiftInfo =[{  //тухайн албан бичигийн шилжүүлгүүдийн мэдээлэл
			userInfo: {
				name: 'Г.Цогтбаяр',
				app: 'Хөгжүүлэгч'
			},
			ruleData: {
				title: 'Удирдлагын заалт',
				note: 'Хүлээн аваад шилжүүлэв'
			}
		},{
			userInfo: {
				name: 'Г.Цогтбаяр',
				app: 'Хөгжүүлэгч'
			},
			ruleData: {
				title: 'Удирдлагын заалт',
				note: 'Хүлээн аваад шилжүүлэв'
			}
		}];
		this.shiftCnt =4; /*
			.timeRequestType (Int) Цаг бүртгэлийн хүсэлтийн төрөл*/
		own =extend(own, option);


		function init()  {
			if(own.hasOwnProperty('timeRequestType'))  box.data({timeRequestType: own.timeRequestType});
			var diff =own.shiftCnt -own.shiftInfo.length;
			box.html([
				['<div class="beforeShift">', [
					['<div class="text">', [(isMobile ? 'Шилжүүлгүүд' : 'Өмнөх шилжүүлгүүд') +' '+ (diff >0 ? '('+ diff +')' : '')]],(!isMobile ?
					['<div class="icon">', [
						['<svg width="10px" height="10px" viewBox="0 0 10 10">', [
							['<path d="M8,0H2C1.448,0,1,0.448,1,1s0.448,1,1,1h3.586L0.293,7.292c-0.391,0.391-0.391,1.024,0,1.415'+
							's1.023,0.39,1.414,0L7,3.415V7c0,0.552,0.448,1,1,1s1-0.448,1-1V1C9,0.448,8.552,0,8,0z">']
						]]
					]] : [])
				]],
				['<div class="shifts">']
			]);

			var shifts =box.select('.shifts:0', 1);
			for(var i =0; i <own.shiftInfo.length; i++)  {
				shifts.append('<ShiftRow>', own.shiftInfo[i]);
			}

			box.select('.beforeShift:0', 1).click(function()  {
				var RootData =box.parents('PostBox:0', 3).data(),
					data ={
						typeModule: 'windowOpenShift',
						pro: RootData.encPro,
						mod: RootData.encModule,
						itemId: RootData.itemId
					};

				let origin =window.location.ancestorOrigins[0];
				if(!isMobile)  window.parent.postMessage([data], origin);
			});
		};

		init();
	};

};
