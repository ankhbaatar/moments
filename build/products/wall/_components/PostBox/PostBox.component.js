

	require([
		"_components/Able/UsersGroupTemp",
		"_components/Able/SlideShow",
		".._components/PlusBox",
		".._components/CommentBranch",
		"./Time/JustTime",
		"./Time/TimeLength",
		"./Header",
		"./ActionBar",
		"./MainInfo",
		"./Images",
		"./Poll",
		"./Files",
		"./Shift",
		"./PostFooter",
		"./event.fun.js",
		"./onPlusClick.fun.js",
		"./onPlusCountClick.fun.js",
		"./loadMoreComments.fun.js",
		"./onSendComment.fun.js",
		"./onRemoveComment.fun.js",
		"./onVote.fun.js",
		"./PostBox.css?8", (isMobile ? 
		"./PostBox.mobile.css" : "")
	]);

function component()  {
	return function PostBoxComponent(box, option)  {
		var own =this;
		this.itemId =0;
		this.isExtended =false;  //Үндсэн постын араас нэмэлт үйлдэл хийгдсэн горимд PostBox-ийг байгуулах эсэх
		this.pro ='public';  /*
			.encPro: (String) */
		this.module ='news';  /*
			.encModule: (String) */
		this.actionTitle ="Хэлэлцүүлэг нэмлээ";  //Энэ заавал утгатай байх ёстой
		this.title ='Эйбл төгөл';  //Постын нэр
		this.description ={ // Постын тайлбар
			intro: 'Эйбл төглийг хаана хийх вэ? Санал хуваагдсан учир санлаа өгөөрэй.',
			more: ''
		};  /*
			.authorId: (Int) Постыг нэмсэн хэрэглэгчийн Id
			.authorName: (String) Постыг нэмсэн хэрэглэгчийн нэр
			.authorComName: (String) Постыг нэмсэн хэрэглэгчийн байгууллагын нэр
			.authorApp: (String) Постыг нэмсэн хэрэглэгчийн албан тушаал
			.authorIcon: (Object) Постыг нэмсэн хэрэглэгчийн зураг {
				path:
				gender:
			}
			.actionTitle: (String) Үйлдлийн гарчиг
			.actionAuthorName: (String) Үйлдлийг хийсэн хэрэглэгчийн нэр
			.actionDate: (ActionBar comp-ийг харна уу) Үйлдлийг хэзээ хийсэн хугацаа
			.labels: (Array) Постын агуулагдах бүлгүүдийн нэр
			.holder: (Object) Системд нэвтэрсэн байгаа хэрэглэгч. CommentBranch comp-ийн holder-ийг харна уу
			.images: (Array) харуулах зургуудын зам бүхий массив
			.imageCnt: (Int) харуулах зургуудын тоо
			.files: (Array) хавсралт файлуудын мэдээлэл бүхий массив
			.poll: (Object) санал асуулгын мэдээлэл
			.comments: (Object) сэтгэгдлийн дата */
		this.commentTotalCnt =0; //(Int) тус постын нийт сэтгэгдэлийн тоо
		this.totalReplyCnt =0; //(Int) Root сэтгэгдэлийн тоо буюу постын эхний түвшний commentCnt
		this.plusCnt =0;  /*(Int) Нийт plus-ийн тоо
			.replyAll (Boolean) захидалд олон хүнд хариулах эсэх
			.mailUsers (Object) захидалыг илгээсэн хэрэглэгчдийн мэдээлэл
			.docsHolder (Boolean) тухайн албан бичигийг хариуцаж байгаа эсэх
			.autoNumber (String) тухайн албан бичигийн авто дугаарлалт
			.senderName (String) тухайн албан бичигийг илгээгчийн нэр
			.shiftInfo (Object)
			.shiftCnt (Int)
			.timeRequestType (Int)
			.isAnswer (Boolean) хариутай бичиг эсэх
			.pastDay (Int) тухайн албан бичигийг хүлээн авснаас хойш өнгөрсөн хоног
			.restDay (Int) тухайн албан бичигт хариу өгөхөд үлдсэн хоног
			.answerDate (String) тухайн албан бичигийн хариу өгөх огноо
			.week (String) тухайн албан бичигийн хариу өгөх огнооны гариг
			.sharedUser (Array) тухайн албан бичигийг хараж байгаа хүмүүсийн icon
			.sharedUserCnt (Int) тухайн албан бичигийг харах хэрэглэгчдийн тоо
			.hasMe: (Boolean) постод би плас дарсан эсэх 
			.readOnly: (Boolean) статик пост эсэх*/
		this.date ="30 мин";  //(String) Постыг хэзээ нэмсэн хугацаа
		this.actionFullDate ="21/06/18 - 18:36";  //ActionBar-ийн бүтэн огноо
		this.fullDate ="21/06/18 - 18:36";  //Бүтэн огноо
		own =extend(own, option);


		function init()  {
			box.html("");
			box.attr('id', own.itemId);

			var boxData ={
				itemId: own.itemId,
				pro: own.pro,
				mod: own.module,
				encPro: own.encPro,
				encModule: own.encModule
			};
			if(own.hasOwnProperty('readOnly'))  boxData['readOnly'] =true;
			else boxData['readOnly'] =false;
			box.data(boxData);
			var className =own.seen ? 'seen' : 'noSeen';
			box.addClass(className);
			if(own.pro =='docs')  {
				var noClose =true;
				if(own.shiftInfo[own.shiftInfo.length -1].hasOwnProperty('cardClose') && own.shiftInfo[own.shiftInfo.length -1].cardClose)  noClose =false;
				if(own.isAnswer && (own.shiftInfo[own.shiftInfo.length -1].hasOwnProperty('cardClose') && !own.shiftInfo[own.shiftInfo.length -1].cardClose))  noClose =true;
				if(!own.isAnswer)  noClose =false;
				if(isMobile)  noClose =false;
			};

			box.append('<div class="box">', [
				['<div class="mainItems">', [
					['<Header>', mkHeadProps()], (own.pro =='docs' ? 
						['<div class="sender">', [
							['<div class="senderName">', [own.senderName]],
							['<div class="sum">', [
								['<svg width="10px" height="10px" viewBox="0 0 10 10">', [
									['<path fill="#5F91D6" d="M9.706,4.293c-0.391-0.391-1.024-0.391-1.415,0L7,5.584V3c0-1.657-1.343-3-3-3H0v2h4c0.551,0,1,0.449,1,1'+
									'v2.584L3.708,4.293c-0.391-0.391-1.024-0.39-1.415,0c-0.39,0.39-0.391,1.023,0,1.414l2.999,2.999'+
									'C5.458,8.873,5.67,8.952,5.886,8.978C5.925,8.981,5.961,9,6,9c0.038,0,0.075-0.018,0.113-0.022c0.216-0.025,0.428-0.105,0.594-0.271'+
									'l2.999-2.999C10.097,5.317,10.096,4.684,9.706,4.293z">']
								]]
							]]
						]] : []
					),
					['<MainInfo>', mkMainProps()], (own.hasOwnProperty('images') ? 
					['<Images>', {
						images: own.images,
						cnt: own.imageCnt
					}] : []), (own.hasOwnProperty('poll') ? 
					['<Poll>', mkPollProps()] : []), (own.hasOwnProperty('files') && own.pro !='docs' ? 
					['<Files>', {
						files: own.files
					}] : []), (own.pro =='docs' ?
					['<div class="docMainInfo">', [(own.hasOwnProperty('files') ?
						['<FileItem>', {
							type :own.files[0].type,
							size :(isMobile ? 'xlarge' : 'large'),
							path :own.files[0].dir
						}] : []),
						['<div class="mainInfo" '+ (own.hasOwnProperty('files') ? 'style="flex-direction: column;"' : '') +'>', [
							['<div class="autoNumber">', [
								['<div class="title">', ['Бүртгэлийн дугаар']],
								['<div class="info">', [
									['<div class="number">', [own.autoNumber]],
									['<div class="text">', ['Манайд бүртгэгдсэн']]
								]]
							]],
							['<div class="answerDate" '+ (own.hasOwnProperty('files') ? '' : 'style="margin-left: 36px;"') +'>', [
								['<div class="title">', [(own.isAnswer ? 'Хариу өгөх огноо' : 'Хариугүй бичиг')]], (own.isAnswer ?
								['<div class="info">', [
									['<div class="date">', [own.answerDate]],
									['<div class="week">', [own.week]]
								]] : []), (noClose ?
								['<div class="answerDays" '+ (own.restDay <0 ? 'style="margin-top: 7px;"' : '') +'>', [(own.restDay >=0 ?
									['<div class="line">', [(own.pastDay ?
										['<div class="pastDay">'] : []), (own.restDay >0 || own.restDay ==undefined ?
										['<div class="restDay">'] : [])
									]] : []),
									['<div class="days">', [(own.restDay >=0 ?
										['<div>', [(!own.pastDay ? '' : own.pastDay +' хоног')]] : []),
										['<div '+ (own.restDay <0 ? 'style="color: #db4f4f;"' : '') +'>', [(!own.restDay ? 'Өнөөдөр' : (own.restDay >0 ? own.restDay +' хоног үлдсэн' : (own.restDay <0 ? '('+ Math.abs(own.restDay) +' хоног хэтэрсэн)' : '')))]]
									]]
								]]: [])
							]]
						]]
					]] : [])
				]], (own.pro =='time_access' ? (own.timeRequestType ==5 ? 
				['<JustTime date:"'+ own.requestDate +'" status:"'+ own.requestStatus +'">'] : (own.timeRequestType ==4 || own.timeRequestType ==8 ? 
				['<TimeLength stime:"'+ own.reqSTime +'" etime:"'+ own.reqETime +'">', {
					days: own.requestDays
				}] : [])) : []), (own.pro =='docs' || own.pro =='time_access' ?
				['<Shift>', mkShiftProps()] : []
				), (own.pro =='mail' || own.hasOwnProperty('readOnly') || own.pro =='time_access' ? [] :
				['<PostFooter>', mkFooterProps()]), (own.pro =='mail' ?
				['<div class="footer">', [
					['<div class="participants">', [(own.mailUsers.toUserCnt ? 
						['<div class="toUser">', [
							['<div class="toUserIcon">', [
								['<svg viewBox="0 0 12 11" class="vicon" width="12px" height="11px" style="margin-top: 8px;">', [
									['<path d="M11.707,5.28L7.719,1.292c-0.391-0.391-1.024-0.39-1.414,0C5.915,1.683,5.914,2.316,6.304,2.707L8.597,'+
									'5H4 C2.897,5,2,4.103,2,3V1c0-0.552-0.448-1-1-1S0,0.448,0,1v2c0,2.209,1.791,4,4,4h4.584l-2.28,2.28 c-0.391,0.391-0.39,'+
									'1.024,0.001,1.415c0.39,0.39,1.023,0.391,1.414,0l3.987-3.987C11.903,6.511,12,6.252,11.999,5.994 C12,5.735,11.903,5.477,11.707,5.28z">']
								]]
							]],
							['<UsersGroupTemp>', {
								size: isMobile ? 7 : 22,
								unit: isMobile ? "vw" : "px",
								bw: isMobile ? 0.2 : 1,
								totalCount: own.mailUsers.toUserCnt,
								icons: own.mailUsers.toUsersIcon,
								mleft: isMobile ? 10 : 6
							}]
						]] : []), (own.mailUsers.ccUserCnt ? 
						['<div class="ccUser">', [
							['<div class="ccUserIcon">', ['Cc']],
							['<UsersGroupTemp>', {
								size: isMobile ? 7 : 22,
								unit: isMobile ? "vw" : "px",
								bw: isMobile ? 0.2 : 1,
								totalCount: own.mailUsers.ccUserCnt,
								icons: own.mailUsers.ccUsersIcon,
								mleft: isMobile ? 10 : 6
							}]
						]] : []), (own.mailUsers.bccUserCnt ? 
						['<div class="bccUser">', [
							['<div class="bccUserIcon">', ['Bcc']],
							['<UsersGroupTemp>', {
								size: isMobile ? 7 : 22,
								unit: isMobile ? "vw" : "px",
								bw: isMobile ? 0.2 : 1,
								totalCount: own.mailUsers.bccUserCnt,
								icons: own.mailUsers.bccUsersIcon,
								mleft: isMobile ? 10 : 6
							}]
						]] : [])
					]],
					['<div class="actions">', [
						['<div class="mailReply">', [
							['<svg class="vicon" width="15px" height="12px" viewBox="0 0 15 12">', [
								['<path d="M0,5c0-0.131,0.053-0.248,0.135-0.336L0.133,4.662l0.011-0.013c0.002-0.001,0.002-0.002,0.004-0.003'+
								'l3.978-4.473l0.002,0.001C4.22,0.069,4.35,0,4.5,0C4.776,0,5,0.224,5,0.5C5,0.81,5,2.646,5,4h7c1.657,0,3,1.343,3,3v2'+
								'c0,1.657-1.343,3-3,3h-1.984c-0.552,0-1-0.448-1-1s0.448-1,1-1H12c0.551,0,1-0.449,1-1V7c0-0.551-0.449-1-1-1H5'+
								'c0,1.354,0,3.19,0,3.5C5,9.776,4.776,10,4.5,10c-0.15,0-0.28-0.069-0.372-0.173L4.125,9.828L0.148,5.354'+
								'C0.146,5.354,0.146,5.353,0.144,5.351L0.133,5.338l0.001-0.002C0.052,5.248,0,5.131,0,5z">']
							]]
						]], (own.hasOwnProperty('replyAll') ?
						['<div class="mailReplyAll">', [
							['<svg class="vicon" width="20px" height="12px" viewBox="0 0 20 12">', [
								['<path d="M11,4H5c0-1.354,0-3.19,0-3.5C5,0.224,4.776,0,4.5,0C4.35,0,4.22,0.069,4.128,0.173L4.125,0.172'+
								'L0.147,4.646C0.146,4.646,0.146,4.647,0.145,4.649L0.133,4.662l0.001,0.002C0.053,4.752,0,4.869,0,5s0.053,0.248,0.135,0.336'+
								'L0.133,5.338l0.011,0.013c0.001,0.001,0.002,0.002,0.003,0.003l3.978,4.473l0.002-0.001C4.22,9.931,4.35,10,4.5,10'+
								'C4.776,10,5,9.776,5,9.5C5,9.19,5,7.354,5,6h6c0.551,0,1,0.449,1,1v2c0,0.551-0.449,1-1,1H9.016c-0.552,0-1,0.448-1,1s0.448,1,1,1'+
								'H11c1.657,0,3-1.343,3-3V7C14,5.343,12.657,4,11,4z">'],
								['<path d="M17,4h-1.002c-0.552,0-1,0.448-1,1c0,0.552,0.448,1,1,1V6H17c0.551,0,1,0.449,1,1v2c0,0.551-0.449,1-1,1'+
								'h-1.002c-0.552,0-1,0.448-1,1c0,0.552,0.448,1,1,1V12H17c1.657,0,3-1.343,3-3V7C20,5.343,18.657,4,17,4z">']
							]]
						]] : []),
						['<div class="mailForward">', [
							['<svg class="vicon" width="15px" height="12px" viewBox="0 0 15 12">', [
								['<path d="M15,5c0-0.131-0.053-0.248-0.135-0.336l0.001-0.002l-0.011-0.013c-0.002-0.001-0.002-0.002-0.004-0.003'+
								'l-3.978-4.473l-0.002,0.001C10.78,0.069,10.65,0,10.5,0C10.224,0,10,0.224,10,0.5c0,0.31,0,2.146,0,3.5H3C1.343,4,0,5.343,0,7v2'+
								'c0,1.657,1.343,3,3,3h1.984c0.552,0,1-0.448,1-1s-0.448-1-1-1H3c-0.551,0-1-0.449-1-1V7c0-0.551,0.449-1,1-1h7c0,1.354,0,3.19,0,3.5'+
								'c0,0.276,0.224,0.5,0.5,0.5c0.15,0,0.28-0.069,0.372-0.173l0.002,0.001l3.978-4.473c0.002-0.001,0.002-0.002,0.004-0.003'+
								'l0.011-0.013l-0.001-0.002C14.948,5.248,15,5.131,15,5z">']
							]]
						]],
						['<div class="alerts">', [
							['<svg class="vicon" width="13px" height="16px" viewBox="0 0 13 16">', [
								['<path d="M5.091,13.998c-0.056,0.158-0.092,0.325-0.092,0.502c0,0.829,0.671,1.5,1.5,1.5s1.5-0.671,1.5-1.5'+
								'c0-0.177-0.036-0.344-0.092-0.502H5.091z">'],
								['<path d="M12.035,10.103C11.581,9.765,11,9.209,11,8.563V5.5c0-1.965-1.268-3.618-3.023-4.231'+
								'C7.865,0.551,7.249,0,6.5,0S5.135,0.551,5.023,1.269C3.268,1.882,2,3.535,2,5.5v3.063c0,0.647-0.581,1.203-1.035,1.54'+
								'C0.401,10.318,0,10.86,0,11.5C0,12.329,0.671,13,1.5,13h5h5c0.829,0,1.5-0.671,1.5-1.5C13,10.86,12.599,10.318,12.035,10.103z">']
							]]
						]]
					]]
				]] : []), (own.pro =='mail' || own.hasOwnProperty('readOnly') || own.pro =='time_access' ? [] :
				['<div class="extendedItems">', [
					['<CommentBranch>', mkCPprops()]
				]])
			]);

			if(own.pro =='docs' && own.isAnswer)  {
				var mainInfoWidth =(isMobile ? 461 : 161);
				var answerDayLine =box.select('.docMainInfo:0 .answerDate:0 .answerDays:0 .line:0'),
					pastDay =answerDayLine.select('.pastDay:0'),
					restDay =answerDayLine.select('.restDay:0'),
					answerDate =box.select('.docMainInfo:0 .answerDate:0 .info:0 .date:0'),
					answerTotalday =own.pastDay +own.restDay,
					pastDayWidth =(own.restDay <=0 || own.restDay ==undefined ? mainInfoWidth : (own.pastDay *((mainInfoWidth -1)/answerTotalday)) -2),
					restDayWidth =(own.pastDay <=0 || own.pastDay ==undefined ? mainInfoWidth : (own.restDay *((mainInfoWidth -1)/answerTotalday)) -4);

				if(own.pastDay ==0 && own.restDay ==0)  {
					pastDayWidth =0;
					restDayWidth =mainInfoWidth;
				}

				pastDay.width(pastDayWidth);
				restDay.width(restDayWidth);

				if(own.restDay >3)  {
					restDay.style('border-color', '#ffcbcb');
					restDay.style('background-color', '#ffcbcb');
					answerDate.style('border-color', '#ffcbcb');
					answerDate.style('background-color', '#ffcbcb');
					answerDate.style('color', '#cf4241');
				}
				else  {
					if(!noClose)  {
						restDay.style('border-color', '#ffcbcb');
						restDay.style('background-color', '#ffcbcb');
						answerDate.style('border-color', '#ffcbcb');
						answerDate.style('background-color', '#ffcbcb');
						answerDate.style('color', '#cf4241');
					}
					else  {
						restDay.style('border-color', '#ed4643');
						restDay.style('background-color', '#ed4643');
						answerDate.style('border-color', '#ed4643');
						answerDate.style('background-color', '#ed4643');
						answerDate.style('color', '#ffffff');
					}
				}
			};

			if(own.isExtended)  setActionBar();
			setSplitter();

			box.attr('modk', own.module);
			box.select('');


			function setActionBar()  {
				if(!own.actionDate)  return;
				var props = {
					userName: own.actionAuthorName,
					actionTitle: own.actionTitle,
					fullDate: own.actionFullDate
				};

				var tmp =own.actionDate.split('/');
				if(tmp.length ==1)  props['lifeTime'] =own.actionDate;
				else  {  //Date time форматтай бол
					tmp =own.actionDate.split(' ');
					props['date'] = {
						day: tmp[0],
						time: tmp[1]
					};
				};

				box.select('.box').prepend('<ActionBar>', props);
			};

			function setSplitter()  {
				box.append('<div class="splitter">', [
					['<div>'],
					['<div>'],
					['<div>']
				]);
			};

			function mkHeadProps()  {
				var props = {
					type: own.module || "forum",
					fullDate: own.fullDate
				};

				if(!own.isExtended)  props['title'] =own.actionTitle;
				if(own.hasOwnProperty('authorName'))  props['userName'] =own.authorName;
				if(own.hasOwnProperty('authorApp'))  props['userApp'] =own.authorApp;
				if(own.hasOwnProperty('authorIcon'))  props['userIcon'] =own.authorIcon;
				if(own.hasOwnProperty('date'))  props['date'] =own.date;
				if(own.hasOwnProperty('pro'))  props['pro'] =own.pro;
				return props;
			};

			function mkShiftProps()  {
				var props ={
					shiftInfo: own.shiftInfo,
					shiftCnt: own.shiftCnt
				};
				if(own.pro =='time_access')  props['timeRequestType'] =own.timeRequestType;
				return props;
			};

			function mkMainProps()  {
				var props = {
					title: own.title,
					description: own.description,
				};

				if(own.hasOwnProperty('labels'))  props['labels'] =own.labels;
				return props;
			};

			function mkCPprops()  {
				var props ={};
				if(own.hasOwnProperty('comments'))  props['replies'] =own.comments;
				if(own.hasOwnProperty('holder'))  props['holder'] =own.holder;
				if(own.hasOwnProperty('totalReplyCnt'))  props['totalReplyCnt'] =own.totalReplyCnt;
				if(own.hasOwnProperty('upReplyCnt'))  props['upReplyCnt'] =own.upReplyCnt;
				if(own.hasOwnProperty('targetId'))  props['targetId'] =own.targetId;

				props['loader'] =loadMoreComments;
				props['onPlusClick'] =onPlusClick;
				props['onPlusCountClick'] =onPlusCountClick;
				props['onSendComment'] =onSendComment;
				props['onRemoveComment'] =onRemoveComment;
				return props;
			};

			function mkPollProps()  {
				if(!own.hasOwnProperty('poll'))  return  {};
				own.poll['onChange'] =onVote;
				return own.poll;
			};

			function mkFooterProps()  {
				var props ={
					plusProps: mkPlusProps(),
					commentCnt: own.commentTotalCnt,
					hasAlert: false
				};

				if(own.pro =='docs')  {
					props['hasUsers']  =true;
					props['hasAlert']  =true;
					props['usersText'] ='Харж буй';
					props['commentText'] ='Явцын тэмдэглэл';
					props['users'] =own.sharedUser;
					props['usersTotalCnt'] =own.sharedUserCnt;
				}

				return props;
			};

			function mkPlusProps()  {
				var props ={
					itemId: own.itemId,
					itemType: own.module,
					count: own.plusCnt,
					onPlusClick: onPlusClick,
					onPlusCountClick: onPlusCountClick
				};
				if(own.hasOwnProperty('hasMe'))  props['hasMe'] =own.hasMe;
				return props;
			}

			var mailReply =box.select('.footer:0 .mailReply:0'),
				mailReplyAll =box.select('.footer:0 .mailReplyAll:0'),
				mailForward =box.select('.footer:0 .mailForward:0'),
				participants =box.select('.footer:0 .participants:0'),
				alerts =box.select('.footer:0 .alerts:0');

			mailReply.overHint({
				hint: "Хариулах",
				side: "bottom"
			});

			mailReplyAll.overHint({
				hint: "Бүгдэд хариулах",
				side: "bottom"
			});

			mailForward.overHint({
				hint: "Засаж илгээх",
				side: "bottom"
			});

			alerts.overHint({
				hint: "Санамж үүсгэх",
				side: "bottom"
			});

			let origin =window.location.ancestorOrigins[0];
			mailReply.click(function()  {
				// window.parent.postMessage([{typeModule: 'mailReply', itemId: own.itemId}], 'http://localhost/able_2021');
				// window.parent.postMessage([{typeModule: 'mailReply', itemId: own.itemId}], 'https://time.able.mn');
				window.parent.postMessage([{typeModule: 'mailReply', itemId: own.itemId}], origin);
			});

			mailReplyAll.click(function()  {
				// window.parent.postMessage([{typeModule: 'mailReplyAll', itemId: own.itemId}], 'http://localhost/able_2021');
				// window.parent.postMessage([{typeModule: 'mailReplyAll', itemId: own.itemId}], 'https://time.able.mn');
				window.parent.postMessage([{typeModule: 'mailReplyAll', itemId: own.itemId}], origin);
			});

			mailForward.click(function()  {
				// window.parent.postMessage([{typeModule: 'mailForward', itemId: own.itemId}], 'http://localhost/able_2021');
				// window.parent.postMessage([{typeModule: 'mailForward', itemId: own.itemId}], 'https://time.able.mn');
				window.parent.postMessage([{typeModule: 'mailForward', itemId: own.itemId}], origin);
			});

			alerts.click(function()  {
				// window.parent.postMessage([{typeModule: 'alert', itemId: own.itemId, pro: 'mail', mod: 'inbox'}], 'http://localhost/able_2021');
				// window.parent.postMessage([{typeModule: 'alert', itemId: own.itemId, pro: 'mail', mod: 'inbox'}], 'https://time.able.mn');
				window.parent.postMessage([{typeModule: 'alert', itemId: own.itemId, pro: 'mail', mod: 'inbox'}], origin);
			});

			participants.select('UsersGroupTemp .count').click(function()  {
				var toUser =adom(this).parents('.toUser', 4),
					ccUser =adom(this).parents('.ccUser', 4),
					bccUser =adom(this).parents('.bccUser', 4),
					data =[],
					type =(toUser.length ? 'is_to' : (ccUser.length ? 'is_cc' : (bccUser.length ? 'is_bcc' : '')));

				data.push({
					typeModule: 'userList',
					data: {
						name: 'Хүлээн авагчид',
						file: 'cHJvZ3JhbXhvcGMwL21haWwvZnVuY3Rpb24ucGhw',
						obj: 'bWFpbEb1ZmN0aW9uc0NsYXNz',
						fn: 'cmVZhFVzZXJz',
						args: {
							itemId: own.itemId,
							type: type
						}
					}
				});
				let origin =window.location.ancestorOrigins[0];

				if(type)  window.parent.postMessage(data, origin);
			});
		};

		init();
		event();
	};

};
