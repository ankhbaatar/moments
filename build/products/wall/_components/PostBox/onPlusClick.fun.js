

function fun()  {
	return function onPlusClick(setCount, datas)  {
		var data ={
			pro: own.pro,
			mod: own.module,
			itemId: (datas.itemType == 'gallery' ? datas.itemId.split(',')[0] : datas.itemId),
			setCount: setCount,
			add: datas.add,
			type: (datas.itemType == 'comment' ? 'comment' : 'post'),
		}

		if(data.itemType !='comment')  data['authorId'] =own.authorId;
		execAction("root/wall", "plus", [data]);
	};

};
