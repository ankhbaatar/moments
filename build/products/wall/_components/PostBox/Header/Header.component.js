

	require([
		"_components/Able/UserInfo",
		"./Header.css", (isMobile ? 
		"./Header.mobile.css" : "")
	]);

function component()  {
	return function HeaderComponent(box, option)  {
		var own =this;  /*
			.title: (String) Хэлэлцүүлэг нэмлээ гэх мэт хийгдсэн үйлдлийн гарчиг */
		this.userName ='Г.Цогтбаяр';
		this.userApp ='Програмист';  /*
			.userIcon: (string)
			.pro: (String)*/
		this.date ='36 мин';
		this.fullDate ="21/06/18 - 18:36";
		this.type ='forum';
		own =extend(own, option);


		function init()  {
			var userInfo = {
				name: own.userName,
				app: own.userApp,
				icon: {
					type: "temp",
					borderOpacity: 0.08,
					unit: isMobile ? "vw" : "px",
					size: isMobile ? 10 : 35,
					bw: isMobile ? 0.2 : 1
				}
			};

			var hasTitle =own.hasOwnProperty('title');
			var hasUserIcon =own.hasOwnProperty('userIcon') && own.userIcon;
			if(hasTitle && !isMobile)  userInfo['label'] ="- "+ own.title;
			if(hasUserIcon)  userInfo.icon =extend(userInfo.icon, own.userIcon);

			var path =FrontEndPath +"/wall/_components/PostBox/Header",
				icon =path +'/'+ own.type +'.png';

			box.html([
				['<UserInfo appMaxw:"260">', userInfo],
				['<div class="rightProps">', [
					['<div class="lifeTime">', [own.date]],
					['<div class="modIcon '+ own.pro +'">', [
						['<svg class="iconBg" viewBox="0 0 28 28">', [
							['<path class="box" d="M0,14C0,3.5,3.5,0,14,0s14,3.5,14,14s-3.5,14-14,14S0,24.5,0,14">'],
							['<path class="active" d="M0,13C0,3.25,3.25,0,13,0s13,3.25,13,13s-3.25,13-13,13S0,22.75,0,13">']
						]],
						['<div class="imgIcon '+ own.type +'">', [
							['<img src="'+ icon +'">']
						]]
					]]
				]]
			]);
		};

		function event()  {
			box.select(".lifeTime:0", 2).overHint({
				hint: own.fullDate,
				side: 'right'
			});

			box.select('.modIcon:0', 2).click(function()  {
				var RootData =box.parents('PostBox:0', 4).data(),
					data ={
						typeModule: 'windowOpen',
						pro: RootData.encPro,
						mod: RootData.encModule,
						itemId: RootData.itemId
					};
				let origin =window.location.ancestorOrigins[0];
				if(RootData.readOnly)  {
					var originUrl =window.location.ancestorOrigins[0];
					if(originUrl =='https://www.able.mn' || originUrl =='https://intranet.gov.mn' || originUrl =='http://localhost')  window.open('https://store.able.mn/_alerts');
					else  window.open('https://store.able.mn/_moments');
				}
				else  {
					if(!isMobile)  window.parent.postMessage([data], origin);
				}
			});
		};

		init();
		event();
	};

};
