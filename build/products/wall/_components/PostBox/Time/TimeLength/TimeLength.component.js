

	require([
		".._components/PostBox/Time/CirclePercentNew",
		".._components/PostBox/Time/Time",
		".._components/PostBox/Time/Dates",
		"./TimeLength.css", (isMobile ? 
		"./TimeLength.mobile.css" : "")
	]);

function component()  {
	return function TimeLengthComponent(box, option)  {
		var own =this,
			sdate =null,  //Эхлэн огноо (date object)
			edate =null,  //Дуусах огноо (date object)
			minutes =0;  //Нийт үргэлжлэх минут

		this.stime ="09:00";  //Эхлэх огноо (24 цагийн форматтай)
		this.etime ="12:45";  //Дуусах огноо (24 цагийн форматтай)
		this.days =[
			"2022/2/9",
			"2022/2/10"
		];
		own =extend(own, option);


		function init()  {
			mkLength();
			var percent =(minutes/60)*30*100/360,  //30: нэг цагт хамаарах градус
				hours =Math.floor(minutes/60),
				mins =minutes%60; //Задгай минут

			var state ="";
			if(mins ==0)  state ="noMins";
			if(hours ==0)  state ="noHours";

			box.append([
				['<div class="panel">', [
					['<div class="panelTitle">', ["Нэг өдөрт"]],
					['<div class="panelBody">', [
						['<div>', [
							['<Time>'],
							['<CirclePercentNew color:"#0d9dc4">', {
								unit: isMobile ? 'vw' : '',
								size: isMobile ? 23 : 110,
								thick: isMobile ? 1.2 : 5,
								percent: percent -1,
								startDegree: mkStartDegree()
							}]
						]],
						['<div class="timeInfo">', [
							['<div>', [
								['<div class="stime">', [own.stime]],
								['<div class="etime">', [own.etime]]
							]],
							['<svg viewBox="0 0 15 45">', [
								['<path d="M8.998,3h-8L1,5h7.998c2.206,0,4,1.794,'+
								  '4,4v27c0,2.206-1.794,4-4,4H4v-3l-4,4l4,4v-3h4.998 c3.314,0,'+
								  '6-2.687,6-6V9C14.998,5.687,12.312,3,8.998,3z">']
							]]
						]],
						['<div class="lengthBox '+ state +'">', [
							['<div class="hours">', [hours]],
							['<div class="info">', [
								['<div class="label">', ["цаг"]],
								['<div class="mins">', [mins]]
							]],
							['<div class="label min">', ["мин"]]
						]]
					]]
				]],
				['<div class="panel">', [
					['<div class="panelHead">', [
						['<div class="panelTitle">', ["Өдрүүд ("+ own.days.length +")"]],
						['<div class="line">']
					]],
					['<Dates>', {
						days: own.days
					}]
				]]
			]);
		};

		function mkStartDegree()  {
			var sh =sdate.getHours(),  //Эхлэх цаг
				sm =sdate.getMinutes(),  //Эхлэх минут
				stime =sh +(sm*1/60);  //Эхлэх хугацааг 60 биш 1-ээр илэрхийлнэ
			return (stime -3)*30;  //3: 0 градус эхлэх цэг нь 3 цаг
		};

		function mkLength()  {  //Эхлэх дуусах огнооны хоорондын зөрүүг цаг минутаар тодорхойлно
			sdate =mkDate('stime');
			edate =mkDate('etime');
			var stime =sdate.getTime(),
				etime =edate.getTime();
			minutes =Math.abs(stime -etime)/1000/60;  //minutes
		};

		function mkDate(key)  {
			var date =new Date(),
				time =own[key].split(':');
			return new Date(date.getFullYear(), date.getMonth() +1, date.getDate(), time[0], time[1]);
		};

		init();
		setBoxSpacingStyle(box, own);
	};

};
