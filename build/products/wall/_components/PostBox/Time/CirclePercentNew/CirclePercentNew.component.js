

	require("./CirclePercentNew.css");

function component()  {
	return function CirclePercentNewComponent(box, option)  {
		var own =this;
		this.thick =5;
		this.percent =68;
		this.size =100;  /*width height
			.color: (String) stroke color
			.startDegree: (Int) Тойргийн эхлэх өнцөг
			.unit: (String) хэмжих нэгж px || vw */
		own =extend(own, option);


		function init()  {
			var unit =own.hasOwnProperty('unit') ? own.unit : 'px',
				cx =cy =own.size/2,  //x coordinat of center
				radius =own.size/2 -own.thick;

			box.append('<svg class="CPround" viewbox="0 0 100 100" '+
				  'width="'+ own.size + unit +'" '+
				  'height="'+ own.size + unit +'">', [
				['<defs>', [
					['<linearGradient id="gColor">', [
						['<stop offset="5%" stop-color="#1e64d0">'],
						['<stop offset="100%" stop-color="#00bbc9">']
					]]
				]],
				['<circle stroke="url(#gColor)" '+
				  'cx="'+ cx + unit +'" '+
				  'cy="'+ cy + unit +'" '+
				  'r="'+ radius + unit +'" '+
				  'stroke-width="'+ own.thick + unit +'">'
				]
			]);

			var Round =box.select(".CPround:0");
			if(own.hasOwnProperty('startDegree'))  Round.style('transform', 'rotate('+ own.startDegree +'deg)');
			if(own.hasOwnProperty('color'))  Round.style('stroke', own.color);

			var roundCircum =2 *radius *Math.PI,  //Тойргийн нийт урт
				roundEmpty =own.percent *roundCircum/100;  //Dash-ийн хоосон хэсгийн урт
			Round.style('stroke-dasharray', roundEmpty + unit +', 999');
		};

		init();
	};

};
