

	require([
		"_components/Able/UsersGroupTemp",
		"./Row.css", (isMobile ? 
		"./Row.mobile.css" : "")
	]);

function component()  {
	return function RowComponent(box, option)  {
		var own =this,
			RootData =box.parents("Poll:0").data(),
			Progress =null,  //Тус саналын хувийг зурж харуулах элемэнт
			On =null,  //Хувийг зурж харуулах элемэнт
			fw =isMobile ? 55 : 358,  //full width буюу 100%
			color ="little";

		this.Id =1;
		this.value ="Цаг хугацааг зогсоож чаддаг байх";
		this.voteCnt =36;  //Өгөгдсөн саналын тоо
		this.percent =58;  //Өгөгдсөн саналын нийт саналд эзлэх хувь
		this.hidden =false;  /*Нууц санал асуулга эсэх. Хэрэв тийм бол санал өгсөн users-ийг харуулахгүй
			.userCnt: (Int) санал өгсөн хэрэглэгчдийн тоо	
			.users: (Array) санал өгсөн хэрэглэгчдийн userIcon*/
		this.hasMe =false;
		own =extend(own, option);


		function init()  {
			if(RootData.expired)  box.addClass('expired');
			box.attr('id', own.Id);
			box.html([
				['<div class="value">', [own.value]],
				['<div class="line">'],
				['<div class="progress">', [
					['<div class="bar" style="display:flex">', [
						['<div class="on">', [
							['<div>']
						]],
						['<div class="percent">']
					]], (!RootData.expired ? 
					['<div class="bttn">', [  //Хугацаа дуусаагүй бол санал өгөх товчийг зурна
						['<svg class="vote" viewBox="0 0 12 9" width="12px" height="9px">', [
							['<polygon points="10.984,0 10,0 10,1 9,1 9,2 8,2 8,3 7,3 7,4 6,4 6,5 5,5 5,6 3.984,6 3,6 3,5 2,5 2,4 0.984,4 0,5 0,6 1,6 1,7 2,7 2,8 3,8 3,9 3.984,9 5,9 5,8 6,8 6,7 7,7 7,6 8,6 8,5 9,5 9,4 10,4 10,3 11,3 11,2 12,2 12,1">']
						]], (own.hasMe ? //Санал өгсөн бол цуцлах товчийг зурна
						['<svg class="cancel" viewBox="0 0 9 9" width="9px" height="9px">', [
							['<polygon points="6,3 7,3 7,2 8,2 8,1 7,0 6,0 6,1 5,1 5,2 4,2 3,2 3,1 2,1 2,0 1,0 0,1 0,2 1,2 1,3 2,3 2,4 2,5 1,5 1,6 0,6 0,7 1,8 2,8 2,7 3,7 3,6 4,6 5,6 5,7 6,7 6,8 7,8 8,7 8,6 7,6 7,5 6,5 6,4">']
						]] : [])
					]] : [])
				]]
			]);

			Progress =box.select(".progress:0");
			On =Progress.select(".on:0", 2);

			updateRow({
				percent: own.percent,
				voteCnt: own.voteCnt,
				hasMe: own.hasMe,
				users: own.users
			});

			box.data({
				updateRow: updateRow
			});
		};

		function event()  {
			var Progress =box.select(".progress:0"),
				Bttn =Progress.select(".bttn:0", 1),
				Line =box.select(".line:0");

			Progress.hover(
				function(){  Line.addClass('over'); },
				function(){  Line.removeClass('over'); }
			);

			Progress.select(".bttn.myVote", 1).overHint({
				hint: "Саналаа цуцлах",
				side: "right"
			});

			if(Bttn.length >0)  Bttn.click(function()  {
				if(RootData.expired)  return;
				if(!RootData.hasOwnProperty('onChange'))  return;

				RootData.onChange({
					Id: own.Id,
					multiple: RootData.multiple,
					hasMe: own.hasMe,
					hidden: own.hidden
				}, RootData.onResultVote);
			});

			box.select('UsersGroupTemp:0 .count').click(function()  {
				var itemId =box.parents('PostBox:0').attr('id'),
					data =[];

				data.push({
					typeModule: 'userList',
					data: {
						name: 'Санал өгсөн хэрэглэгчид',
						file: 'cHJvZ3JhbXMwcGhvL3B1YmxpYy9mdW5jdGlvbi5waHA=',
						obj: 'cHVibGlVRnjuY3Rpb25zQ2xhc3M=',
						fn: 'cmVhZFZvdGVVc2Vy',
						args: {
							itemId: itemId,
							answerId: own.Id
						}
					}
				});
				// window.parent.postMessage(data, 'http://localhost/able_2021');
				let origin =window.location.ancestorOrigins[0];
				window.parent.postMessage(data, origin);
			});
		};

		function updateRow(data)  {
			var OnCildren =On.select("div:0"),
				Percent =Progress.select(".percent:0", 2),
				Bttn =Progress.select(".bttn:0", 1),
				UsersGroupTemp =box.select('UsersGroupTemp:0'),
				width =(data.percent *fw)/100;

			if(data.percent >80)  color ="high";  else
			if(data.percent >40)  color ="middle";

			On.removeClass('little');
			On.removeClass('high');
			On.removeClass('middle');
			On.addClass(color);
			On.width(width, (isMobile ? 'vw' : 'px'));

			if(data.voteCnt)  OnCildren.text(data.voteCnt);
			else OnCildren.text();

			Percent.text(data.percent +'%');

			if(Bttn.length >0)  Bttn.removeClass('myVote');
			own.hasMe =data.hasMe;
			if(data.hasMe && Bttn.length >0)  Bttn.addClass('myVote');
			if(Bttn.length >0)  Bttn.html([
				['<svg class="vote" viewBox="0 0 12 9" width="12px" height="9px">', [
					['<polygon points="10.984,0 10,0 10,1 9,1 9,2 8,2 8,3 7,3 7,4 6,4 6,5 5,5 5,6 3.984,6 3,6 3,5 2,5 2,4 0.984,4 0,5 0,6 1,6 1,7 2,7 2,8 3,8 3,9 3.984,9 5,9 5,8 6,8 6,7 7,7 7,6 8,6 8,5 9,5 9,4 10,4 10,3 11,3 11,2 12,2 12,1">']
				]], (own.hasMe ? //Санал өгсөн бол цуцлах товчийг зурна
				['<svg class="cancel" viewBox="0 0 9 9" width="9px" height="9px">', [
					['<polygon points="6,3 7,3 7,2 8,2 8,1 7,0 6,0 6,1 5,1 5,2 4,2 3,2 3,1 2,1 2,0 1,0 0,1 0,2 1,2 1,3 2,3 2,4 2,5 1,5 1,6 0,6 0,7 1,8 2,8 2,7 3,7 3,6 4,6 5,6 5,7 6,7 6,8 7,8 8,7 8,6 7,6 7,5 6,5 6,4">']
				]] : [])
			]);

			if(!own.hidden)  {
				if(UsersGroupTemp.length)  UsersGroupTemp.remove();
				if(!data.users)  return;
				box.append('<UsersGroupTemp mtop:"5">', {
					totalCount: own.userCnt,
					icons: data.users,
					bgColor: "white",
					size: isMobile ? 7 : 22,
					unit: isMobile ? "vw" : "px",
					bw: isMobile ? 0.2 : 1,
					way: 'start'
				});
			};
		};

		init();
		event();
	};

};
