

	require([
		"./ActionBar.css", (isMobile ? 
		"./ActionBar.mobile.css" : "")
	]);

function component()  {
	return function ActionBarComponent(box, option)  {
		var own =this;  /*
			.count (Int) Хараагүй өнгөрүүлсэн үйлдлийн тоо */
		this.userName ='Г.Цогтбаяр';
		this.actionTitle ='сэтгэгдэл нэмлээ';
		this.fullDate ="";
		this.date = {
			day: "06/18",
			time: "18:36"
		}; /*
			.lifeTime (String) "36 мин" гэх мэт */
		own =extend(own, option);


		function init()  {
			box.html(
				'<div class="items">', [(own.hasOwnProperty('count') ? 
					['<div class="count">', [own.count]] : []),
					['<div class="userName">', [own.userName +" - "]],
					['<div class="actionTitle">', [own.actionTitle]]
				]
			);

			if(own.hasOwnProperty('lifeTime'))
				box.append('<div class="lifeTime textBox">', [
					['<div>', [own.lifeTime]]
				]);
			else  box.append('<div class="date textBox">', [
				['<div>', [own.date.day]],
				['<div class="dot">'],
				['<div>', [own.date.time]]
			]);

			box.select(".textBox:0", 1).overHint({
				hint: own.fullDate,
				side: 'right'
			});
		};

		init();
	};

};
