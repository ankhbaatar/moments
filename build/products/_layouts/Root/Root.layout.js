

	require([
		"_components/Able/DialogMenu",
		"./Root.css",
		"./Root.mobile.css"
	]);

function layout()  {
	return function RootLayout(box)  {
		var wh =window.innerHeight,
			currPost =null,  //Хараагүй байгаа хамгийн эхний постыг хадгална
			scrollDown =null,
			rootScrollTop =0;  //RootLayout ийн сүүлийн scrollTop ийг хадгална

		window.isMobile =Math.min(window.screen.width, window.screen.height) < 768 || navigator.userAgent.indexOf("Mobi") > -1;
		if(isMobile)  adom('body').addClass('Mobile');

		box.html('<Layout id="RootLayout">', [
			['<div class="header">', [
				['<div class="intro">', [
					['<div class="logo">', [
						['<svg viewBox="0 0 30 30">', [
							['<path d="M15,0C3.75,0,0,3.75,0,15s3.75,15,15,15s15-3.75,15-15S26.25,0,15,0z M27.702,11.65 c-4.721,0.347-5.299,3.799-5.679,10.052c0,0.441-0.357,0.798-0.797,0.798c-0.001,0-0.003,0-0.003,0c-0.001,0-0.003,0-0.003,0 c-0.001,0-0.003,0-0.004,0c-0.002,0-0.002,0-0.003,0c-0.44,0-0.797-0.356-0.797-0.798c-0.38-6.253-0.958-9.705-5.68-10.052 c-0.44,0-0.797-0.357-0.797-0.796c0-0.44,0.356-0.798,0.797-0.798c4.457-0.329,5.24-3.301,5.616-7.96 C20.356,2.055,20.541,1.5,21.221,1.5c0.679,0,0.862,0.555,0.865,0.596c0.376,4.66,1.16,7.631,5.616,7.96 c0.441,0,0.798,0.357,0.798,0.798C28.5,11.293,28.144,11.65,27.702,11.65z">']
						]]
					]],
					['<div class="text">', ["Able Moments"]],
					['<div class="version">', ["Beta"]]
				]],
				['<div class="search">', [
					['<div class="icon">', [
						['<svg class="lens" width="13px" height="13px" viewBox="0 0 13 13">', [
							['<path fill="#8F959D" d="M12.634,10.866l-1.603-1.603C11.642,8.323,12,7.204,12,6c0-3.313-2.687-6-6-6S0,2.687,0,6s2.687,6,6,6'+
							 'c1.204,0,2.323-0.358,3.263-0.969l1.603,1.603c0.488,0.488,1.28,0.488,1.769,0C13.122,12.146,13.123,11.354,12.634,10.866z M2.5,6'+
							 'c0-1.93,1.57-3.5,3.5-3.5S9.5,4.07,9.5,6S7.93,9.5,6,9.5S2.5,7.93,2.5,6z">']
						]],
						['<svg class="arrow" width="9px" height="6px" viewBox="0 0 9 6">', [
							['<path fill="#8A9099" d="M8.706,0.292c-0.39-0.39-1.022-0.39-1.413,0L4.499,3.086L1.706,0.293c-0.391-0.391-1.023-0.391-1.413,0'+
							'c-0.391,0.391-0.391,1.023,0,1.414l4.206,4.206v0v0l0,0v0l4.206-4.206C9.096,1.316,9.096,0.683,8.706,0.292z">']
						]]
					]]
				]]
			]],
			['<div class="moduleBox">']
		]);

		var RootLayout =box.select('#RootLayout:0'),
			searchButton =box.select('.search:0');

		box.height(wh);
		box.scroll(function()  {
			var FMenu =adom("DialogMenu:0");  //Filter menu
			if(FMenu.exist())  FMenu.data().close();  //Шүүлтийн менюг хаана

			var scrollTop =box.scrollTop(),
				rootHeight =RootLayout.outerHeight(),
				scrollHeight =wh +scrollTop,
				moduleData =getModuleData('root');

			if(rootScrollTop > scrollTop)  scrollDown =false;
			else  scrollDown =true;

			rootScrollTop =scrollTop;

			if(rootHeight <scrollHeight +5)  {
				if(moduleData.isLoading)   return;  //Дараагийн хуудасны дата ирээгүй байвал буцна
				if(moduleData.theEnd)   return;  //Backend ээс постууд ирж дууссан
				var page =moduleData.page,
					array =[page];

				if(moduleData.hasOwnProperty('searchData'))  array.push(moduleData.searchData);
				execAction('root/wall', 'getData', array);
			};

			if(!currPost && !moduleData.isLoading)  {
				var firstNoSeenPost =RootLayout.select('PostBox.noSeen:0', 1);
				if(firstNoSeenPost.length)  currPost =firstNoSeenPost;
			};

			if(currPost && scrollDown) checkPost();
		});

		if(isMobile)  {
			box.style('overflow-y', 'auto');
		}
		else  box.setScroll({
			width: 8,
			noTrack: true,
			margin: 3,
			resizeHolder: 'window'
		});

		searchButton.select(".icon:0").mouseEnter(function()  {
			var DialogMenu =adom("DialogMenu", 1);
			if(DialogMenu.exist())	DialogMenu.remove();

			var pos =searchButton.position(),
				dialogTop =(isMobile ? pos.top +75 : pos.top +32),
				dialogWidth =(isMobile ? 400 : 125),
				dialogHeight =(isMobile ? 510 : 165);

			adom('body').prepend('<DialogMenu>', {
				top: dialogTop,
				right: pos.right,
				width: dialogWidth,
				height: dialogHeight,
				items: [
					{ name:"Олон нийт", data:{ pro: 'public' }},
					{ name:"Өргөдөл гомдол", data:{ pro: 'docs', mod: 'complain' }},
					{ name:"Ирсэн бичиг", data:{ pro: 'docs', mod: 'received' }},
					{ name:"Захидал", data:{ pro: 'mail'}},
					{ name:"Цаг бүртгэл", data:{ pro: 'time_access'}}
				],
				onChoose: onChoose,
				rangeItem: ".search"
			});
		});

		var origin =window.location.ancestorOrigins[0];

		adom('body').mouseEnter(function()  {
			// window.parent.postMessage([{ typeModule: 'closeUserIntro' }], 'http://localhost/able_2021');
			window.parent.postMessage([{ typeModule: 'closeUserIntro' }], origin);
		});

		adom('body').click(function()  {
			// window.parent.postMessage([{ typeModule: 'closeSubMenu' }], 'http://localhost/able_2021');
			window.parent.postMessage([{ typeModule: 'closeSubMenu' }], origin);
		});

		onRequestComplete(function()  {
			if(!currPost)  {
				var firstNoSeenPost =RootLayout.select('PostBox.noSeen:0', 1);
				if(firstNoSeenPost.length)  currPost =firstNoSeenPost;
			};

			if(currPost)  checkPost();
		});

		window.addEventListener('message', function(event)  {
			// if(event.origin != 'http://localhost')  {
			// if(event.origin != 'https://www.able.mn')  {
			// 	return;
			// }
			
			if(event.data =='reload')  {
				alert('reload');
			}
			else  {
				var post =RootLayout.select('PostBox#'+ event.data.itemId +'[modk="'+ event.data.mod +'"]'),
				shift =post.select('Shift:0', 2),
				postFooter =post.select('PostFooter:0', 2);

				shift.after('<Shift>', {
					shiftInfo: event.data.momentData.shiftInfo,
					shiftCnt: event.data.momentData.shiftCnt
				});
				shift.remove();

				if(postFooter.length && event.data.momentData.sharedUser && event.data.momentData.sharedUserCnt)  {
					postFooter.data().usersUpdate(event.data.momentData.sharedUser, event.data.momentData.sharedUserCnt);
				}
			}
		}, false);


		function findNextPost(obj)  {  //Харсан төлөвт ороогүй байгаа постыг олно
			if(obj)  {
				var nextPost =obj.next();
				if(nextPost.length)  {
					if(nextPost.hasClass('noSeen'))  currPost =nextPost;
					else  findNextPost(nextPost);
				}
				else  currPost =null;
			}
		};

		function checkPost()  {  //Постыг харсан төлөвт орсон эсэхийг шалгана
			var postPos =currPost.position(),
				postTop =postPos.top,
				postBottom =postPos.bottom;

			if(postTop <=80 || postBottom <=wh-80)  {
				if(!currPost.data().hasOwnProperty('readOnly'))  {
					console.log('seendlee');
					execAction('root/wall', 'seenPost', [currPost.data()]);
					currPost.removeClass('noSeen');
					currPost.addClass('seen');
					findNextPost(currPost);
				}
			}
			else  {
				console.log('seendeegui');
			};
		};

		function onChoose(data)  {
			var Posts =RootLayout.select("PostBox", 1);
			if(Posts.length)  Posts.remove();
			setModuleData('root', {});
			execAction('root/wall', 'getData', [0, data, 'notify_byuser_k']);
			var cancelBtn =searchButton.select('.cancel:0', 1);
			if(!cancelBtn.length)  {
				searchButton.select('.icon:0', 1).addClass('searching');
				searchButton.append('<div class="cancel">', [
					['<svg width="9px" height="9px" viewBox="0 0 9 9">', [
						['<path fill="#db3c44" d="M5.914,4.5l2.792-2.792c0.391-0.391,0.391-1.023,0-1.414s-1.023-0.391-1.414,0L4.5,3.085L1.707,0.293'+
						'c-0.391-0.391-1.023-0.391-1.414,0s-0.391,1.023,0,1.414L3.085,4.5L0.293,7.292c-0.391,0.391-0.391,1.023,0,1.414'+
						's1.023,0.391,1.414,0L4.5,5.914l2.792,2.792c0.391,0.391,1.023,0.391,1.414,0s0.391-1.023,0-1.414L5.914,4.5z">']
					]]
				]);

				var cancelBtn =searchButton.select('.cancel:0', 1);
				cancelBtn.overHint({ hint:"Хайлтыг цуцлах", side:"right" });
				cancelBtn.mouseEnter(function()  {
					var DialogMenu =adom("DialogMenu", 1);
					if(DialogMenu.exist())	DialogMenu.remove();
				});

				cancelBtn.click(function()  {
					cancelBtn.remove();
					searchButton.select('.icon:0', 1).removeClass('searching');
					adom('#able DialogMenu:0').remove();
					var Posts =RootLayout.select("PostBox", 1);
					if(Posts.length)  Posts.remove();
					setModuleData('root', {});
					execAction('root/wall', 'getData', [0, null, 'notify_byuser_k']);
				});
			}
		};
	};

};
