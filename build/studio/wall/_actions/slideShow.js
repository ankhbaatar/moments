

	export function slideShow(data, slideData)  {
		fetchRequest(ApiServer +'/slideShow?pro='+ data.pro +'&mod='+ data.mod +'&itemId='+ data.itemId,  {
			method: 'GET',
			cookies: ['accessToken'],
			onResponse: onResult
		});

		function onResult(res)  {
            var items =[],
                mobileItems =[];

            res.data.each(function(image)  {
                var width =0,
                    height =0,
                    item ={
                        id: image.itemId,
                        exp: slideData.exp,
                        autorId: slideData.authorId,
                        autorName: slideData.authorName,
                        autorApp: slideData.authorApp,
                        autorIcon: slideData.authorIcon,
                        comName: slideData.comName,
                        date: slideData.date,
                        ind: data.ind,
                        url: image.path,
                        downloadUrl: image.path
                    };

                if(data.pro == 'public' && data.mod == 'gallery')  {
                    width =image.width,
                    height =image.height
                }
                else  {
                    width =data.width,
                    height =data.height
                }

                item['w'] =width;
                item['h'] =height;

                items.push(item);
                mobileItems.push({file: image.path});
            });

            if(isMobile)  {
                @('body').prepend('<SlideShow closeOutClick>', {
                    path: '',
                    currInd: data.ind,
					slides: mobileItems
				});
            }
            else  {
                var msgData ={
                    typeModule: 'imageFrame',
                    items: items,
                    isInfo: (isMobile ? false : true)
                }
    
                // window.parent.postMessage(items, 'http://localhost/able_2021');
                let origin =window.location.ancestorOrigins[0];
                window.parent.postMessage(msgData, origin);
            }
		};
	};

