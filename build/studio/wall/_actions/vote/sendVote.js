

	export function sendVote(data, method, onLoad)  {
        var method =(!method) ? 'POST' : 'DELETE';
		fetchRequest(ApiServer +'/vote',  {
			method: method,
            body: JSON.stringify(data),
			cookies: ['accessToken'],
			onResponse: onResult
		});

		function onResult(res)  {
			if(method =='POST')  {
				res.data.notify['typeModule'] ='sendNotify';
				var notify =[res.data.notify];
				if(!data.hidden)  {
					res.data.notify1['typeModule'] ='sendNotify';
					var notify1 =[res.data.notify1];
				}
				let origin =window.location.ancestorOrigins[0];
				window.parent.postMessage(notify, origin);
				if(!data.hidden)  window.parent.postMessage(notify1, origin);
			}
			var response ={
				totalVote: res.data.totalVote,
				answers: res.data.answers
			};
			onLoad(response);
		};
	};

