

	export function deleteComment(data, onLoad)  {
		fetchRequest(ApiServer +'/comment',  {
			method: 'DELETE',
            body: JSON.stringify(data),
			cookies: ['accessToken'],
			onResponse: onResult
		});

		function onResult(res)  {
			setStatusBar('done', 'Амжилттай устгалаа!');
            onLoad();
		};
	};

