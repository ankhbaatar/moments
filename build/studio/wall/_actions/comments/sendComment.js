

	export function sendComment(data, onLoad)  {
        var method =(data.id) ? 'PUT' : 'POST';
		fetchRequest(ApiServer +'/comment',  {
			method: method,
            body: JSON.stringify(data),
			cookies: ['accessToken'],
			onResponse: onResult
		});

		function onResult(res)  {
			if(method == 'POST')  {
				res.data.notify['typeModule'] ='sendNotify';
				var notify =[res.data.notify];
				// window.parent.postMessage(notify, 'http://localhost/able_2021');
				// window.parent.postMessage(notify, 'https://time.able.mn');
				let origin =window.location.ancestorOrigins[0];
				window.parent.postMessage(notify, origin);
			};
			onLoad(res.data.comment[0]);
		};
	};

