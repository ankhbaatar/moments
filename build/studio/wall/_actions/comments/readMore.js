

	export function readMore(data)  {
		fetchRequest(ApiServer +'/comment?pro='+ data.pro +'&module='+ data.module +'&itemId='+ data.itemId +'&parentId='+ data.parentId +'&guideCommId='+ data.guideCommId +'&way='+ data.way +'&notIn='+ data.notIn.join(','),  {
			method: "GET",
			cookies: ['accessToken'],
			onResponse: onResult
		});

		function onResult(res)  {
			if(data.way =="up")  res.data.reverse();
			data.onLoad(res.data);
		};
	};

