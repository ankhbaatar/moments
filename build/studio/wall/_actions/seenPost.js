

	export function seenPost(data)  {

		fetchRequest(ApiServer +'/seen',  {
			method: "POST",
            body: JSON.stringify(data),
			cookies: ['accessToken'],
            onResponse: onResult
		});

        function onResult(res)  {
			let origin =window.location.ancestorOrigins[0];
			window.parent.postMessage([{typeModule: 'newState'}], origin);
        }
	};

