

	export function setMoreLink(isOnInit)  {  //MoreLink-ийг дээшээ, доошоо ачаалах байдлаар 2 удаа зурна
		var Sign =null;  /*MoreLink-ийг устгачихаар тухайн байрлалд нь буцааж үүсгэхийн тулд 
			temp div-ээр тэмдэг тавина. Ингэж тэмдэг тавихгүй бол ямарч нөхцөлд тохирох 
			оновчтой Holder DOM олдохгүй (ComposeBox, top CommentBranch гэх мэт) байна */

		if(own.totalReplyCnt ==0)  return;
		if(More.Link)  More.Link.remove();  //Init байгуулалтын дараа load more хийхэд л Sign хийгдэнэ

		var tmp =ReplyBox.select(".sign:0", 1);
		if(tmp.length ==1)  Sign =tmp;

		if(isOnInit || More.way =="up")
			if(more.up.remainCnt >0)
				setUpLink();

		if(isOnInit || More.way =="down")
			if(more.down.remainCnt >0)
				setDownLink();

		if(Sign)  Sign.remove();
		More =null;


		function setUpLink()  {  //Дээшээгээ комментууд ачаалах линк байгуулна
			var MoreLink =@('<MoreLink class="MLup">', {
				isRoot: (own.Id ==0),
				count: more.up.remainCnt
			});

			if(!isOnInit)  {  //Эхний байгуулалтын араас loadUp хийж байгаа бол
				if(Sign)  Sign.after(MoreLink);  /*Sign үүсгэгдсэн бол түүний доогуур зурна.
					MoreLink мөчрийнхөө дээд захад биш бол түүний байрлалыг тэмдэглэж Sign хийгдсэн байх ёстой */
				else  ReplyBox.prepend(MoreLink);  //Мөчрийнхөө дээд захад зурна
			}
			else  {  //Эхний удаагийн байгуулалт бол (Энд MoreLink байхгүй)
				if(toUp)  ReplyBox.select("CommentBranch:0", 1).before(MoreLink);  //Default ComposeBox-ийн доогуур зурна
				else  ReplyBox.prepend(MoreLink);
			};

			more.up.Link =MoreLink;
			more.up.Link.click(function()  {
				More =more.up;
				loadMore("up", MoreLink.next());
			});
		};

		function setDownLink()  {  //Доошоо комментууд ачаалах линк байгуулна
			var MoreLink =@('<MoreLink>', {
				isRoot: (own.Id ==0),
				count: more.down.remainCnt
			});

			if(isOnInit)  ReplyBox.append(MoreLink);  //Эхний удаагийн байгуулалт бол (Энд MoreLink байхгүй)
			else  {  //Эхний байгуулалтын араас loadDown хийж байгаа бол Sign үүсгэгдсэн байх ёстой
				if(Sign)  Sign.after(MoreLink);  /*Sign үүсгэгдсэн бол түүний доогуур зурна.
					MoreLink мөчрийнхөө доод захад биш бол түүний байрлалыг тэмдэглэж Sign хийгдсэн байх ёстой */
				else  ReplyBox.append(MoreLink);
			};

			more.down.Link =MoreLink;
			more.down.Link.click(function()  {
				More =more.down;
				loadMore("down", MoreLink.prev());
			});
		};
	};

