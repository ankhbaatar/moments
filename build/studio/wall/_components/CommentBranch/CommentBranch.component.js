

	require([
		"_components/Able/UserIcon",
		"./ComposeBox",
		"./CommBox",
		"./Footer",
		"./MoreLink",
		"./setRootBox.fun.js",
		"./setComm.fun.js",
		"./mkLinePath.fun.js",
		"./mkLinePathUps.fun.js",
		"./setMoreLink.fun.js",
		"./loadMore.fun.js",
		"./CommentBranch.css", (isMobile ? 
		"./CommentBranch.mobile.css" : "")
	]);

	export function CommentBranchComponent(box, option)  {
		var own =this,
			isRoot =false,  //Root branch эсэх
			ReplyBox =null,  //Тус комментийн хариултуудыг агуулах бокс
			CommBox =null,
			Footer =null,
			RootData =null,  //Child comp-уудад ашиглах cross датааг
			more = {
				up: {
					way: "up",  //Load more component үйлдэл аль чиглэлд хийж байгааг хэлж өгнө (up || down)
					remainCnt: 0,  //Дэлгэцэнд ачаалагаагүй үлдсэн хариулт буюу дэд комментуудын тоо
					Link: null  //Үлдсэн комментуудыг дээшээ ачаалах линк NL
				},
				down: {
					way: "down",  //Load more component үйлдэл аль чиглэлд хийж байгааг хэлж өгнө (up || down)
					remainCnt: 0,  //Дэлгэцэнд ачаалагаагүй үлдсэн хариулт буюу дэд комментуудын тоо
					Link: null  //Үлдсэн комментуудыг доошоо ачаалах линк NL
				}
			},
			More =null;  //Load more үйлдэл аль чиглэлд хийж байгааг хэлж өгөх заагч. more.up || down-ийг авна

		this.pId =0;  //Parent comment id
		this.Id =0;  //Comment Id
		this.holder = { /*
			path: (String),
			gender: (String)
			overHint: (Object of OverHint comp) */
		};  /*Системд нэвтэрсэн байгаа буюу комментуудыг үзэж 
			байгаа хэрэглэгч. ComposeBox comp-ийн holder-ийг харна уу */
		this.author = {
			Id: 1206,
			name: "Б.Хүлэгү" /*
			icon: (Object) {
				path: (String),
				gender: (String)
				overHint: (Object of OverHint comp)
			}*/
		};
		this.date ="36 мин";
		this.fullDate ="21/07/04 - 18:36";
		this.comment ="Цэвэр агаар дүүрэн амьсгалах шиг Чиний минь үг ухаарал хайрлана";
		this.plusCnt =8;
		this.totalReplyCnt =10;  //Нийт хариу буюу дэд комментийн тоо (гэхдээ эхний түвшнийх)
		this.upReplyCnt =0;  //Зурах гэж байгаа комментуудаас дээшээ (сүүлд нэмэгдсэн) байгаа ачаалагдаагүй комментуудын тоо
		this.replies = [  /*Эхний түвшний хариу буюу дэд комментууд
			{	pId: 2,
				Id: 4,
				comment: "Ус мэт чамайг тал мэт хайрлая",
				totalReplyCnt: 1
			},
			{	pId: 2,
				Id: 7,
				comment: "Туршилт хийж байна"
			} */
		];
		this.loader =null;  /*Дэлгэцэнд зурагдаагүй бусад сэтгэгдлийг ачаалах гадаад функц
			.onPlusClick: (Function) Plus нэмж хасах api (Зөвхөн RootBranch өөртөө хадгална)
			.onPlucCountClick: (Function) Plus count click дээр ажиллах api (Зөвхөн RootBranch өөртөө хадгална)
			.onSendComment: (Function) Коммент нэмж, засах api (Зөвхөн RootBranch өөртөө хадгална)
			.onRemoveComment (Function) Коммент устгах api (Зөвхөн RootBranch өөртөө хадгална) */
		this.animate =false;  //Comment гарч ирэхдээ animate хийх эсэх. (Compose цонхноос comment нэмэх үед хэрэглэгдэнэ)
		this.onComplete =null;  //CommentBranch-ийг үүсгэх animate дуусахад ажиллах функц 
		this.targetId =0;  /*Хайж олж улаан тэмдгээр онцолж харуулах комментийн Id. 
			Энэ нь зөвхөн rootBranch л хамаарах буюу entire comment tree-ийн хувьд ганцхан байна */
		own =extend(own, option);
		var toUp =true;  //Хамгийн сүүлийн коммент дээрээ зурагдах горим горим


		function init()  {
			box.attr('id', own.Id);
			var eachData = {  //Root болон дэд мөчрүүдэд хоёуланд нь байх глобал дататай нийлүүлнэ
				Id: own.Id,
				append: append,
				onRemoveChild: onRemoveChild,
				onAppend: onAppend
			};

			isRoot =(own.Id ==0);
			if(isRoot)  setRootBox(eachData);
			else  {
				setComm(eachData);
				toUp =false;
			};

			if(!toUp)  {
				own.upReplyCnt =(own.totalReplyCnt -own.replies.length -own.upReplyCnt);
			};
			more.up.remainCnt =own.upReplyCnt;
			more.down.remainCnt =own.totalReplyCnt -own.upReplyCnt;
			More =more.down;

			ReplyBox =box.select(".CBreplyBox:0", 2);
			append(own.replies);
			onAppend('onInit');
		};

		function event()  {
			if(own.Id ==0)  return;  //Root branch-д Footer, MoreMenu гэж байхгүй тул буцна
			if(RootData.holder.Id !=own.author.Id)  return;  //Holder-ийн бичсэн коммент биш бол

			var MoreMenu =Footer.select(".CBFmoreMenu:0");
			CommBox.hover(
				function(){ if(!MoreMenu.hasClass('on'))  MoreMenu.show(); },
				function(){ MoreMenu.hide(); }
			);
			Footer.hover(
				function(){ if(!MoreMenu.hasClass('on'))  MoreMenu.show(); },
				function(){ MoreMenu.hide(); }
			);
		};


	//--Append функцууд
		function append(replies, onAdd)  {  //Тус комментийн дэд комментуудыг байгуулна
			var onBranch =!isRoot,  //Дэд мөчир дээр байгаа эсэх
				onInit =true;  //Байгуулалтын горимд зурж байгаа эсэх

			if(onAdd)  {  //Байгуулалт дууссаны дараа хэрэглэгч шинэ коммент нэмж байгаа бол
				More =toUp ? more.up : more.down;
				onInit =false;
			};

			if(ReplyBox.length ==0)  /*Тухайн branch-д эхний хүүхдийг нь нэмэх үед 
				ReplyBox утгаа аваагүй байх тул түүнийг тодорхойлж өгнө */
				ReplyBox =box.select(".CBreplyBox:0", 2);

			var Holder =null,  //Сүүлд нэмэгдсэн комментийг хэлж өгнө
				ComposeBox =ReplyBox.select("ComposeBox:0", 1),
				noCompose =(ComposeBox.length ==0);  //ComposeBox байхгүй эсэх

			replies.each(function(comm)  {
				comm['loader'] =own.loader;  //loader функийг хүүхдүүд рүүгээ дамжуулна
				var NewComm =@('<CommentBranch>', comm);

				if(onInit)  More.remainCnt -=1;
				if(More.way =="down")  {  //Жагсаалтын төгсгөлд зурах бол
					if(toUp)  ReplyBox.append(NewComm);  //Сүүлийн бичлэг дээрээ байрлах горим бол (Root мөчир)
					else  {  //Сүүлийн бичлэг доороо байрлах горим бол (Дэд мөчир)
						if(!noCompose)  ComposeBox.before(NewComm);
						else  {
							if(!Holder)  {
								if(onInit && more.down.Link)  more.down.Link.before(NewComm);
								else  ReplyBox.append(NewComm);
							}
							else  Holder.before(NewComm);
							Holder =NewComm;
						};
					};

					return;
				};

			//--Жагсаалтын эхэнд зурах бол (Load up). Энэ хэсэг рүү зөвхөн Load More link-дарснаар л ирэх болно
				if(toUp)  {  //Сүүлийн бичлэг дээрээ байрлах горим бол (Root мөчир)
					if(onAdd)  ComposeBox.after(NewComm);  //Root мөчирт нэмэх бол
					else  {
						if(!Holder)  Holder =more.up.Link;
						Holder.after(NewComm);
						Holder =NewComm;
					};
				}
				else  {  //Сүүлийн бичлэг доороо байрлах горим бол (Дэд мөчир)
					if(noCompose)  ReplyBox.prepend(NewComm);
					else  {  //Compose нээгээд орхисон байгаа бол
						if(noCompose)  ComposeBox.before(NewComm);  //Дэд мөчирт үйлдэл хийсэн бол
						else  ReplyBox.prepend(NewComm);
					};
				};
			});
		};

		function onAppend(action)  {  //onInit || onAdd || onExtend
			if(action =='onInit')  {  //onInit буюу анх зурагдах үед бол mkLinePathUps дуудахгүй
				setMoreLink(true);
				mkLinePath('onInit');  //Тус CommBranch өөрийнхөө line path-ийг анх удаа зурна
				return;
			};

			if(action =='onAdd')  own.totalReplyCnt +=1;  //Коммент нэмэх үед MoreLink дахин зурахгүй
			else  setMoreLink();

			mkLinePath('onExtend');  //Шинэ хүүхдүүд зурагдсан тул өөрийнхөө line path-ийг тохируулна
			mkLinePathUps('onExtend');
		};


	//--Update функцууд
		function update(newProps)  {
			CommBox.data().update(newProps);  //CommBox comp-ийн update функцийг дуудна
			onUpdate();
		};

		function onUpdate()  {
			mkLinePath('onUpdate');  //Шинэ хүүхдүүд зурагдсан тул өөрийнхөө line path-ийг тохируулна
			mkLinePathUps('onExtend');
		};


	//--Remove функцууд
		function remove()  {  //Тус коммент өөрийгөө устгана (backend api руу callBack-аар дуудагдана)
			var ParentComm =box.parents("CommentBranch:0", 3);
				onRmCh =ParentComm.data().onRemoveChild;  //Тус мөчрийн parent-ийн onRemoveChild функц

			box.addClass("CBonRemove");
			box.height(box.outerHeight());
			box.animate({ height: 0 }, 150, function()  {
				var Parents =box.parents("CommentBranch");  //Доорх мөрөнд box.remove хийх тул өмнө нь Parents-ийг тодорхойлно
				box.remove();
				onRmCh(Parents);
			});
		};

		function onRemoveChild(Parents)  {  //Тус мөчрийн эхний түвшний хүүхдүүдээс устгагдсаны дараа зохих тохироо хийнэ
			own.totalReplyCnt -=1;
			mkLinePathUps('removeChild', Parents);
		};

		init();
		event();
	};

