

	require([
		"_components/Able/Inputs/TextAreaBlue",
		"_components/Able/Icons/CancelIcon",
		"./ComposeBox.css", (isMobile ? 
		"./ComposeBox.mobile.css" : "")
	]);

	export function ComposeBoxComponent(box, option)  {
		var own =this,
			RootLib =box.parents("CommentBranch.root:0").data(),
			ParentLib =box.parents("CommentBranch:0", 3).data(),
			Textarea =null,
			CancelIcon =null,
			isEdit =false,  //Edit горимд байгаа эсэх
			isNew =true;  //Шинийг нэмэх горимд байгаа эсэх

		this.Id =0;  //Хэрэв засах үйлдэл бол засагдаж буй comment Id
		this.pId =0;  //Parent comment Id
		this.value ="";
		this.parentHolderName ="";  //Parent comment holder user name
		this.holder ={  /*Коммент бичих гэж буй хэрэглэгчийн мэдээлэл
			Id: (Int) holder id
			name: (String) holder name
			icon: (String) holder userIcon path */
		};
		this.height =50;
		this.onCancel =null;
		this.animate =false;  //Гарч ирэлт animate - тэй эсэх. Moments init дээр false байна
		own =extend(own, option);


		function init()  {
			if(own.value !="")  {
				isEdit =true;
				isNew =false;
			};

			if(isEdit)  box.addClass('edit');
			if(own.animate)  box.addClass('prepare');
			box.html([(isNew ? 
				['<UserIconTemp>', mkHolderProps()] : []),
				['<TextAreaBlue noResize height:"'+ own.height +'">', {
					placeHolder: "Таны сэтгэгдэл ...",
					value: own.value || own.parentHolderName
				}], (own.pId >0 ? //Root мөчир биш бол
				['<CancelIcon>'] : [])
			]);

			Textarea =box.select("TextAreaBlue:0 textarea:0");
			CancelIcon =box.select("CancelIcon:0", 1);

			if(own.animate)	 Textarea.focus();
			animate();


			function mkHolderProps()  {
				var isReply =!box.hasClass('root'),
					props = {
						unit: isMobile ? "vw" : "px",
						size: isMobile ? 9 : 35,
						bw: isMobile ? 0.2 : 1
					};

				if(isReply)  props.size =isMobile ? 7 : 22;
				if(own.holder.hasOwnProperty('icon'))  props =extend(props, own.holder.icon);
				return props;
			};

			function animate()  {  //Comment-ийг animate хийж гаргаж ирнэ
				if(!own.animate)  return;
				var h =Textarea.outerHeight();  //offset height
				box.animate({ height: h }, 60, function()  {
					box.removeClass('prepare');
					box.removeAttr('style');  //element style-аар өгсөн height-ийг устгана
					setLinePath();
				});
			};
		};

		function sendComment()  {  //Backend рүү коммент нэмэх, засах хүсэлт илгээх гадаад функц дуудна
			if(!RootLib.hasOwnProperty('onSendComment'))  return;
			var props = {
				comment: Textarea.val().trim()
			};

			if(props.comment =="")  return;
			if(isNew)  props['parentId'] =own.pId;
			else  props['Id'] =own.Id;
			RootLib.onSendComment(props, proceed);
		};

		function proceed(item)  {  //Backend api-гаас үр дүнг хүлээж аваад гүцээнэ
			if(isNew)  {  //Add үйлдэл бол
				if(own.pId ==0)  Textarea.val("");
				else  box.remove();

				item['animate'] =true;
				item['onComplete'] =function(){  ParentLib.onAppend('onAdd');  };
				ParentLib.append([item], true);
				return;
			};

		//--Edit үйлдэл бол
			box.prev().show();  //CommBox
			box.remove();
			ParentLib.update(item);
		};

		function setLinePath(task)  {  //Parent CommentBranch-даа path line үүсгээд parents-ийн path line-ийг update хийнэ
			if(own.pId ==0)  return;
			ParentLib.mkLinePath(task || 'composeReply');
			ParentLib.mkLinePathUps('onExtend');
		};

		function event()  {
			Textarea.keyDown(function(e)  {
				if(e.which ==13)  {
					sendComment();
					e.preventDefault();
				};
			});

			CancelIcon.mouseDown(function()  {
				box.remove();
				if(own.onCancel)  own.onCancel();
				if(isNew)  //Нэмэх гэснээ цуцалж байгаа бол
					setLinePath('cancelCompose');
			});
		};

		init();
		event();
	};

