

	export function mkLinePath(action, depth)  {  /*
		Энэ функц нь тухайн commentBranch - ийнхаа 
		За энэ функц дараах дүрмээр ажиллана
		Тухайн комментийнхоо хамгийн доор байгаа обьектийг заана.
		Үүнд:
			1 Хүүхдүүдийг бүрэн зураагүй байгаа бол bottom MoreLink-ийг заана
			2 Бүх хүүхдүүд зурагдсан байгаа бол bottom хүүхдийг нь заана
			3 Хүүхэдгүй бол line байхгүй.
				Хүүхэдгүй үед compose гарч ирвэл түүнийг заана
	*/
		if(depth ==undefined)  depth =0;
		var botIsMoreLink =false,  //Доошоо ачаалах MoreLink байгаа бөгөөд тэр нь тухайн мөчрийн хамгийн доорх Item эсэх
			myMore =(own.pId ==0) ? more.up : more.down,  //Горимоос хамаарч down object оо өөрөөр авна. Гэхдээ яг тодорхой ойлгосонгүй!
			onTest =false;

		if(onTest)  console.log('mkLinePath: '+ own.Id +' > '+ action, depth);
		if(myMore.remainCnt >0)  {
			let botItem =myMore.Link.next();  //MoreLink - ийн доорх Item
			if(botItem.length ==0)  botIsMoreLink =true;
		};

	//--Ямар нэгэн үйлдэл хийхгүй шууд буцах нөхцлүүд
		if(own.Id ==0)  return;  //Root branch-ийн хувьд path line байхгүй
		if(depth ==0)  {
			if(action =="onUpdate" && own.totalReplyCnt ==0)  {  console.log('	returned');  return;  };
		}
		if(depth >0 && botIsMoreLink)  {
		//Тухайн комментийн өөрийнх нь branch дотор үйлдэл хийгдсэн бол
		//--Тухайн комментийн хүүхдүүдийнх нь branch дотор үйлдэл хийгдсэн бол
			if(action =="onExtend")  {  console.log('	returned');  return; }  //Яг доор нь шинэ хүүхэд нэмэгдсэн бол
		};

		if(action =='onInit' && own.totalReplyCnt ==0)  {  /*CommBox нь дөнгөж зурагдаад 
			байж байгаа бөгөөд хүүхэдгүй бол path line зурах шаардлагагүй */
			if(onTest)  console.log('	onInit & noChild return');
			return;
		};


	//--Зохих үйлдэл гүйцэтгэх нөхцлүүд
		var Tree =box.select(".CBtree:0"),
			PathLine =Tree.select(".line:0", 1);

		if(PathLine.length ==0)  {  //PathLine байхгүй бол үүсгэнэ
			create();
			if(!botIsMoreLink)  LinkLastAuthor();  //Bottom more link байхгүй бол
			if(onTest)  console.log('	created & returned');
			return;
		};

		if(action =='cancelCompose' || action =='removeChild')  //Устгах эсэхийг шийднэ. Устгасан бол буцаана
			if(remove())  return;

		if(action =='composeReply')  LinkLastAuthor();
		if(botIsMoreLink)  PathLine.removeStyle('height');
		else  LinkLastAuthor();


		function create()  {  /*Path line-ийг үүсгэнэ. Анх үүсэхдээ өндрөө тухайн 
			branch box-ийнхоо хувьд тултал авах бөгөөд тэр нь bottom moreLink-тэй яг таарна */
			Tree.append([
				['<div class="line">'],
				['<div class="bot">']
			]);

			PathLine =Tree.select(".line:0");
		};

		function remove()  {  //Тус комментийн path line-ийг устгана
			if(more.up.remainCnt >0)  return;  //Ачаалаагүй үлдсэн хүүхэд байгаа буюу MoreLink байгаа бол
			if(own.totalReplyCnt >0)  return;  //Дэд коммент байгаа л бол path line байна

			Tree.select(".line:0").remove();
			Tree.select(".bot:0").remove();
			return true;
		};

		function LinkLastAuthor()  {  //Хамгийн доор байрлах authorIcon руу заана
			if(onTest)  console.log('	Link last author');
			var UIbot =Tree.select("UserIconTemp:0").position().bottom,  //My user icon bottom line
				LastUI =Tree.next().select("UserIconTemp:last", 5);  //Last user icon

			if(LastUI.length ==0)  return;

			var oldh =PathLine.outerHeight(),
				lastUITop =LastUI.position().top,
				lineh =lastUITop -UIbot +(isMobile ? 2 : 0);

			if(lineh >0 && oldh !=lineh)  {
				var starth =lineh +(lineh >oldh ? -10 : 10);
				PathLine.addClass('linkedIcon');
				PathLine.height(starth);  //Animate эхлүүлэх өндрийг өгнө
				PathLine.animate({ height: lineh }, 150);
			};
		};
	};

