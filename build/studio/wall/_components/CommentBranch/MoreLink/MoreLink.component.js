

	require("./MoreLink.css");

	export function MoreLinkComponent(box, option)  {
		var own =this;
		this.isRoot =false;
		this.count =0;
		this.onClick =null;
		own =extend(own, option);


		function init()  {
			var msg =(own.isRoot) ? "сэтгэгдэл" : "хариу";
			box.html([
				['<svg viewBox="0 0 12 11">', [
					['<path d="M11.707,5.28L7.719,1.292c-0.391-0.391-1.024-0.39-1.414,0C5.915,1.683,5.914,2.316,6.304,2.707L8.597,5H4'+
					 ' C2.897,5,2,4.103,2,3V1c0-0.552-0.448-1-1-1S0,0.448,0,1v2c0,2.209,1.791,4,4,4h4.584l-2.28,2.28'+
					 ' c-0.391,0.391-0.39,1.024,0.001,1.415c0.39,0.39,1.023,0.391,1.414,0l3.987-3.987C11.903,6.511,12,6.252,11.999,5.994'+
					 ' C12,5.735,11.903,5.477,11.707,5.28z">']
				]],
				['<div>', [own.count +" "+ msg]]
			]);
		};

		function event()  {
			box.click(function()  {
				box.addClass('MLloading');
				box.select("div:0").html("Ачаалж байна ...");
			});
		};

		init();
		event();
	};

