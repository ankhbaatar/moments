

	export function loadMore(way, CommBox)  {  /*Load more comments.
		тухайн мөчрийн parent comment Id болон түүний хүүхдүүдээс ачаалах гэж буй чиглэлээс 
		хамааран хамгийн эхэнд эсвэл сүүлд ачаалагдсан байгаа comment Id-г тодорхойлж 
		loader callBack функц руу илгээнэ */
		if(!own.hasOwnProperty('loader'))  return;

		var	guideCommId =0;  /*Энэ нь дээшээ комментуудыг ачаалах үед 
			байгуулагдсан байгаа комментуудын хамгийн дээрх комментийн Id-г, 
			доошоо ачаалах бол хамгийн доорх комментийн Id-г хэлж өгнө */

		if(CommBox.length ==1)
			if(CommBox.tagName() =='CommentBranch')  /*Энд нэг ч коммент 
				нээгээгүй үед ComposeBox гэх мэт DOM байж болох тул шалгав */
				guideCommId =parseInt(CommBox.attr("id"));

		var commsId =[];  //Тухайн CommBranch-ийн эхний түвшний хүүхдүүд
		ReplyBox.select("CommentBranch", 1).each(function(comm)  {
			let Id =parseInt(comm.attr('id'));
			commsId.push(Id);
		});

		own.loader({
			parentId: own.Id,
			friends: commsId,
			guideCommId: guideCommId,
			way: isRoot ? way : (way =="down" ? "up" : "down")
		}, function(items)  {
			if(More.way =="up")  /*MoreLink мөчрийнхөө дээд захад биш бол түүний байрлалыг тэмдэглэж Sign хийнэ.
				Байгуулалт хийгдсэний дараа шинэ коммент нэмэхэд moreLink-ийн дээгүүр орж болно */
				if(More.Link.prev().length ==1)
					More.Link.before('<div class="sign">');

			if(More.way =="down")  /*MoreLink мөчрийнхөө доод захад биш бол түүний байрлалыг тэмдэглэж Sign хийнэ.
				Байгуулалт хийгдсэний дараа шинэ коммент нэмэхэд moreLink-ийн доогуур орж болно */
				if(More.Link.next().length ==1)
					More.Link.after('<div class="sign">');

			append(items);
			onAppend('onExtend');
		});
	};

