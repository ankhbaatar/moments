

	export function onRemoveComment(commentId, onLoad)  {
		var data = {
			commentId: commentId
		};

		setStatusBar('exec');
		execAction('root/wall', 'deleteComment', [data, onLoad]);
	};

