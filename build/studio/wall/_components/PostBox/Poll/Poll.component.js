

	require([
		"_components/Caption",
		"_components/Able/CirclePercent",
		"./Row",
		"./Poll.css", (isMobile ? 
		"./Poll.mobile.css" : "")
	]);

	export function PollComponent(box, option)  {
		var own =this;
		this.totalVote =50;
		this.remainDays ='08/26 (3 хоног)';
		this.answers = [
			{ voteCnt: 13, hidden: true, hasMe: true },
			{ value: 'Дуртай үедээ үл үзэгдэгч болох, Өнгөрсөн цаг хугацаагаар аялах (гэхдээ өөрчлөх боломжгүй)' },
			{ voteCnt: 25 }
		]; /*
		   	.onChange: (Function) санал өгөх үед хийгдэх үйлдэл
			.expired: (Boolean) санал өгөх хугацаа дууссан
			.secret: (Boolean) Нууцлалтай санал асуулга
			.multiple: (Beoolean) Олон сонголттой 
			.allUserCnt: (Int) постыг харах бүх хэрэглэгчдийн тоо */
		own =extend(own, option);


		function init()  {
			RootData = {  //Child comp-уудад ашиглах датааг зөвхөн root мөчирт үүсгэнэ
				multiple: own.hasOwnProperty('multiple'),
				expired: own.hasOwnProperty('expired'),
				onChange: own.onChange || function(){},
				onResultVote: onResultVote
			};

			var mainPercent =((own.totalVote *100)/ own.allUserCnt).toFixed(0);
			box.data(RootData);
			box.html([
				['<div class="mainInfo">', [
					['<div class="details">', [
						['<div class="labels">', ["Нийт санал<br/>Нууцлалын төрөл<br/>Хариулах хэлбэр<br/>Дуусах хугацаа"]],
						['<div class="values">', [
							own.totalVote +" санал<br/>"+
							(own.hasOwnProperty('secret') ? "Нууцлалтай" : "Нээлттэй")+ "<br/>"+
							(own.hasOwnProperty('multiple') ? "Олон сонголттой" : "Нэг сонголттой")+ "<br/>"+
							own.remainDays
						]]
					]], (isMobile ? [] : 
					['<CirclePercent mleft:"20" mtop:"11" percent:"78" size:"small">', {
						percent: mainPercent
					}])
				]], (isMobile ? 
				['<CirclePercent percent:"78" size:"mlarge" thick:"12">', {
					percent: mainPercent
				}] : []),
				['<div class="rows">']
			]);

			if(own.hasOwnProperty('answers'))  {
				own.answers.each(function(row)  {
					var percent =(row.voteCnt *100)/own.totalVote;
					row['hidden'] =(own.hasOwnProperty('secret') ? true : false);
					row['percent'] =own.totalVote ? (row.voteCnt ? (percent != 100 ? percent.toFixed(1) : percent) : 0) : 0;
					box.select('.rows:0').append('<Row>', row);
				});
			};
		};

		function onResultVote(data)  {
			data.answers.each(function(row)  {
				var percentOld =(row.voteCnt *100)/data.totalVote,
					percent =data.totalVote ? (row.voteCnt ? (percentOld != 100 ? percentOld.toFixed(1) : percentOld) : 0) : 0,
					rowData ={
						percent: percent,
						voteCnt: row.voteCnt,
						hasMe: row.hasMe
					};

				if(row.users)  rowData['users'] =row.users;
				var updateRow =box.select('Row#'+ row.Id).data('updateRow');
				updateRow(rowData);
			});
		};

		init();
	};

