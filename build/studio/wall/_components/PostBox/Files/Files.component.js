

	require([
		"_components/Able/FileItem",
		"./Files.css", (isMobile ? 
		"./Files.mobile.css" : "")
	]);

	export function FilesComponent(box, option)  {
		var own =this;
		this.files =[
			{ name:"Main design", type:"doc" },
			{ name:"Main design", type:"xls" },
			{ name:"Main design", type:"psd" }
		];
		own =extend(own, option);


		function init()  {
			box.html([
				['<div class="title">', ["Хавсралтууд ("+ own.files.length +")"]],
				['<div class="files">']
			]);

			var FilesBox =box.select(".files:0");
			own.files.each(function(file)  {
				FilesBox.append('<FileItem type:"'+ file.type +'">', {
					name: file.name,
					dir: file.dir,
					size: (isMobile ? 'large' : 'small'),
					preview: (isMobile ? false : true)
				});
			});
		};

		init();
	};

