

	require([
		".._components/PostBox/Time/CirclePercentNew",
		".._components/PostBox/Time/Time",
		".._components/PostBox/Time/Dates",
		"./JustTime.css", (isMobile ? 
		"./JustTime.mobile.css" : "")
	]);

	export function JustTimeComponent(box, option)  {
		var own =this,
			date =null,
			weeks =['Даваа','Мягмар','Лхагва','Пүрэв','Баасан','Бямба','Ням'];

		this.date ="2022/02/11 09:30";
		this.status ="Ажил эхлэх цаг";
		own =extend(own, option);


		function init()  {
			date =mkDate();
			var degree =mkStartDegree();
			var year =date.getFullYear(),
				month =fillZero(date.getMonth() +1),
				day =fillZero(date.getDate()),
				week =weeks[(date.getDay() || 7) -1],
				time =fillZero(date.getHours()) +':'+ fillZero(date.getMinutes());
			var dateStr =year +'/'+ month +'/'+ day;
			var length =isMobile ? 52.5 : 50;  //hand length

			box.append([
				['<div class="JTtimeBox">', [
					['<div>', [
						['<Time>'],
						['<CirclePercentNew color:"#0d9dc4">', {
							unit: isMobile ? 'vw' : '',
							size: isMobile ? 25 : 114,
							thick: isMobile ? 1.6 : 8,
							percent: 0.1,
							startDegree: degree
						}],
						['<svg class="hourHand" viewBox="0 0 110 110">', [
							['<line x1="48" y1="55" x2="'+ (48 +length) +'" y2="55" '+
							  'style="transform:rotate('+ degree +'deg)">']
						]]
					]],
					['<div class="JTinfo">', [
						['<div class="date">', [dateStr +' '+ week]],
						['<div class="jtime">', [time]],
						['<div class="status">', [own.status]]
					]]
				]],
				['<Dates>', {
					days: [dateStr]
				}]
			]);
		};

		function mkStartDegree()  {
			var sh =date.getHours(),  //Эхлэх цаг
				sm =date.getMinutes(),  //Эхлэх минут
				stime =sh +(sm*1/60);  //Эхлэх хугацааг 60 биш 1-ээр илэрхийлнэ
			return (stime -3)*30;  //3: 0 градус эхлэх цэг нь 3 цаг
		};

		function mkDate()  {
			var tmp =own.date.split(' '),
				d =tmp[0].split('/'),
				t =tmp[1].split(':');

			var year =parseInt(d[0]);
			if(year <100)  year +=2000;
			var month =parseInt(d[1]) -1;  //Date функц month index авдаг тул 1-ийг хасав
			return new Date(year, month, d[2], t[0], t[1]);
		};

		init();
	};

