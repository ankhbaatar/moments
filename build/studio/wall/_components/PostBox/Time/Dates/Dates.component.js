

	require([
		"./Date",
		"./Dates.css", (isMobile ? 
		"./Dates.mobile.css" : "")
	]);

	export function DatesComponent(box, option)  {
		var own =this,
			DayBox =null,
			MonthBox =null;

		this.days =[
			"2022/2/9",
			"2022/2/10",
			"2022/2/14",
			"2022/3/7",
			"2022/3/8"
		];
		own =extend(own, option);


		function init()  {
			box.append([
				['<div class="week">', [
					['<div>', ["Да"]],
					['<div>', ["Мя"]],
					['<div>', ["Лх"]],
					['<div>', ["Пү"]],
					['<div>', ["Ба"]],
					['<div class="wend">', ["Бя"]],
					['<div class="wend">', ["Ня"]]
				]],
				['<div class="dayBox">']
			]);

			DayBox =box.select(".dayBox:0", 1);
			own.days.each(function(date)  {
				var tmp =date.split('/'),
					y =parseInt(tmp[0]),
					m =parseInt(tmp[1]),
					d =parseInt(tmp[2]);

				if(y <100)  y +=2000;  //Оныг 4 оронтой болгоно
				MonthBox =mkMonth(m);

				var DateBox =MonthBox.select("."+ d, 2);
				if(DateBox.length ==0)  mkWeek(y, m, d);
				DateBox =MonthBox.select("."+ d, 2);
				DateBox.addClass('chosed');
			});
		};

		function mkMonth(month)  {  //Орж ирсэн сарын бокс үүсээгүй бол үүсгэж буцаана
			var mBox =DayBox.select(".monthBox."+ month, 1);
			if(mBox.length ==0)  DayBox.append('<div class="monthBox '+ month +'">');
			return DayBox.select(".monthBox."+ month, 1);
		};

		function mkWeek(year, month, day)  {  //Орж ирсэн өдөрт хамаарах 7 хоногийн мөрийг зурна
			var LastWeek =MonthBox.select(".weekBox:last", 1);
			if(LastWeek.length ==0)  draw(day);
			else  {  //Зурагдсан байгаа сүүлийн week-ээс хойших хоосон week-үүдийг нөхөж зурна
				var DayBox =LastWeek.select("Date:0", 1),
					fday =parseInt(DayBox.attr('class'));  //Зурагсан өдрүүдээс сүүлийн 7 хоногийн эхний өдөр

				var i =fday +7;
				while(i <=day)  {
					draw(i);
					i +=7;
				};
			};


			function draw(d)  {
				var WeekBox =@('<div class="weekBox">');
				MonthBox.append(WeekBox);

				var ddate =new Date(year +"/"+ month +"/"+ d);
				
				// console.log('d: '+ d);
				// console.log('date: '+ ddate.getDay());
				
				var monDay =d -(ddate.getDay() -1),
					lastDay =new Date(year, month, 0).getDate();  //Тухайн сарын сүүлийн өдөр

				var delta =0;  //Дараа сар луу шилжсэн тохиолдолд тоолуураас хасах утга
				// console.log('monDay: '+ monDay);
				for(let i=monDay; i<monDay +7; i++)  {
					if(i >lastDay && delta ==0)  delta =i -1;  //Сүүлийн өдрөөс хэтэрвэл дараа сар луу шилжинэ
					let wend =(i -monDay >4) ? ' wend' : '',
						date =i -delta;

					WeekBox.append('<Date class="'+ date + wend +'">', {
						month: month,
						day: date
					});
				};
			};
		};

		init();
	};

