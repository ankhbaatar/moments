

	export function loadMoreComments(datas, onLoad)  {
		execAction("root/wall", "readMore", [{
			pro: own.pro,
			module: own.module,
			itemId: own.itemId,
			parentId: datas.parentId,
			guideCommId: datas.guideCommId,
			way: datas.way,
			notIn: datas.friends,
			onLoad: onLoad
		}]);
	};

