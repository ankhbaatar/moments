

	export function event()  {
		var imageComp =box.select('Images:0', 3),
			images =imageComp.select("ImageFrame", 2);

		images.click(function()  {
			if(own.hasOwnProperty('readOnly'))  {
				// var originUrl =window.location.ancestorOrigins[0];
				// if(originUrl =='https://www.able.mn' || originUrl =='https://intranet.gov.mn' || originUrl =='http://localhost')  window.open('https://store.able.mn/_alerts');
				// else  window.open('https://store.able.mn/_moments');
			}
			else  {
				var me =@(this);
				var ind =images.index(me),
					imageData =me.data();

				var data = {
					itemId: imageData.itemId,
					pro: own.pro,
					mod: own.module,
					url: imageData.path,
					ind: ind
				};

				if(own.pro == 'public' && own.mod != 'gallery')  {
					var img =me.select('img:0').nodeList[0].element();
					data['width'] =img.naturalWidth;
					data['height'] =img.naturalHeight;
				};

				var slideData = {
					exp: own.title || '',
					authorId: own.authorId || 77,
					authorName: own.authorName || 'М.Болд',
					authorApp: own.authorApp || 'Зөвлөх',
					authorIcon: own.authorIcon,
					comName: own.authorComName || 'Ablesoft LLC',
					date:own.date
				};

				execAction('root/wall', 'slideShow', [data, slideData]);
			}
		});

		box.select('UserIcon').mouseOver(function()  {
			var userIntroData =[{
				typeModule: 'userIntro',
				obj: @(this).html(),
				box: @(this).html(),
				workerId: own.authorId
			}];
			console.log('userIntroData', userIntroData);
			// window.parent.postMessage(userIntroData, 'http://localhost/able_2021');
			let origin =window.location.ancestorOrigins[0];
			window.parent.postMessage(userIntroData, origin);
		});
	};

