

	require([
		"_components/Able/ImageFrame",
		"./Images.css", (isMobile ? 
		"./Images.mobile.css" : "")
	]);

	export function ImagesComponent(box, option)  {
		var own =this,
			w =box.outerWidth(), h =w/1.5,  //width, height
			space =Math.ceil(w/100),  //Зураг хоорондын зай
			hw =(w -space)/2, hh =(h -space)/2, hww =(hw -space)/2,  //half of width, height
			sizes =[
				[[w, h]],  //1 зурагтай үеийн width height
				[[hw, h], [hw, h]],  //2 зурагтай үеийн width height
				[[hw, h], [hw, hh], [hw, hh]],  //3 зурагтай үеийн width height
				[[hw, h], [hw, hh], [hww, hh], [hww, hh]]  //4 зурагтай үеийн width height
			];

		this.images =[
			{ itemId: 1, plusCnt:3, commentCnt:15, path: FrontEndPath +"/_layouts/Root/Img1.jpg" },
			{ itemId: 2, commentCnt:8, path: FrontEndPath +"/_layouts/Root/Img2.jpg" },
			{ itemId: 3, path: FrontEndPath +"/_layouts/Root/Img3.jpg" }
		];
		this.cnt =3;  //Нийт зургийн тоо
		own =extend(own, option);


		function init()  {
			box.html("");
			if(own.cnt ==1)  //Нэг зураг харуулах бол
				box.append('<ImageFrame index="0">', mkProps(1, 0));

			else if(own.cnt ==2)  box.append([  //Хоёр зураг харуулах бол
				['<ImageFrame index="0">', mkProps(2, 0)],
				['<ImageFrame index="1">', mkProps(2, 1)]
			]);

			else if(isMobile)  {  //Mobile дээр бол ихдээ 3 зураг харуулна
				if(own.cnt >=3)  box.append([  //Гурван зураг харуулах бол
					['<ImageFrame index="0">', mkProps(3, 0)],
					['<div class="column">', [
						['<ImageFrame index="1">', mkProps(3, 1)],
						['<div class="moreBox" style="width:'+ sizes[2][2][0] +'px; height:'+ sizes[2][2][1] +'px;">', [
							['<ImageFrame index="2">', mkProps(3, 2)], (own.cnt >3 ? 
							['<div class="cover" style="margin-top:-'+ sizes[2][2][1] +'px">', [
								['<div>', ["+ "+ (own.cnt -3)]]
							]] : [])
						]]
					]]
				]);

				return;
			}

			else if(own.cnt ==3)  box.append([  //Гурван зураг харуулах бол
				['<ImageFrame index="0">', mkProps(3, 0)],
				['<div class="column">', [
					['<ImageFrame index="1">', mkProps(3, 1)],
					['<ImageFrame index="2">', mkProps(3, 2)]
				]]
			]);

			else if(own.cnt >=4)  box.append([  //Дөрвөн зураг харуулах бол
				['<ImageFrame index="0">', mkProps(4, 0)],
				['<div class="column">', [
					['<ImageFrame index="1">', mkProps(4, 1)],
					['<div class="box4">', [
						['<ImageFrame index="2">', mkProps(4, 2)],
						['<div class="moreBox" style="width:'+ sizes[3][3][0] +'px; height:'+ sizes[3][3][1] +'px;">', [
							['<ImageFrame index="3">', mkProps(4, 3)], (own.cnt >4 ? 
							['<div class="cover" style="margin-top:-'+ sizes[3][3][1] +'px">', [
								['<div>', ["+ "+ (own.cnt -4)]]
							]] : [])
						]]
					]]
				]]
			]);
		};

		function mkProps(imgCnt, imgInd)  {
			var img =own.images[imgInd];
			var props = {
				width: sizes[imgCnt -1][imgInd][0],
				height: sizes[imgCnt -1][imgInd][1],
				path: img.path,
				borderWidth: Math.floor(w/400),
				borderRadius: Math.ceil(w/150),
				data: {
					itemId: img.itemId,
					path: img.path
				}
			};

			var DetailBox =@('<div class="detailBox">', [(img.hasOwnProperty('plusCnt') ? 
				['<div class="plusCnt">', [img.plusCnt]] : []), (img.hasOwnProperty('commentCnt') ? 
				['<div class="commCnt">', [img.commentCnt]] : [])
			]);

			if(DetailBox.select("div:0").length >0)  props['detail'] =DetailBox;
			return props;
		};

		init();
	};

