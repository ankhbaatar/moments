

	require([
        "_components/Able/UsersGroupTemp",
		"./PostFooter.css", (isMobile ? 
		"./PostFooter.mobile.css" : "")
	]);

	export function PostFooterComponent(box, option)  {
		var own =this;
        this.hasUsers =false;
        this.hasAlert =false;
        this.usersText ='Харж байгаа';
        this.commentText ='Сэтгэгдэл';
        this.commentCnt =0;  /*
            .users (Array)
            .usersTotalCnt (Int)
            .plusProps (Object)
            .usersInfo (Object)*/
		own =extend(own, option);


		function init()  {
			box.html('<div class="main">', [(!isMobile ?
                ['<div class="comments">', [own.commentText +' ('+ own.commentCnt +')']] : []), (own.hasAlert ? 
                ['<div class="alerts">', [
                    ['<svg width="13px" height="16px" viewBox="0 0 13 16">', [
                        ['<path d="M5.091,13.998c-0.056,0.158-0.092,0.325-0.092,0.502c0,0.829,0.671,1.5,1.5,1.5s1.5-0.671,1.5-1.5'+
                        'c0-0.177-0.036-0.344-0.092-0.502H5.091z">'],
                        ['<path d="M12.035,10.103C11.581,9.765,11,9.209,11,8.563V5.5c0-1.965-1.268-3.618-3.023-4.231'+
                        'C7.865,0.551,7.249,0,6.5,0S5.135,0.551,5.023,1.269C3.268,1.882,2,3.535,2,5.5v3.063c0,0.647-0.581,1.203-1.035,1.54'+
                        'C0.401,10.318,0,10.86,0,11.5C0,12.329,0.671,13,1.5,13h5h5c0.829,0,1.5-0.671,1.5-1.5C13,10.86,12.599,10.318,12.035,10.103z">']
                    ]]
                ]] : []),
                ['<PlusBox size:"large">', own.plusProps]
            ]);

            if(own.hasUsers)  {
                box.prepend('<div class="users">', [
                    ['<div class="text">', [own.usersText]],
                    ['<UsersGroupTemp>', {
						icons: own.users,
						totalCount: own.usersTotalCnt,
						bgColor: "white",
						size: isMobile ? 7 : 22,
						unit: isMobile ? "vw" : "px",
						bw: isMobile ? 0.2 : 1
                    }]
                ]);

                box.data({
                    usersUpdate: usersUpdate
                })
            }

            if(own.hasAlert)  {
                var RootData =box.parents('PostBox:0', 2).data();
                box.select('.alerts:0').click(function()  {
                    // window.parent.postMessage([{typeModule: 'alert', itemId: RootData.itemId, pro: RootData.pro, mod: RootData.mod}], 'http://localhost/able_2021');
                    // window.parent.postMessage([{typeModule: 'alert', itemId: RootData.itemId, pro: RootData.pro, mod: RootData.mod}], 'https://time.able.mn');
                    let origin =window.location.ancestorOrigins[0];
                    window.parent.postMessage([{typeModule: 'alert', itemId: RootData.itemId, pro: RootData.pro, mod: RootData.mod}], origin);
                });
                
                box.select('.alerts:0').overHint({
                    hint: "Санамж үүсгэх",
                    side: "bottom"
                });
            };

            function usersUpdate(users, usersCnt)  {
                var users =box.select('.users:0 UsersGroupTemp:0');
                users.after('<UsersGroupTemp>', {
                    icons: users,
                    totalCount: usersCnt,
					bgColor: "white",
					size: isMobile ? 7 : 22,
					unit: isMobile ? "vw" : "px",
					bw: isMobile ? 0.2 : 1
                });
                users.remove();
            };
		};

		init();
	};

