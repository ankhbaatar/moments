const fs = require('fs');
const readline = require('readline');
const tr = require('./token_recognizer');

let fileContent =[];
let renderedContents =[];

let fnComonentRelace =false;
let fnLayoutRelace =false;
let actionFileReplace =false;
let layOutEndBracket =true;

let js = {
	listener: (readPath, productPath) =>  {
		js.destory();
		const readStream = fs.createReadStream(readPath);
		const writeStream = fs.createWriteStream(productPath, { encoding: "utf8" });
		// tr.start(readPath);
		const rl = readline.createInterface({
			input: readStream,
			output: writeStream,
			terminal: false,
			historySize: 0
		});

		rl.on('line',(line) =>  {
			fileContent.push(line);
		});

		rl.on('close',async () =>  {
			for(let i=0; i<fileContent.length; i++)  {
				let rdrRow = await js.render(fileContent[i], productPath, i);
				renderedContents.push(rdrRow);
			}

			/* render хийгдээд гараад ирсний дараа */
			if(actionFileReplace ==true)  {
				for(let i=renderedContents.length-1; i>=0; i--)  {
					if(renderedContents[i].indexOf('}') >=0)  {
						renderedContents[i] ="});";
						actionFileReplace =false;
						break;
					}
				}
			}

			let renderedLine ='';
			for(let i=0; i<renderedContents.length; i++)  {
				let row =renderedContents[i];
				renderedLine +=row;
			}

			writeStream.write(renderedLine);
		});
	},

	render: (line, productPath, lineCnt) =>  {
		var completedLine ='';
		completedLine =line.replace("@(", "adom(");

		/* layout file */  {
			if(/\w.layout.js$/.test(productPath.trim()))  {
				if(/export\sfunction\s\w+/.test(line))  {
					/* component.js үүд дээр ажиллана. Кодон дотроос
						"export function" гэсэн мөрийг олоод, "function layout()  {"
						ийм болгож replace хийнэ.
					*/
					completedLine ="function layout()  {" + '\n' + line.replace('export ','return ');
					fnLayoutRelace =true;
				}
				else if(fileContent.length -1 === lineCnt && fnLayoutRelace)  {
					/* layout.js үүд дээр ажиллна хамгийн эхний мөр эхлэхэд
						"function layout() {" гэсэн мөрийг шууд нэмж өгсөн бол
						одоо хамгийн сүүлийн мөр дээр хаалтыг нэмж өгнө.
					*/
					completedLine +='\n};';
				}
			}
		};

		/* compoenet file */  {
			if(/\w.component.js$/.test(productPath.trim()))  {
				if(/export\sfunction\s\w+/.test(line))  {
					/* component.js үүд дээр ажиллана. Кодон дотроос
						"export function" гэсэн мөрийг олоод, "function component()  {"
						ийм болгож replace хийнэ.
					*/
					completedLine ="function component()  {" + '\n' + line.replace('export ','return ');
					fnComonentRelace =true;
				}
				else if(fileContent.length -1 === lineCnt && fnComonentRelace)  {
					/* Хамгийн сүүлийн мөрийг олоод хаалтыг хааж өгнө */
					completedLine +='\n};';
				}
			}
		};

		/* proto file */  {
			if(/\w.proto.js$/.test(productPath.trim()))  {
				if(/export\sfunction\s\w+/.test(line))  {
					/* proto.js үүд дээр ажиллана. Кодон дотроос
						"export function" гэсэн мөрийг олоод, "function proto()  {"
						ийм болгож replace хийнэ.
					*/
					completedLine ="function proto()  {" + '\n' + line.replace('export ','return ');
					fnComonentRelace =true;
				}
				else if(fileContent.length -1 === lineCnt && fnComonentRelace)  {
					/* Хамгийн сүүлийн мөрийг олоод хаалтыг хааж өгнө */
					completedLine +='\n};';
				}
			}
		};

		/* fun file */  {
			if(/\w.fun.js$/.test(productPath.trim()))  {
				if(/export\sfunction\s\w+/.test(line))  {
					/* fun.js үүд дээр ажиллана. Кодон дотроос
						"export function" гэсэн мөрийг олоод, "function fun()  {"
						ийм болгож replace хийнэ.
					*/
					completedLine ="function fun()  {" + '\n' + line.replace('export ','return ');
					fnComonentRelace =true;
				}
				else if(fileContent.length -1 === lineCnt && fnComonentRelace)  {
					/* Хамгийн сүүлийн мөрийг олоод хаалтыг хааж өгнө */
					completedLine +='\n};';
				}
			}
		};

		/* action file */
		if(/export\s+function\s+\w+[(]/.test(completedLine))  {
			let tmpFnName =completedLine.match(/function\s\w[^(]*\(([^)]*)\)/)[0].replace('function ','');
			completedLine ="setModuleAction(function "+tmpFnName+"  {";
			actionFileReplace =true;
		}

		return completedLine+'\n';
	},

	destory:() =>  {
		fileContent =[];
		renderedContents =[];

		fnComonentRelace =false;
		fnLayoutRelace =false;
		actionFileReplace =false;
		layOutEndBracket =true;
	}
};

module.exports =js;
