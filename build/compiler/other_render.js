const fs = require('fs');
const readline = require('readline');

let otherFileRender =  {
	listener: (readPath, productPath) =>  {
		const readStream = fs.createReadStream(readPath);
		const writeStream = fs.createWriteStream(productPath, { encoding: "utf8" });
		const rl = readline.createInterface({
			input: readStream,
			output: writeStream,
			terminal: false,
			historySize: 0
		});

		rl.on('line',(line) =>  {
			writeStream.write(line+"\n");
		});
	}
};

module.exports =otherFileRender;

