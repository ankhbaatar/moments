const fs =require('fs');
const shell = require('shelljs');
const copy = require('recursive-copy');

let lfs ={  //Файлтай ажилладаг локал функцүүд

    getProductPath: (filePath) =>  {
        return filePath.replace("studio","products");
    },

	fileExtention: (filePath) =>  {
		let ext =filePath.split('.');
		return ext[ext.length -1];
	},

	mkDir: (fullPath) =>  {
		shell.mkdir('-p', lfs.getProductPath(fullPath));
	},

	fileCopy: async (rfp, wfp) =>  {
		wfp = wfp || lfs.getProductPath(rfp);

		copy(rfp, wfp).then((results) =>  {
            console.info('Copied ' + results.length + ' files');
		}).catch((error) =>  {
            console.error('Copy failed: ' + error);
	    });
	},

	isFile: (fullPath) =>  {
		var stats = fs.statSync(fullPath);
		return stats.isFile();
	},

	deleteFile: (filePath) =>  {
		fs.unlinkSync(lfs.getProductPath(filePath));
	}
};

module.exports =lfs;

