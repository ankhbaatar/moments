

const fs =require('fs');
const readline =require('readline');
const define =require('./define');

let DOMPath =[];
let atKeyWord =[];
let fileContentArr =[];
let renderedContents =[];

let css = {  //Css файл хийхэд ашиглагдах функүүдын обьект

	listener: async (readPath, productPath) => {
		css.destory();
		const readStream = fs.createReadStream(readPath);
		const writeStream = fs.createWriteStream(productPath, { encoding: "utf8" });
		const rl = readline.createInterface({
			input: readStream,
			output: writeStream,
			terminal: false,
			historySize: 0
		});

		rl.on('line',(line) => {
			fileContentArr.push(line);
		});

		rl.on('close',() =>  {
			for(let i=0; i<fileContentArr.length; i++)  {
				let rdrRow =css.render(fileContentArr[i], productPath);
				renderedContents.push(rdrRow);
			};

			/* render хийж дууссаны дараа ажиллна. */
			// renderedLine =renderedLine.replace(/\/\*[\s\S]*?\*\/|([^:]|^)\/\/.*$/,'');

			let islayoutPath =/_layouts/.test(productPath);
			let isComponentPath =/_components/.test(productPath);
			let renderedLine ='';

			for(let i=0; i<renderedContents.length; i++)  {  //render hiigdeed garch irsen stringuud deer dahij ajillana.
				let row =renderedContents[i];
				if((islayoutPath || (isComponentPath && !define.isAblejsCore)) && row.indexOf('export '))  {
					let paths =productPath.split('products')[1];
					paths =paths.split('_')[0].replace(/([\\]|[\/])/g,'/').split('/');

					let token ='', sumCh ='', count=0;
					if(!define.isAblejsCore)  /*Ablejs Core project-ийн хувьд бол root гэж байхгүй. Учир нь Ablejs project-д 
						задгай байгаа модулиуд local project-уудад очихдоо _ablejs хавтаст хуулагдах тул тухайн project-ийн 
						root модультай зэрэгцээ байрлалд очих юм. Харин local project бол Хамгийн эхэнд root module key-г шигтгэнэ */
						paths.splice(0, 0, 'root');

					paths.map((path) => {
						if(path =='')  return;
						sumCh +=path;
						token +=path.charAt(0);
						if(path!='') count++;
					});

					token =token +(sumCh.length +(count-1));
					renderedLine +=row.replace('export ','#able .moduleBox[token='+ token +'] ');
				}
				else  {
					renderedLine +=row.replace('export ','#able ');
				};
			};

			writeStream.write(renderedLine);
		});
	},

	render:(line, productPath) => {
		let completedLine ='';
		completedLine =css.replacePath(line);
		return completedLine;
	},

	replacePath:(line) => {
		let realLine =line.trim();

		if(line.indexOf("@") > 0)  {
			atKeyWord.push(line);
		}

		if(atKeyWord.length == 0)  {
			if(line.indexOf("}") >= 0)  {
				DOMPath.pop();
				realLine =(DOMPath.length ==0) ? "}\n" : "";
			}

			if(line.indexOf("{") >= 0)  {
				let lineRow ='';
				if(DOMPath.length > 0) lineRow ="}\n";
				DOMPath.push(line.replace("{", ""));
				for(let i=0; i<DOMPath.length; i++)  {
					lineRow +=" "+DOMPath[i].trim();
				}

				realLine =lineRow +="{";
			}

			if(realLine.indexOf("&") >= 0)  {
				tmpPathStr ='';
				let tmpArr =realLine.split("&");
				for(let i=0; i<tmpArr.length; i++)  {
					tmpPathStr +=tmpArr[i].trim();
				}
				realLine =tmpPathStr;
			}
		}

		if(atKeyWord.length > 0 && line.indexOf("}") >=0)  {
			atKeyWord.pop();
        }

		/* Хоосон зайнуудыг шах нь */  {
			realLine =realLine.replace(/(:\s+)/,':');
			realLine =realLine.replace(/(\s+:)/,':');
		}

		realLine =realLine.replace(/\/\*[\s\S]*?\*\/|([^:]|^)\/\/.*$/,'');
		return realLine;
	},

	destory:() =>  {
		DOMPath =[];
		atKeyWord =[];
		fileContentArr =[];
		renderedContents =[];
	}
};

module.exports = css;

