/**
	+ Created by Able soft
	+ Author: Zev

	Ablejs core файлуудыг add хийнэ.
	Ингэхдээ тухайн project ablejs-ийг ямар замаар ашиглаж байгаагаас хамаараад
	хураангуйлсан файлуудыг local зам дээрээс, эсвэл эх файлуудыг core project-оос
	холбоно.
*/

	(function()  {
		var Ind =0,
			localFiles = [  //abeljs-ийг локал project-доо install хийсэн байгаа бол холбох ёстой core файлууд
				"able.js",
				"able.css",
				"comps.js",
				"comps.css"
			],
			sourceFiles = [  //abeljs-ийг эх project-оос нь шууд татаж ажиллуулах бол холбосон байх core файлууд
				"_ablecore/ableCore.js",
				// "comps.js",
				// "comps.css",

			//--abledom library файлууд
				"_abledom/ableNode/ableNode.js",
					"_abledom/ableNode/ableNode.service.js",
					"_abledom/ableNode/ableNode.structure.js",
					"_abledom/ableNode/ableNode.structure.extend.js",
					"_abledom/ableNode/ableNode.parser.js",

				"_abledom/ableNodeList/ableNodeList.js",
					"_abledom/ableNodeList/ableNodeList.structure.use.js",
					"_abledom/ableNodeList/ableNodeList.structure.selector.js",

				"_abledom/ableNodeList/structure.changer/structure.changer.js",
					"_abledom/ableNodeList/structure.changer/extend.js",
					"_abledom/ableNodeList/structure.changer/move.js",

				"_abledom/ableNodeList/ableNodeList.animate.js",
					"_abledom/ableNodeList/ableNodeList.events.js",
					"_abledom/ableNodeList/ableNodeList.actions.js",
					"_abledom/ableNodeList/other.js",

			//--Load _builder functions
				"_ablecore/_builder/builder.js",
					"_ablecore/_builder/layer.js",
					"_ablecore/_builder/range.js",
					"_ablecore/_builder/module.js",
					"_ablecore/_builder/component.js",
				"_ablecore/_builder/request/request.js",
					"_ablecore/_builder/request/setNextLayer.js",
					"_ablecore/_builder/request/apiData.js",
					"_ablecore/_builder/request/isFinish.js",
				"_ablecore/_builder/layerBuilder/layerBuilderClass.js",
					"_ablecore/_builder/layerBuilder/setNextRange.js",
				"_ablecore/_builder/rangeBuilder/rangeBuilderClass.js",
					"_ablecore/_builder/rangeBuilder/isSkip.js",
					"_ablecore/_builder/rangeBuilder/isExist.js",
					"_ablecore/_builder/rangeBuilder/setNextModule.js",
					"_ablecore/_builder/rangeBuilder/getNextk.js",
				"_ablecore/_builder/moduleBuilder/moduleBuilderClass.js",
					"_ablecore/_builder/moduleBuilder/buildLayout.js",
					"_ablecore/_builder/moduleBuilder/loadData.js",
					"_ablecore/_builder/moduleBuilder/setOnMore.js",
				"_ablecore/_builder/componentBuilder/componentBuilderClass.js",
					"_ablecore/_builder/componentBuilder/loadData.js",
					"_ablecore/_builder/componentBuilder/go.js",
					"_ablecore/_builder/componentBuilder/parse.js",
					"_ablecore/_builder/componentBuilder/getBranch.js",

			//--tools/layers + Давхаргуудыг удирдах сервис функцууд
				"_ablecore/layers/layers.js",
					"_ablecore/layers/other.js",

			//--tools/router + Routing path удирдах функцууд
				"_ablecore/router/router.js",
					"_ablecore/router/setState.js",

			//--Load tools + Энд дандаа сервис төрлийн глобал функцууд байна /object биш/
				"_ablecore/tools/modules/actions.js",
					"_ablecore/tools/modules/get.js",
					"_ablecore/tools/modules/load.js",
					"_ablecore/tools/modules/reach.js",
					"_ablecore/tools/modules/shift.js",
					"_ablecore/tools/modules/other.js",
				//tools/extends + native javascript функцуудыг өргөтгөж хялбаршуулсан функцууд байна
				"_ablecore/tools/extends/array.js",
					"_ablecore/tools/extends/objects.js",
				//tools/functions + глобал функцууд
				"_ablecore/tools/functions/functions.js",
					"_ablecore/tools/functions/core.js",
					"_ablecore/tools/functions/component.js",
					"_ablecore/tools/functions/xmlRequest.js",
					"_ablecore/tools/functions/other.js",
				//tools/require + файлуудыг require хийх сервис функцууд
				"_ablecore/tools/require/require.js",
					"_ablecore/tools/require/functions.js",

			//--Бусад файлууд
				"_ablecore/zdebug.js",
				"_ablecore/systemData/default.css",
				"_ablecore/systemData/core.css",
				"_ablecore/systemData/fonts/Roboto.css",
				"_ablecore/systemData/fonts/RobotoCondensed.css"
			];

		var coreFiles =(isLocal) ? localFiles : sourceFiles;
		loadAblejs();


		function loadAblejs()  {
			if(Ind ==coreFiles.length)  return;
			var path =AblejsPath +"/"+ coreFiles[Ind],
				arr =path.split('.'),
				ext =arr[arr.length -1];

			var newNode =(ext =="js") ? addNode(path) : addStyleNode(path);
			newNode.onload =function()  {
				Ind +=1;
				loadAblejs();
			};
		};

		function addNode(path)  {
			var node =document.createElement('script');
			node.setAttribute('type', "text/javascript");
			node.setAttribute('src', path);
			document.head.appendChild(node);
			return node;
		};

		function addStyleNode(path)  {
			var node =document.createElement('link');
			node.setAttribute('rel', "stylesheet");
			node.setAttribute('type', "text/css");
			node.setAttribute('href', path);
			document.head.appendChild(node);
			return node;
		};
	}());

