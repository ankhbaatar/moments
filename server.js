/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
const fs = require('fs');
const cors = require('cors');
const path = require('path');
const http = require('http');
const https = require('https');
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const responseHandler = require('express-response-handler');

// Sentry requires
const Sentry = require('@sentry/node');
const Tracing = require('@sentry/tracing');

// swagger requires
const swaggerAuthOptions = {
    // swaggerOptions: {
    //     authAction: {
    //         Bearer: {
    //             schema: {
    //                 type: 'http',
    //                 in: 'header',
    //                 name: 'Authorization',
    //             },
    //             name: 'Bearer',
    //             value:
    //                 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDBmY2VkY2RiZDk2MzNiNzAwODQyYTMiLCJwaG9uZSI6Ijk5MTAxMDEwIiwidWFnZW50IjoiTW96aWxsYS81LjAgKFdpbmRvd3MgTlQgMTAuMDsgV2luNjQ7IHg2NCkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzg3LjAuNDI4MC4xNDEgU2FmYXJpLzUzNy4zNiIsImlhdCI6MTYxMTgzMTk2NX0.4rWvxc3vKErhG8LFkzCtTl4qtgn4E6oCfvQdMoVNTuU',
    //         },
    //     },
    // },
    // host: 'http://localhost:5000/docs/#/',
};

const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

// config requires
const config = require('./config/config.js');
const swaggerOptions = require('./config/swagger.js');
const responseCode = require('./config/response');

// variable defines
const swaggerSpec = swaggerJSDoc(swaggerOptions);

const corsOptions = {
    credentials: true,
    origin: '*',
};

// server-н redis-тэй холболт үүсгэх тохиргоо
const redis = require('./models/redis');

const redisConf = redis(config.db.redis);

// init express app
const app = express();
app.set('redis', {redis: redisConf});
app.use(cors(corsOptions));
app.use(cookieParser());
app.use(bodyParser.json({ extended: true, limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: true }));

// Error handler middleware
Sentry.init({
    dsn: 'https://db2a3db3805e4d4eb894f6dac54bda6c@o532162.ingest.sentry.io/5651479',
    integrations: [
        // enable HTTP calls tracing
        new Sentry.Integrations.Http({ tracing: true }),
        // enable Express.js middleware tracing
        new Tracing.Integrations.Express({ app }),
    ],

    // We recommend adjusting this value in production, or using tracesSampler
    // for finer control
    tracesSampleRate: 1.0,
});

if (process.env.NODE_ENV === 'production') {
    app.use(Sentry.Handlers.requestHandler());
    app.use(Sentry.Handlers.tracingHandler());
}

// Response handler
app.use(responseHandler(responseCode));

// Main routes
app.use('/api', require('./api/routes'));

// documentations
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec, swaggerAuthOptions));

// Front end
app.use(express.static(path.join(__dirname, './build')));
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, './build/products', 'index.html'));
});

// Not found
app.use((req, res) => {
    res.error.User('not_found.error');
});

// Error handler
// app.use(Sentry.Handlers.errorHandler());
app.use((err, req, res, next) => {
    res.error.ServerError('server.error', err);
});

// create http server
let server;
if (process.env.NODE_ENV === 'production') {
    const ssl = { 
        secureProtocol: 'SSLv23_method', 
        secureOptions: require('constants').SSL_OP_NO_SSLv3, 
        ca: fs.readFileSync('/etc/httpd/ssl/ca.crt'), 
        key: fs.readFileSync('/etc/httpd/ssl/able.mn.key'), 
        cert: fs.readFileSync('/etc/httpd/ssl/STAR_able_mn.crt'), 
        passphrase: '',
    }; 
    server = https.createServer(ssl, app);
} else {
    server = http.Server(app);
}

if(process.env.NODE_ENV ==='development') {
    const { fork } = require('child_process');
    const forked = fork('./build/compiler/render.js');
    forked.on('message', (msg) => { Print(`compiler listening ${msg}`); });
}

server.listen(config.app.port, () => {
    console.log(`web server listening on: ${config.app.port}`);
});

module.exports = app;
