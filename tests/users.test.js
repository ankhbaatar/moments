const request = require('supertest');
const app = require('../server');

describe('Users Endpoints', () => {
    it('Users list', async () => {
        const res = await request(app).get('/api/users');
        console.log('user array length: ', res.body.length);
        // expect(res.o, /json/)
        expect(res.body.length).toEqual(5);
        expect(res.statusCode).toEqual(200);
    });
});

// describe("Login Endpoints", () => {
//     it("Login check success", async () => {
//         const res = await request(app).post("/api/users/login").send({
//             phone: "86088806",
//             password: "123"
//         });
//         expect(res.statusCode).toEqual(200);
//         expect(res.body).toHaveProperty("token");
//     });
// });

// describe("Login Endpoints", () => {
//     it("Login password invalid", async () => {
//         const res = await request(app).post("/api/users/login").send({
//             phone: "86088806",
//             password: "123shdjk"
//         });
//         expect(res.statusCode).toEqual(200);
//         expect(res.body).toHaveProperty("error");
//     });
// });

// describe("Login Endpoints", () => {
//     it("Login phone invalid", async () => {
//         const res = await request(app).post("/api/users/login").send({
//             phone: "8608880656",
//             password: "123"
//         });
//         expect(res.statusCode).toEqual(200);
//         expect(res.body).toHaveProperty("error");
//     });
// });
