const Router = require('express').Router();

Router.use('/post', require('./controllers/post/router'));
Router.use('/comment', require('./controllers/comment/router'));
Router.use('/plus', require('./controllers/plus/router'));
Router.use('/vote', require('./controllers/vote/router'));
Router.use('/slideShow', require('./controllers/slideShow/router'));
Router.use('/seen', require('./controllers/seen/router'));
Router.use('/getError', require('./controllers/getError/router'));
Router.use('/redis', require('./controllers/redis/router'));
Router.use('/user', require('./controllers/user/router'));

module.exports = Router;
