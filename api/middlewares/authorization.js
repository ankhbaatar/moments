module.exports = authorizationFn => (req, res, next) => {
    const isAuthorized = authorizationFn(req);

    if (typeof isAuthorized !== 'boolean') {
        throw new Error('Authorization function must be return boolean');
    }

    if (isAuthorized) {
        next();
    } else {
        return res.status(403).json({
            error: 'error authorization',
        });
    }
};
