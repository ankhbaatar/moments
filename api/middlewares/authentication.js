const jwt = require('jsonwebtoken');

const auth = (req, res, next) => {
    try {
        const bearer = req.headers['x-access-token'] || req.headers.authorization  || req.query.aceessToken || req.body.accessToken;

        if (bearer === undefined) {
            res.error.Auth('authentication.error');
            return;
        }

        let token;

        if (bearer.startsWith('Bearer ')) {
            // Remove Bearer from string
            token = bearer.slice(7, bearer.length);
        };
        if (req.query.aceessToken || req.body.accessToken) {
            token = req.query.aceessToken || req.body.accessToken;
        };

        if (token === undefined || token === null) {
            res.error.Auth('authentication.error');
            return;
        };

        jwt.verify(token, process.env.AUTH_KEY, (err, datas) => {
            if (err) {
                res.error.Auth('authentication.error');
                return;
            }

            if (datas.myHID === null || datas.myComId === undefined || datas.key === null) {
                res.error.Auth('authentication.error');
                return;
            }

            // Global variables
            req.auth = { myHID: datas.myHID, myComId: datas.myComId, dbKey: datas.key, accessToken: token };
            next();
        });
    } catch (e) {
        res.error.Auth('authentication.error');
    }
};

module.exports = auth;
