const moment =require('moment');
const { showText, mkEasyDate, encStr, getGroupName } =require('../../helpers/globalFunction');
const { getUserInfo } =require('../../services/getUserInfo');
const config = require('../../config/config');

module.exports = async (post, desc, state, req) => {
    const accessToken =req.auth.accessToken,
          authorInfo =await getUserInfo(post.autor_id, accessToken, req);

    const images =[];

    let groupName =['Цаг бүртгэл', 'Хүсэлт'];

    let postTitle ='';
    if(post.type ==0)  postTitle ='Илүү цагийн хүсэлт';
    if(post.type ==1)  {
        if(desc[0].is_vacation ==1)  postTitle ='Ээлжийн амралт биеэр эдлэх хүсэлт';
        else  postTitle ='Илүү цагийг биеэр эдлэх хүсэлт';
    }
    if(post.type ==2)  postTitle ='Илүү цагийг мөнгөөр авах хүсэлт';
    if(post.type ==4)  postTitle =state[0].name +' төлвийн хүсэлт';
    if(post.type ==5)  postTitle ='Цаг бүртгүүлэх хүсэлт';
    if(post.type ==7)  postTitle ='';
    if(post.type ==8)  postTitle ='Тодорхойлолтын хүсэлт';

    return {
        postTitle: showText(postTitle),
        postDescription: post.note,
        authorInfo: authorInfo,
        easyDate: mkEasyDate(post.date),
        fullDate: moment(post.date *1000).format('YY/MM/DD - HH:mm'),
        images: images,
        groupName: groupName
    };
};
