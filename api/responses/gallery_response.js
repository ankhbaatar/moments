const moment =require('moment');
const { showText, mkEasyDate, encStr, getGroupName } =require('../../helpers/globalFunction');
const { getUserInfo } =require('../../services/getUserInfo');
const config = require('../../config/config');
const comment = require('../../services/comment');
const plus = require('../../services/plus');

module.exports = async (post, req) => {
    const accessToken =req.auth.accessToken;

    let authorInfo =await getUserInfo(post[0].autor_id, accessToken, req);

    const images =[];
    let length =0;
    if(post.length > 4)  length =4;
    else length =post.length;
    
    for(let i =0; i <length; i++)  {
        let image =post[i].small;
        if(i ==0) image =post[i].large;  //Ямагт эхний зургийн том хэмжээг авна
        if(i ==1) image =post[i].large;  //Хоёрдох зураг 2хон зурагтай үед том байна
        if(i ==1 && post.length >2) image =post[i].small;  //Хоёрдох зураг 2оос их зурагтай үед жижиг байна
        let dataDir = encStr(req.auth.dbKey),
            base64Str =image ? encodeURIComponent(encStr(`public/gallery/${image}`)) : '',
            imagePath =`${config.storageHost}/main.php?g=bG9ZhEltYWdl&path=${base64Str}&accessToken=${accessToken}&dataDir=${dataDir}`;

        let plusCnt =await plus.count('`item_id`='+ post[i].id +' AND `pro`="public" AND `mod`="gallery"', req);
        let commentCnt =await comment.count('`mid`='+ post[i].id +' AND `pro`="public" AND `module`="gallery"', req);

        let obj ={
            itemId: post[i].id,
            path: imagePath
        }

        if(plusCnt)  obj['plusCnt'] =plusCnt;
        if(commentCnt)  obj['commentCnt'] =commentCnt;

        images.push(obj);
    }

    let groupName =['Олон нийт', 'Зургийн цомог'];
    // if(post.group_id)  {
        // const groupNames =await getGroupName(post.group_id, req.auth.dbKey);
        // groupName =groupName.concat(groupNames);
    // }

    return {
        postTitle: showText(post[0].exp),
        postDescription: null,
        authorInfo: authorInfo,
        easyDate: mkEasyDate(post[0].date),
        fullDate: moment(post[0].date *1000).format('YY/MM/DD - HH:mm'),
        images: images,
        groupName: groupName,
        imageCnt: post.length
    };
};
