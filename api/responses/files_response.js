const { encStr } =require('../../helpers/globalFunction');
const config = require('../../config/config');

module.exports = async (file, folder, dbKey, pro, module, accessToken) => {
    const fileNameArr =file.name.split('.'),
          fileType =fileNameArr[fileNameArr.length -1];
    let base64Str =file.name ? encodeURIComponent(encStr(`${pro}/${module}/${folder[0].name}/${file.name}`)) : '';

    if(pro =='docs')  base64Str =file.name ? encodeURIComponent(encStr(`${pro}/${folder[0].name}/${file.name}`)) : '';
    const filePath =`${config.storageHost}/main.php?g=ZG9b3mxvYWQ=&path=${base64Str}&accessToken=${accessToken}`;

    return {
        name: file.name,
        type: fileType,
        dir: filePath
    };
};
