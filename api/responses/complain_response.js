const moment =require('moment');
const { showText, mkEasyDate, encStr, getGroupName } =require('../../helpers/globalFunction');
const { getUserInfo } =require('../../services/getUserInfo');
const config = require('../../config/config');

module.exports = async (post, req) => {
    const accessToken =req.auth.accessToken,
          authorInfo =await getUserInfo(post.autor_id, accessToken, req);


    let groupName =['Албан бичиг', 'Өргөдөл гомдол'];
    // if(post.group_id)  {
    //     const groupNames =await getGroupName(post.group_id, req.auth.dbKey);
    //     groupName =groupName.concat(groupNames);
    // }

    return {
        postTitle: showText(post.name),
        postDescription: {
            intro: showText(post.note),
            more: ''
        },
        authorInfo: authorInfo,
        easyDate: mkEasyDate(post.date),
        fullDate: moment(post.date *1000).format('YY/MM/DD - HH:mm'),
        images: [],
        groupName: groupName,
        imageCnt: 0
    };
};
