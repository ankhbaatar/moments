const moment =require('moment');
const { showText, mkEasyDate, showTextAnother } =require('../../helpers/globalFunction');
const { getUserInfo } =require('../../services/getUserInfo');
const config = require('../../config/config');

module.exports = async (comment, req) => {
    const accessToken =req.auth.accessToken;

    const authorInfo =await getUserInfo(comment.user_id, accessToken, req);

    const result ={
        pId: comment.reply_id,
        Id: comment.id,
        author: {
            Id: comment.user_id,
            name: authorInfo.system_name,
            icon: {
                path: authorInfo.user_icon,
                gender: (authorInfo.gender ? 'male' : 'female'),
                age: authorInfo.age,
                overHint: authorInfo.system_name
            }
        },
        date: mkEasyDate(comment.date),
        fullDate: moment(comment.date *1000).format('YY/MM/DD - HH:mm'),
        comment: showTextAnother(comment.comment)
    };

    return result;
};
