const moment =require('moment');
const { showText, mkEasyDate, encStr, getGroupName, htmlParse } =require('../../helpers/globalFunction');
const { getUserInfo } =require('../../services/getUserInfo');
const config = require('../../config/config');

module.exports = async (post, authorId, req) => {
    const accessToken =req.auth.accessToken,
          authorInfo =await getUserInfo(authorId, accessToken, req);

    const images =[];
        //   dataDir = encStr(req.auth.dbKey),
        //   base64Str =post.img ? encStr(`public/news/${post.img}`) : '',
        //   imagePath =`${config.storageHost}/main.php?g=bG9ZhEltYWdl&path=${base64Str}&accessToken=${accessToken}&dataDir=${dataDir}`;

    if(post.img)  images.push({
        itemId: post.id,
        path: imagePath
    });

    let groupName =['Захидал'];
    // if(post.group_id)  {
    //     const groupNames =await getGroupName(post.group_id, req.auth.dbKey);
    //     groupName =groupName.concat(groupNames);
    // }

    let letter =post.letter.split('---------------------------------------------------------------------------------------- Өмнөх захидал')[0];

    return {
        postTitle: showText(post.subject),
        postDescription: post.letter,
        authorInfo: authorInfo,
        easyDate: mkEasyDate(post.date),
        fullDate: moment(post.date *1000).format('YY/MM/DD - HH:mm'),
        images: images,
        groupName: groupName,
        imageCnt: 0
    };
};
