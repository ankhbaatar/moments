const moment =require('moment');
const { showText, mkEasyDate, encStr, getGroupName } =require('../../helpers/globalFunction');
const { getUserInfo } =require('../../services/getUserInfo');
const config = require('../../config/config');

module.exports = async (post, req) => {
    const accessToken =req.auth.accessToken,
          authorInfo =await getUserInfo(post.autor_id, accessToken, req);

    const images =[],
          dataDir = encStr(req.auth.dbKey),
          base64Str =post.img ? encodeURIComponent(encStr(`public/vote/${post.img}`)) : '',
          imagePath =`${config.storageHost}/main.php?g=bG9ZhEltYWdl&path=${base64Str}&accessToken=${accessToken}&dataDir=${dataDir}`;

    if(post.img)  images.push({
        itemId: post.id,
        path: imagePath
    });

    let groupName =['Олон нийт', 'Санал асуулга'];
    // if(post.group_id)  {
        // const groupNames =await getGroupName(post.group_id, req.auth.dbKey);
        // groupName =groupName.concat(groupNames);
    // }

    return {
        postTitle: showText(post.title),
        postDescription: post.exp,
        authorInfo: authorInfo,
        easyDate: mkEasyDate(post.date),
        fullDate: moment(post.date *1000).format('YY/MM/DD - HH:mm'),
        images: images,
        groupName: groupName,
        imageCnt: 1
    };
};
