/* eslint-disable no-unused-vars */
const Mongoose = require('mongoose');
const { ObjectId } = require('mongodb');
const { Op } = require('sequelize');
const moment = require('moment');
const config = require('../../../config/config');

// responses
const newsResponse = require('../../responses/news_response');
const fileResponse = require('../../responses/file_response');
const filesResponse = require('../../responses/files_response');
const forumResponse = require('../../responses/forum_response');
const voteResponse = require('../../responses/vote_response');
const galleryResponse = require('../../responses/gallery_response');
const mailResponse = require('../../responses/mail_response');
const complainResponse = require('../../responses/complain_response');
const receivedResponse = require('../../responses/received_response');
const timeRequestResponse = require('../../responses/timeRequest_response');

// models
const { postByUser } = require('../../../models/mongo');

const postByUserModel = postByUser.model;

// services
const post = require('../../../services/post');
const postByNotify = require('../../../services/postByNotify');
const postJoinNotifyUser = require('../../../services/postJoinNotifyUser');
const notify = require('../../../services/notify');
const notifyByUser = require('../../../services/notifyByUser');
const notifyByUserBackUp = require('../../../services/notifyByUserBackUp');
const folder = require('../../../services/folder');
const files = require('../../../services/files');
const news = require('../../../services/news');
const file = require('../../../services/file');
const forum = require('../../../services/forum');
const vote = require('../../../services/vote');
const complain = require('../../../services/complain');
const complainByUser = require('../../../services/complainByUser');
const complainNumber = require('../../../services/complainNumber');
const complainCardShift = require('../../../services/complainCardShift');
const complainClose = require('../../../services/complainClose');
const received = require('../../../services/received');
const receivedByUser = require('../../../services/receivedByUser');
const receivedNumber = require('../../../services/receivedNumber');
const receivedCardShift = require('../../../services/receivedCardShift');
const receivedClose = require('../../../services/receivedClose');
const customer = require('../../../services/customer');
const person = require('../../../services/person');
const mail = require('../../../services/mail');
const mailByUser = require('../../../services/mailByUser');
const mailAddress = require('../../../services/mailAddress');
const mailFile = require('../../../services/mailFile');
const timeRequest = require('../../../services/timeRequest');
const timeGraphic = require('../../../services/timeGraphic');
const overTimeDesc = require('../../../services/overTimeDesc');
const timeState = require('../../../services/timeState');
const requestShift = require('../../../services/requestShift');
const agreeByUser = require('../../../services/agreeByUser');
const voteByUser = require('../../../services/voteByUser');
const gallery = require('../../../services/gallery');
const comment = require('../../../services/comment');
const plus = require('../../../services/plus');
const viewUsers = require('../../../services/viewUsers');
const postErrors = require('../../../services/postErrors');
const { getAnswerInfo } = require('../../../services/getAnswerInfo');
const { getCommentInfo } = require('../../../services/getCommentInfo');
const { getUserInfo } =require('../../../services/getUserInfo');
const { showText, mkEasyDate, getSpecialComment, encStr } =require('../../../helpers/globalFunction');

// user errors
const userErrors = {
    EXIST: 'rules_exist',
};

let lastInsertItemId = null,
    week =['Ням','Даваа','Мягмар','Лхагва','Пүрэв','Баасан','Бямба'];

module.exports.list =async (req, res, next) => {
    try {
        const postResult ={},
        accessToken =req.auth.accessToken,
        myInfo =await getUserInfo(req.auth.myHID, accessToken, req);

        postResult['datas'] =[];
        const tableName = req.query.table || 'notify_byuser_k';
        const allCnt =await postJoinNotifyUser.findAllCnt(req, tableName);
        // if(allCnt <= parseInt(req.query.limit) *3 +3)  postResult['theEnd'] =true;
        // else  postResult['theEnd'] =false;
        if(allCnt <= parseInt(req.query.limit) *3 +3)  {
            if(req.query.table =='notify_byuser_backup')  {
                postResult['theEnd'] =true;
                postResult['table'] ='notify_byuser_backup';
                postResult['page'] =parseInt(req.query.limit) +1;
                postResult['isBackUp'] =true;
            }
            else  {
                const backUpAllCnt =await postJoinNotifyUser.findAllCnt(req, 'notify_byuser_backup');
                if(backUpAllCnt)  {
                    postResult['theEnd'] =false;
                    postResult['table'] ='notify_byuser_backup';
                    postResult['page'] =0;
                    postResult['isBackUp'] =true;
                }
                else  {
                    postResult['theEnd'] =true;
                    postResult['table'] ='notify_byuser_backup';
                    postResult['page'] =parseInt(req.query.limit) +1;
                    postResult['isBackUp'] =true;
                }
            }
        }
        else  {
            postResult['theEnd'] =false;
            postResult['table'] =req.query.table;
            postResult['page'] =parseInt(req.query.limit) +1;
            if(req.query.table =='notify_byuser_backup')  postResult['isBackUp'] =true;
            else  postResult['isBackUp'] =false;
        }

        const result =await postJoinNotifyUser.findAll(req);

        for (var i =0; i <result.length; i++) {
            const titleResult =await postByUserModel.findOne({
                $and: [{
                    userId: req.auth.myHID,
                    comId: req.auth.myComId,
                    postId: result[i].postId
                }],
            });

            if (!titleResult) {
                await postByUserModel.insertMany({
                    userId: req.auth.myHID,
                    comId: req.auth.myComId,
                    postId: result[i].postId
                });
                // result[i]['title'] =titleInsert[0]['action'];
            }

            const post ={},
                itemId =result[i].itemId,
                postPro =result[i].pro,
                postMod =result[i].module,
                notifyId =result[i].notify_id,
                filesArray =[];
            
            let postInfo =null,
                comments =null,
                commentAll =null,
                commentRootAll =null,
                targetId =null,
                upReplyCnt =0;
                actionBar =null;

            if(postPro =='public')  {
                switch(postMod)  {
                    case 'news':
                        const newsResult =await news.findOne('`id` ='+ itemId, req);
                        postInfo =await newsResponse(newsResult[0], req);
                        break;
                    case 'file':
                        const fileResult =await file.findOne('`id` ='+ itemId, req);
                        postInfo =await fileResponse(fileResult[0], req);
                        break;
                    case 'forum':
                        const forumResult =await forum.findOne('`id` ='+ itemId, req);
                        postInfo =await forumResponse(forumResult[0], req);
                        break;
                    case 'gallery':
                        const galleryResult =await gallery.findOne('`id` IN ('+ itemId +')', req);
                        postInfo =await galleryResponse(galleryResult, req);
                        break;
                    case 'vote':
                        const voteInfo ={},
                            now =moment().unix(),
                            voteResult =await vote.findOne('`id` ='+ itemId, req),
                            voteByUserResult =await voteByUser.findOne('`vote_id` ='+ itemId, req);

                        postInfo =await voteResponse(voteResult[0], req);

                        const date =parseInt(voteResult[0].date),
                            duration =parseInt(voteResult[0].duration),
                            tmp =parseInt((now -date)/86400),
                            endDate =moment((date +(duration *86400)) *1000).format('MM/DD');

                        if(duration -tmp ==0)  voteInfo['remainDays'] =endDate +' (Эцсийн өдөр)';
                        if(duration -tmp >0)  voteInfo['remainDays'] =endDate +' ('+ (duration -tmp) +' хоног)';
                        if(duration -tmp <0)  {
                            voteInfo['remainDays'] =endDate +' (Дууссан)';
                            voteInfo['expired'] =true;
                        }
                        voteInfo['totalVote'] =voteByUserResult.length;
                        if(voteResult[0].status)  voteInfo['secret'] =true;
                        if(voteResult[0].vote_type)  voteInfo['multiple'] =true;
                        voteInfo['answers'] =await getAnswerInfo(itemId, true, req);
                        let allUserCnt =null;
                        if(tableName =='notify_byuser_k')  allUserCnt =await notifyByUser.count('`notify_id`='+ notifyId, req);
                        else  allUserCnt =await notifyByUserBackUp.count('`notify_id`='+ notifyId, req);
                        voteInfo['allUserCnt'] =allUserCnt +1;
                        post['voteInfo'] =voteInfo;
                        break;
                }

                // Сэтгэгдэлийг унших хэсэг
                commentAll =await comment.count('`mid` IN ('+ itemId +') AND `pro`="'+ postPro +'" AND `module`="'+ postMod +'" ORDER BY `date` DESC', req);
                commentRootAll =await comment.count('`mid` IN ('+ itemId +') AND `reply_id`=0 AND `pro`="'+ postPro +'" AND `module`="'+ postMod +'" ORDER BY `date` DESC', req);

                // if(result[i]['title'].length >1 && result[i]['title'][1].actionKey =='addComment' && commentRootAll && result[i]['title'][1]['actionId'])  {
                if(titleResult && titleResult.lastAction && titleResult.lastAction.actionKey =='addComment' && commentRootAll && titleResult.lastAction.actionItemId)  {
                    const commentInfo =await comment.findOne('`id`='+ titleResult.lastAction.actionItemId, req);
                    if(commentInfo.length)  {
                        targetId =titleResult.lastAction.actionItemId;
                        const specialComments =await getSpecialComment(targetId, req);
                        comments =specialComments.replies;
                        upReplyCnt =specialComments.upReplyCnt;
                    }
                    else  {
                        let commentResult =await comment.findOne('`mid` IN ('+ itemId +') AND `reply_id`=0 AND `pro`="'+ postPro +'" AND `module`="'+ postMod +'" ORDER BY `date` DESC LIMIT 0,3', req);
                        comments =await getCommentInfo(commentResult, req);
                    }
                }
                else  {
                    if(commentRootAll)  {
                        let commentResult =await comment.findOne('`mid` IN ('+ itemId +') AND `reply_id`=0 AND `pro`="'+ postPro +'" AND `module`="'+ postMod +'" ORDER BY `date` DESC LIMIT 0,3', req);
                        comments =await getCommentInfo(commentResult, req);
                    }
                }
            }
            else if(postPro =='mail')  {
                const mailResult =await mail.findOne('`id` ='+ itemId, req),
                      mailAuthor =await mailByUser.findOne('`mail_id`='+ itemId +' AND `state`="sent"', req),
					  mailUsersInfo ={};

                if(mailResult[0].mail_type == 0 || mailResult[0].mail_type == 3)  {
                    let counter=0;
                    let tmpCnt =await mailAddress.count('`mail_id`='+ itemId +' AND `state`!="reply_to" AND `state`!="sender" AND `state`!="from"', req);
                    let outCnt =(tmpCnt >0) ? tmpCnt-1 : tmpCnt;
                    let inCnt =null;
                    if(mailResult[0].msg_id)  inCnt =await mailByUser.count('`mail_id`='+ itemId +' AND user_id='+ req.auth.myHID +' AND `state`="inbox" AND `is_bcc`=0', req);
                    else inCnt =await mailByUser.count('`mail_id`='+ itemId +' AND user_id!=0 AND `state`="inbox" AND `is_bcc`=0', req);
                    counter =outCnt + inCnt;
                    if(counter > 1)  post['replyAll']  =true;
                }

				let toUsersIcon =[],
                    ccUsersIcon =[],
                    bccUsersIcon =[];

                const mailToUsers =await mailByUser.findAll('`mail_id`='+ itemId +' AND `is_to`=1 AND `state`="inbox"', req);
				let toUserCnt =mailToUsers.length,
					toUserLen =(toUserCnt >= 3) ? 3 : toUserCnt;
				for(let i =0; i <toUserLen; i++)  {
					const toUserInfo =await getUserInfo(mailToUsers[i].user_id, accessToken, req);
                    toUsersIcon.push({
                        path: toUserInfo.user_icon,
                        gender: (toUserInfo.gender ? 'male' : 'female'),
                        age: toUserInfo.age,
                        overHint: toUserInfo.system_name
                    });
                }

                const mailCcUsers =await mailByUser.findAll('`mail_id`='+ itemId +' AND `is_cc`=1 AND `state`="inbox"', req);
				let ccUserCnt =mailCcUsers.length;
				let ccUserLen =(ccUserCnt >= 3) ? 3 : ccUserCnt;
				for(let i =0; i <ccUserLen; i++)  {
					const ccUserInfo =await getUserInfo(mailCcUsers[i].user_id, accessToken, req);
                    ccUsersIcon.push({
                        path: ccUserInfo.user_icon,
                        gender: (ccUserInfo.gender ? 'male' : 'female'),
                        age: ccUserInfo.age,
                        overHint: ccUserInfo.system_name
                    });
                }

                const mailBccUsers =await mailByUser.findAll('`mail_id`='+ itemId +' AND `is_bcc`=1 AND `user_id`='+ req.auth.myHID +' AND `state`="inbox"', req);

				if(mailBccUsers.length)  {
					const bccUserInfo =await getUserInfo(mailBccUsers[0].user_id, accessToken, req);
                    bccUsersIcon.push({
                        path: bccUserInfo.user_icon,
                        gender: (bccUserInfo.gender ? 'male' : 'female'),
                        age: bccUserInfo.age,
                        overHint: bccUserInfo.system_name
                    });
					mailUsersInfo['bccUsersIcon'] =bccUsersIcon;
				}

				mailUsersInfo['toUserCnt'] =toUserCnt;
				mailUsersInfo['toUsersIcon'] =toUsersIcon;
				if(ccUserCnt)  {
					mailUsersInfo['ccUserCnt'] =ccUserCnt;
					mailUsersInfo['ccUsersIcon'] =ccUsersIcon;
				}

				post['mailUsers'] =mailUsersInfo;

                const mailFileResult =await mailFile.findAll('`mail_id`='+ itemId, req);

                for(let i =0; i <mailFileResult.length; i++)  {
                    const fileNameArr =mailFileResult[i].name.split('.'),
                          fileType =fileNameArr[fileNameArr.length -1],
                          base64Str =mailFileResult[i].name ? encStr(`${'mail'}/${mailResult[0].folder}/${mailFileResult[i].name}`) : '',
                          filePath =`${config.storageHost}/main.php?g=ZG9b3mxvYWQ=&path=${base64Str}&accessToken=${accessToken}`;

                    filesArray.push({
                        name: mailFileResult[i].name,
                        type: fileType,
                        dir: filePath
                    });
                }
                
                postInfo =await mailResponse(mailResult[0], mailAuthor[0].user_id, req);
            }
            else if(postPro =='docs')  {
                const mainTable =(postMod =='complain' ? complain : received),
                      numberTable =(postMod =='complain' ? complainNumber : receivedNumber),
                      byUserTable =(postMod =='complain' ? complainByUser : receivedByUser),
                      closeTable =(postMod =='complain' ? complainClose : receivedClose),
                      cardShiftTable =(postMod =='complain' ? complainCardShift : receivedCardShift),
                      docsResponce =(postMod =='complain' ? complainResponse : receivedResponse);

                const mainResult =await mainTable.findOne('`id` ='+ itemId, req);
                const numberResult =await numberTable.findOne('`doc_id` ='+ itemId, req);
                postInfo =await docsResponce(mainResult[0], req);
                post['autoNumber'] =numberResult[0].auto_year +'-'+ numberResult[0].auto_number;

                post['senderName'] =showText(mainResult[0].sender_name);

                if(postMod =='received' || (mainResult[0].sender_id >0 && mainResult[0].sender_type ==11))  {
                    post['senderName'] =showText(mainResult[0].sender_name);
                }
                else if(mainResult[0].sender_type >0 && mainResult[0].sender_type !=11)  {
                    let senderInfo =null;
                    if(mainResult[0].sender_type ==2)  senderInfo =await customer.findOne('`id`='+ mainResult[0].sender_id, req);
                    else senderInfo =await person.findOne('`id`='+ mainResult[0].sender_id, req);
                    post['senderName'] =showText(senderInfo[0].name);
                }

                post['shiftInfo'] =[];

                const closedResult =await closeTable.findOne('`doc_id`='+ itemId, req);

                if(closedResult.length)  {
                    const closedUserInfo =await getUserInfo(closedResult[0].user_id, req.auth.accessToken, req);

                    let diffDayString ='',
                        isLate =false;
                    if(mainResult[0].answer_date >100)  {
                        const answerDate =(mainResult[0].answer_snoozed_date >100 ? mainResult[0].answer_snoozed_date : mainResult[0].answer_date);
                        const diffDay =Math.ceil((closedResult[0].date -answerDate)/86400);
						if(diffDay ==0)  diffDayString ='Яг хугацаандаа';  else
						if(diffDay >0)   { diffDayString =Math.abs(diffDay) +' хоног хоцорсон'; isLate =true}
						else			diffDayString =Math.abs(diffDay) +' хоногийн өмнө';
					}

                    let receivedDate =await cardShiftTable.findOne('`doc_id`='+ itemId +' AND `receiver_id`='+ closedResult[0].user_id, req);
                    if(!receivedDate.length)  receivedDate.push({date: 0});
                    post.shiftInfo.push({
                        userInfo: {
                            name: closedUserInfo.system_name,
                            app: closedUserInfo.app_name,
                            path: {
                                path: closedUserInfo.user_icon,
                                gender: (closedUserInfo.gender ? 'male' : 'female'),
                                age: closedUserInfo.age,
                                overHint: closedUserInfo.system_name
                            }
                        },
                        receivedDate: (receivedDate[0].date ? mkEasyDate(receivedDate[0].date) : '(Танилцаагүй)'),
                        receivedDateHint: (receivedDate[0].date ? 'Танилцсан '+ moment(receivedDate[0].date *1000).format('YY/MM/DD : HH:mm') : 'Танилцаж амжаагүй'),
                        cardClose: true,
                        closedData: {
                            title: 'Хаалтын мэдээлэл',
                            answeredType: closedResult[0].type,
                            closedNote: closedResult[0].note,
                            answeredDate: moment(closedResult[0].date *1000).format('YYYY/MM/DD'),
                            answeredWeek: week[moment(closedResult[0].date *1000).day()] +' ('+ diffDayString +')',
                            isLate: isLate
                        }
                    });
                }
                else  {
                    const shifts =await cardShiftTable.findAll('`doc_id`='+ itemId +' ORDER BY `id` ASC', req);
                    let shiftStart =(shifts.length >2 ? shifts.length -2 : 0);
                    for(let i =shiftStart; i <shifts.length; i++)  {
                        let shift =shifts[i];
                        let receivedDateString =(shift.date ? mkEasyDate(shift.date) : '(Танилцаагүй)');
                        let receivedDateHint =(shift.date ? 'Танилцсан '+ moment(shift.date *1000).format('YY/MM/DD : HH:mm') : 'Танилцаж амжаагүй');
                        const shiftUserInfo =await getUserInfo(shift.receiver_id, req.auth.accessToken, req);
                        if(i ==shifts.length-1)  {
                            let obj ={
                                userInfo: {
                                    name: shiftUserInfo.system_name,
                                    app: shiftUserInfo.app_name,
                                    path: {
                                        path: shiftUserInfo.user_icon,
                                        gender: (shiftUserInfo.gender ? 'male' : 'female'),
                                        age: shiftUserInfo.age,
                                        overHint: shiftUserInfo.system_name
                                    }
                                },
                                receivedDate: receivedDateString,
                                receivedDateHint: receivedDateHint,
                                lastShift: true
                            };

                            if(shift.receiver_id == req.auth.myHID)  obj['holderMe'] =true;
                            post.shiftInfo.push(obj);
                        }
                        else  {
                            post.shiftInfo.push({
                                userInfo: {
                                    name: shiftUserInfo.system_name,
                                    app: shiftUserInfo.app_name,
                                    path: {
                                        path: shiftUserInfo.user_icon,
                                        gender: (shiftUserInfo.gender ? 'male' : 'female'),
                                        age: shiftUserInfo.age,
                                        overHint: shiftUserInfo.system_name
                                    }
                                },
                                receivedDate: receivedDateString,
                                receivedDateHint: receivedDateHint,
                                ruleData: {
                                    title: 'Удирдлагын заалт',
                                    note: shift.rule,
                                    ruleDate: mkEasyDate(shift.shift_date)
                                }
                            });
                        }
                    }
                }

                post['shiftCnt'] =await cardShiftTable.count('`doc_id`='+ itemId, req);

                post['isAnswer'] =false;

                if(mainResult[0].answer_date >100)  {
                    const now =moment().unix();

                    post['isAnswer'] =true;
                    post['pastDay'] =parseInt((now -numberResult[0].received_date)/86400);
                    post['restDay'] =parseInt((mainResult[0].answer_date -now)/86400);
                    post['answerDate'] =moment(mainResult[0].answer_date *1000).format('YY/MM/DD');
                    post['week'] =week[moment(mainResult[0].answer_date *1000).day()] +' гариг';
                }

                post['sharedUser'] =[];

                const byUserResult =await byUserTable.findAll('`doc_id`='+ itemId +' GROUP BY `user_id`', req);
                post['sharedUserCnt'] =byUserResult.length;

                const sharedUserLength =(byUserResult.length > 5 ? 5 : byUserResult.length);

                for(let k =0; k <sharedUserLength; k++)  {
                    let sharedUserInfo =await getUserInfo(byUserResult[k].user_id, req.auth.accessToken, req);

                    post.sharedUser.push({
                        path: sharedUserInfo.user_icon,
                        gender: (sharedUserInfo.gender ? 'male' : 'female'),
                        age : sharedUserInfo.age,
                        overHint: sharedUserInfo.system_name
                    });
                }

            // Сэтгэгдэлийг унших хэсэг
                commentAll =await comment.count('`mid` IN ('+ itemId +') AND `pro`="'+ postPro +'" AND `module`="'+ postMod +'" ORDER BY `date` DESC', req);
                commentRootAll =await comment.count('`mid` IN ('+ itemId +') AND `reply_id`=0 AND `pro`="'+ postPro +'" AND `module`="'+ postMod +'" ORDER BY `date` DESC', req);

                // if(result[i]['title'].length >1 && result[i]['title'][1].actionKey =='addComment' && commentRootAll && result[i]['title'][1]['actionId'])  {
                if(titleResult && titleResult.lastAction && titleResult.lastAction.actionKey =='addComment' && commentRootAll && titleResult.lastAction.actionItemId)  {
                    const commentInfo =await comment.findOne('`id`='+ titleResult.lastAction.actionItemId, req);
                    if(commentInfo.length)  {
                        targetId =titleResult.lastAction.actionItemId;
                        const specialComments =await getSpecialComment(targetId, req);
                        comments =specialComments.replies;
                        upReplyCnt =specialComments.upReplyCnt;
                    }
                    else  {
                        let commentResult =await comment.findOne('`mid` IN ('+ itemId +') AND `reply_id`=0 AND `pro`="'+ postPro +'" AND `module`="'+ postMod +'" ORDER BY `date` DESC LIMIT 0,3', req);
                        comments =await getCommentInfo(commentResult, req);
                    }
                }
                else  {
                    if(commentRootAll)  {
                        let commentResult =await comment.findOne('`mid` IN ('+ itemId +') AND `reply_id`=0 AND `pro`="'+ postPro +'" AND `module`="'+ postMod +'" ORDER BY `date` DESC LIMIT 0,3', req);
                        comments =await getCommentInfo(commentResult, req);
                    }
                }
            }
            else if(postPro =='time_access')  {
                const timeResult =await timeRequest.findOne('`id`='+ itemId, req);
                let descResult =null,
                    stateResult =null;
                if(timeResult[0].type ==1)  descResult =await overTimeDesc.findOne('`id`='+ timeResult[0].desc_id, req);
                if(timeResult[0].type ==4)  stateResult =await timeState.findOne('`id`='+ timeResult[0].state_id, req);
                postInfo =await timeRequestResponse(timeResult[0], descResult, stateResult, req);

                post['shiftInfo'] =[];

                const shiftCnt =await requestShift.count('`item_id`='+ itemId, req);
                post['sharedUserCnt'] =shiftCnt;
                post['shiftCnt'] =shiftCnt;

                if(timeResult[0].state ==1)  {
                    const requestShiftResult =await requestShift.findAll('`item_id`='+ itemId +' ORDER BY `id` DESC LIMIT 1', req);

                    const closedUserInfo =await getUserInfo(requestShiftResult[0].sender_id, req.auth.accessToken, req);

                    post.shiftInfo.push({
                        userInfo: {
                            name: closedUserInfo.system_name,
                            app: closedUserInfo.app_name,
                            path: {
                                path: closedUserInfo.user_icon,
                                gender: (closedUserInfo.gender ? 'male' : 'female'),
                                age: closedUserInfo.age,
                                overHint: closedUserInfo.system_name
                            }
                        },
                        receivedDate: (requestShiftResult[0].received_date ? mkEasyDate(requestShiftResult[0].received_date) : '(Танилцаагүй)'),
                        receivedDateHint: (requestShiftResult[0].received_date ? 'Танилцсан '+ moment(requestShiftResult[0].received_date *1000).format('YY/MM/DD : HH:mm') : 'Танилцаж амжаагүй'),
                        cardClose: true,
                        closedData: {
                            title: 'Хаалтын мэдээлэл',
                            answeredType: '',
                            closedNote: requestShiftResult[0].note,
                            answeredDate: moment(timeResult[0].confirm_date *1000).format('YYYY/MM/DD'),
                            answeredWeek: week[moment(timeResult[0].confirm_date *1000).day()] +' гариг',
                            isLate: false
                        }
                    });
                }
                else  {
                    const requestShiftResult =await requestShift.findAll('`item_id`='+ itemId +' ORDER BY `id` DESC LIMIT 0,2', req);
                    const superUser =await agreeByUser.count('`user_id`='+ req.auth.myHID, req);
                    
                    requestShiftResult.reverse();

                    for(let i =0; i <requestShiftResult.length; i++)  {
                        let shift =requestShiftResult[i];
                        let receivedDateString =(shift.received_date ? mkEasyDate(shift.received_date) : '(Танилцаагүй)');
                        let receivedDateHint =(shift.received_date ? 'Танилцсан '+ moment(shift.received_date *1000).format('YY/MM/DD : HH:mm') : 'Танилцаж амжаагүй');
                        const shiftUserInfo =await getUserInfo(shift.sender_id, req.auth.accessToken, req);

                        if(i ==1)  {
                            let obj ={
                                userInfo: {
                                    name: shiftUserInfo.system_name,
                                    app: shiftUserInfo.app_name,
                                    path: {
                                        path: shiftUserInfo.user_icon,
                                        gender: (shiftUserInfo.gender ? 'male' : 'female'),
                                        age: shiftUserInfo.age,
                                        overHint: shiftUserInfo.system_name
                                    }
                                },
                                receivedDate: receivedDateString,
                                receivedDateHint: receivedDateHint,
                                lastShift: true
                            };
                            
                            if(shift.sender_id == req.auth.myHID)  obj['holderMe'] =true;
                            if(superUser >0 && shift.sender_id == req.auth.myHID)  {
                                obj['superUser'] =true;
                                obj['holderMe'] =true;
                            }
                            post.shiftInfo.push(obj);
                        }
                        else  {
                            post.shiftInfo.push({
                                userInfo: {
                                    name: shiftUserInfo.system_name,
                                    app: shiftUserInfo.app_name,
                                    path: {
                                        path: shiftUserInfo.user_icon,
                                        gender: (shiftUserInfo.gender ? 'male' : 'female'),
                                        age: shiftUserInfo.age,
                                        overHint: shiftUserInfo.system_name
                                    }
                                },
                                receivedDate: receivedDateString,
                                receivedDateHint: receivedDateHint,
                                ruleData: {
                                    title: 'Удирдлагын заалт',
                                    note: shift.note,
                                    ruleDate: mkEasyDate(shift.shift_date)
                                }
                            });
                        }
                    }
                }

                const firstMan =await timeGraphic.findOne('`item_id`='+ itemId +' ORDER BY `id` ASC LIMIT 1', req);
                const graphicResult =await timeGraphic.findOne('`item_id`='+ itemId +' AND `user_id`='+ firstMan[0].user_id +' ORDER BY `sdate`', req);

                post['timeRequestType'] =timeResult[0].type;
                if(timeResult[0].type ==5)  {
                    post['requestDate'] = moment(graphicResult[0].sdate *1000).format('YYYY/MM/DD HH:mm');
                    post['requestStatus'] = (graphicResult[0].type_id ==1 ? 'Ажил эхлэх цаг' : 'Ажил тарах цаг');
                }
                else if(timeResult[0].type ==4 || timeResult[0].type ==8)  {
                    let requestDays =[];
                    for(let i =0; i <graphicResult.length; i++)  {
                        requestDays.push(moment(graphicResult[i].sdate *1000).format('YYYY/MM/DD'));
                    }
                    post['requestDays'] =requestDays;
                    post['reqSTime'] =moment(graphicResult[0].sdate *1000).format('HH:mm');
                    post['reqETime'] =moment(graphicResult[0].edate *1000).format('HH:mm');
                }
            }

        //--Action title ийн дата
            if(titleResult && titleResult.lastAction)  {
                const actionInfo =titleResult.lastAction,
                      actionAuthorId =actionInfo.authorId,
                      actionAuthorInfo =await getUserInfo(actionAuthorId, accessToken, req);

                actionBar ={
                    title: actionInfo.actionKey,
                    date: mkEasyDate(moment(result[i].updatedAt).unix()),
                    actionFullDate: moment(result[i].updatedAt).format('YY/MM/DD - HH:mm'),
                    actionAuthorInfo
                }
    
                post['isExtended'] =true;
            }
            else  post['isExtended'] =false;

        //--Хавсралт файлыг уншина
            if(postPro !='mail')  {
                let folderResult =null,
                filesResult =null;

                folderResult =await folder.findOne("`item_id` IN ("+ itemId +") AND `pro`='"+ postPro +"' AND `module`='"+ postMod +"'", req);
                if(folderResult.length)  filesResult =await files.findOne("`folder_id`="+ folderResult[0].id, req);

                if(filesResult)
                    for(let i =0; i <filesResult.length; i++)  {
                        if(folder)  filesArray.push(await filesResponse(filesResult[i], folderResult, req.auth.dbKey, postPro, postMod, accessToken));
                    }
            }

        //--Постын пласыг уншина
            
            const plusCnt =await plus.findAll('`item_id` IN ('+ itemId +') AND `type`="post" AND `pro`="'+ postPro +'" AND `mod`="'+ postMod +'"', req);
            for(let i=0; i <plusCnt.length; i++)  {
                if(plusCnt[i].user_id == req.auth.myHID)  {
                    post['hasMe'] =true;
                    break;
                }
            }

        //--Постыг уншсан эсэхийг шалгана
            post['seen'] =false;
            const seenResult = await viewUsers.findOne('`userId`='+ req.auth.myHID +' AND `comId`='+ req.auth.myComId +' AND `pro`="'+ postPro +'" AND `mod`="'+ postMod +'" AND `itemId`="'+ itemId +'"', req);

            if(seenResult.length)  post['seen'] =true;

            post['holder'] ={
                Id: req.auth.myHID,
                name: myInfo.system_name,
                icon: {path: myInfo.user_icon, gender: (myInfo.gender ? 'male' : 'female'), age: myInfo.age}
            };

            post['postId'] =result[i].postId;
            post['itemId'] =itemId;
            post['pro'] =postPro;
            post['encPro'] =encStr(postPro);
            post['module'] =postMod;
            post['encModule'] =encStr(postMod);
            post['title'] =postInfo.postTitle;
            post['description'] =postInfo.postDescription;
            post['authorInfo'] =postInfo.authorInfo;
            post['easyDate'] =postInfo.easyDate;
            post['fullDate'] =postInfo.fullDate;
            post['images'] =postInfo.images;
            post['imageCnt'] =postInfo.imageCnt;
            post['files'] =filesArray;
            post['actionBar'] =actionBar;
            post['groupName'] =postInfo.groupName;
            post['comments'] =comments;
            post['commentCnt'] =commentAll;
            post['commentRootCnt'] =commentRootAll;
            post['upReplyCnt'] =upReplyCnt;
            post['plusCnt'] =plusCnt.length;

            if(targetId)  post['targetId'] =targetId;

            postResult.datas.push(post);
        }

        res.success.OK('post.list', postResult);
    } catch (error) {
        const { redis } =req.app.get('redis');
        const errors = await redis.get('getPostError');
        const logs = [];
        if(errors)  {
            const objectArray = Object.entries(errors);
            objectArray.map(([key, value]) => {
                logs.push(value);
            });
        }
        logs.push({myHID: req.auth.myHID, type:'postError', error: error.toString(), date: moment().format('YY/MM/DD : HH:mm')});

        await redis.set('getPostError', logs);
        console.log('eeror', error);
        // const field ='`userId`,`comId`,`errorMsg`,`date`',
        //       value =req.auth.myHID +','+ req.auth.myComId +',"'+ error.toString() +'",'+ moment().unix();
        // await postErrors.insert(field, value, req);
    }
};

module.exports.create = async (req, res, next) => {
    let reqData =req.body.data;
    if(reqData.pro =='public' || reqData.pro =='mail' || (reqData.pro =='docs' && (reqData.module =='complain' || reqData.module =='received')) || (reqData.pro =='time_access' && reqData.module =='timeRequest'))  {
        console.log('notify aas irj chadal bn');
        const hasPost =await post.findOne("`itemId` LIKE '%"+ reqData.itemId +"%' AND `pro`='"+ reqData.pro +"' AND `module`='"+ reqData.module +"'", reqData.dbKey);

        if (!hasPost.length && lastInsertItemId === reqData.itemId && (reqData.actionKey == 'addComplain' || reqData.actionKey == 'addReceived')) {
            myTimeOut(reqData);
            return;
        }

        lastInsertItemId = reqData.itemId;

        if(!hasPost.length)  {
            let authorId =(reqData.pro == 'mail' ? 0 : reqData.authorId);
            let field ='`authorId`, `itemId`, `pro`, `module`';
            let value ='"'+ authorId +'", "'+ reqData.itemId +'", "'+ reqData.pro +'", "'+ reqData.module +'"';

            const result =await post.insert(field, value, reqData.dbKey);
            await postByNotify.insert('`postId`, `notifyId`', result.insertId +' ,'+ reqData.notifyId, reqData.dbKey);
        }
        else {
            let actionItemId =0;
            if(reqData.actionKey == 'addComplain' || reqData.actionKey == 'addReceived' || reqData.actionKey == 'addTimeRequest')  {
                await postByNotify.insert('`postId`, `notifyId`', hasPost[0].id +' ,'+ reqData.notifyId, reqData.dbKey);
            }
            if(reqData.hasOwnProperty('actionId'))  actionItemId =reqData.actionId;
            await postByUserModel.updateMany({
                $and: [{
                    postId: hasPost[0].id,
                    comId: reqData.comId
                }]
            },{ $set: {
                    lastAction: {
                        actionKey: reqData.actionKey,
                        authorId : reqData.authorId,
                        actionId: reqData.notifyId,
                        actionItemId: actionItemId
                    }
                }
            });

            await post.update('', "`itemId` LIKE '%"+ reqData.itemId +"%' AND `pro`='"+ reqData.pro +"' AND `module`='"+ reqData.module +"'", reqData.dbKey);
        }
        res.success.Created('Created');
    }
    else  {
        res.success.Created('Created');
    }
};

function myTimeOut(reqData)  {
    setTimeout(async function(){
        const basicPost =await post.findOne("`itemId` LIKE '%"+ reqData.itemId +"%' AND `pro`='"+ reqData.pro +"' AND `module`='"+ reqData.module +"'", reqData.dbKey);
        if(basicPost.length)  {
            await postByNotify.insert('`postId`, `notifyId`', basicPost[0].id +' ,'+ reqData.notifyId, reqData.dbKey);
            return;
        }
        else myTimeOut();
    }, 300);
}
