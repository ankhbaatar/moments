const Router = require('express').Router();
const { authentication } = require('../../middlewares');
// const validations = require('./validations.js');
const asyncRouteWrapper = require('../../../helpers/asyncRouteWrapper');
const controller = require('./controller.js');

Router.get.apply(Router, ['',
    authentication, 
    asyncRouteWrapper(controller.list)]);

Router.post.apply(Router, ['', asyncRouteWrapper(controller.create)]);

module.exports = Router;
