const { authorization } = require('../../../middlewares');

module.exports.show = authorization(req => {
    return req.USER_ID === req.params.id * 1;
});

module.exports.list = authorization(req => {
    // authorization
    return true;
});
