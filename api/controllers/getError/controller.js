/* eslint-disable no-unused-vars */
const Mongoose = require('mongoose');
const { ObjectId } = require('mongodb');
const { Op } = require('sequelize');
const { encStr } =require('../../../helpers/globalFunction');

const moment = require('moment');

// models
const { postByUser } = require('../../../models/mongo');

const postByUserModel = postByUser.model;

// responses
const commentResponse = require('../../responses/comment_response');

// services
const comment = require('../../../services/comment');
const plus = require('../../../services/plus');
const viewUsers = require('../../../services/viewUsers');
const { getCommentInfo } = require('../../../services/getCommentInfo');
const { getUserInfo } = require('../../../services/getUserInfo');

// user errors
const userErrors = {
    EXIST: 'rules_exist',
};

module.exports.show = async (req, res, next) => {};

module.exports.list = async (req, res, next) => {
    const { redis } =req.app.get('redis');
    const logs =await redis.get('getPostError');
    let errors =[];
    if(req.query.type)  {
        if(logs)  {
            const objectArray = Object.entries(logs);
            objectArray.map(([key, value]) => {
                if(value.type == req.query.type)  errors.push(value);
            });
        }
    }
    else  {
        errors = logs;
    }
    res.success.OK('error.list', errors);
};
