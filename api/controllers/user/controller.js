/* eslint-disable no-unused-vars */
const Mongoose = require('mongoose');
const { ObjectId } = require('mongodb');
const { Op } = require('sequelize');
const { encStr } =require('../../../helpers/globalFunction');

const moment = require('moment');

// services
const failedLogin = require('../../../services/failedLogin');
const confirmMsg = require('../../../services/confirmMsg');

// user errors
const userErrors = {
    EXIST: 'rules_exist',
};

module.exports.show = async (req, res, next) => {};

module.exports.clearBlock = async (req, res, next) => {
    const { userName, ip, mail } =req.query;
    let response ={};
    if(userName && ip)  {
        const result =await failedLogin.update('`accessed`=1', '`user_name`="'+ userName +'" AND `addr`="'+ ip +'" AND `accessed`=0');
        if(result)  {
            response['error'] =false;
            response['msg'] ='Амжилттай цэвэрлэлээ.';
            response['changedRows'] =result.changedRows;
        }
        else  {
            response['error'] =true;
            response['msg'] ='Алдаа гарлаа.';
        }
    }
    else {
        response['error'] =true;
        response['msg'] ='Хэрэглэгчийн нэвтрэх нэр эсвэл ip хаяг хоосон байна.';
    }
	
	if(mail) {
		await confirmMsg.update('`is_active`=0', '`receiver` LIKE "'+ mail +'" AND `is_active`=1');
	}
    res.success.OK('success', response);
};
