const Router = require('express').Router();
const { authentication } = require('../../middlewares');
const authorization =require('./authorizations');
// const validations = require('./validations.js');
const asyncRouteWrapper = require('../../../helpers/asyncRouteWrapper');
const controller = require('./controller.js');

Router.put.apply(Router, ['/clearUserBlock',asyncRouteWrapper(controller.clearBlock)]);

module.exports = Router;
