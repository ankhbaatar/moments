const Router = require('express').Router();
const { authentication } = require('../../middlewares');
// const validations = require('./validations.js');
const mongoModelBinding = require('../../../helpers/mongoModelBinding');
const asyncRouteWrapper = require('../../../helpers/asyncRouteWrapper');
const controller = require('./controller.js');

Router.post.apply(Router, ['',
    authentication,
    asyncRouteWrapper(controller.create)]);

Router.delete.apply(Router, ['',
    authentication,
    asyncRouteWrapper(controller.destroy)]);

module.exports = Router;
