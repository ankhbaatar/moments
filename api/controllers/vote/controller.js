/* eslint-disable no-unused-vars */
const Mongoose = require('mongoose');
const { ObjectId } = require('mongodb');
const { Op } = require('sequelize');

const moment = require('moment');

// responses

// services
const vote = require('../../../services/vote');
const voteByUser = require('../../../services/voteByUser');
const voteAnswer = require('../../../services/voteAnswer');
const viewUsers = require('../../../services/viewUsers');
const { getAnswerInfo } = require('../../../services/getAnswerInfo');

// user errors
const userErrors = {
    EXIST: 'rules_exist',
};

module.exports.create = async (req, res, next) => {
    const { voteId, answerId, type, authorId, hidden } =req.body,
          userId = req.auth.myHID,
          now = moment().unix(),
          fields ='`user_id`, `vote_id`, `answer_id`, `date`',
          value ="'"+ userId +"','"+ voteId +"','"+ answerId +"','"+ now +"'";
    let isAnswered =null;
    if(!type)  {
        isAnswered = await voteByUser.findOne('`user_id`='+ userId +' AND `vote_id`='+ voteId, req);
        if(!isAnswered.length)  await voteByUser.insert(fields, value, req);
        else await voteByUser.update('`answer_id`="'+ answerId +'"', '`user_id`='+ userId +' AND `vote_id`='+ voteId, req);
    }
    else  {
        isAnswered = await voteByUser.findOne('`user_id`='+ userId +' AND `vote_id`='+ voteId +' AND `answer_id`='+ answerId, req);
        if(!isAnswered.length)  await voteByUser.insert(fields, value, req);
    };

    //--Notify
    const sameAnswerUsers =[];
    if(!hidden)  {
        const sameAnswerResult =await voteByUser.findOne('`vote_id`='+ voteId +' AND `answer_id`='+ answerId +' AND `user_id`!='+ req.auth.myHID, req);
        for(let i= 0; i <sameAnswerResult.length; i++)  {
            if(!sameAnswerUsers.includes(sameAnswerResult[i].user_id))  sameAnswerUsers.push(sameAnswerResult[i].user_id);
        }
    }

    const viewUsersResult =await viewUsers.findAll('`pro`="public" AND `mod`="vote" AND itemId LIKE "%'+ voteId +'%" AND `comId`='+ req.auth.myComId +' AND `userId`!='+ req.auth.myHID, req);
    const seenUsers =[];
    for(let j= 0; j <viewUsersResult.length; j++)  {
        if(!seenUsers.includes(viewUsersResult[j].userId) && !sameAnswerUsers.includes(viewUsersResult[j].userId))  seenUsers.push(viewUsersResult[j].userId);
    }

    if(!sameAnswerUsers.includes(authorId) && !seenUsers.includes(authorId)) seenUsers.push(authorId);

    const answerResult =await voteAnswer.findOne('`id`='+ answerId +' AND `mid`='+ voteId, req);
    const voteByUserResult =await voteByUser.findOne('`vote_id` ='+ voteId, req);
    const result ={};
    result['notify'] ={
        users: seenUsers,
        itemId: voteId,
        pro: 'public',
        mod: 'vote',
        headText: 'санал өглөө',
        title: answerResult[0].body,
        content: '',
        url: 'main.php?p=cHiVbGlj&lisMod=dm09ZQ==&lisId='+ voteId,
        type: 'vote',
        actionKey: 'addAnswer'
    };

    if(!hidden)  {
        result['notify1'] ={
            users: sameAnswerUsers,
            itemId: voteId,
            pro: 'public',
            mod: 'vote',
            headText: 'тантай санал нэгдлээ!',
            title: answerResult[0].body,
            content: '',
            url: 'main.php?p=cHiVbGlj&lisMod=dm09ZQ==&lisId='+ voteId,
            type: 'vote',
            actionKey: 'addAnswer'
        };
    }
    result['totalVote'] =voteByUserResult.length;
    result['answers'] =await getAnswerInfo(voteId, false, req);
    res.success.Created('Created', result);
};

module.exports.destroy = async (req, res, next) => {
    const { voteId, answerId } =req.body;
    await voteByUser.destroy('`vote_id`='+ voteId +' AND `answer_id`='+ answerId +' AND `user_id`='+ req.auth.myHID, req);
    const voteByUserResult =await voteByUser.findOne('`vote_id` ='+ voteId, req);
    const result ={};
    result['totalVote'] =voteByUserResult.length;
    result['answers'] =await getAnswerInfo(voteId, false, req);
    res.success.Created('success', result);
};
