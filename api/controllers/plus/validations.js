const { body } = require('express-validator');
const { validation } = require('../../../middlewares');

module.exports.show = [validation];

module.exports.list = [validation];

module.exports.create = [
    body('name').notEmpty().isString().trim().escape(),
    body('authorId').notEmpty(),
    body('roomType').notEmpty(),
    body('users').not().isEmpty().isArray(),
    validation,
];

module.exports.update = [
    body('lastName').notEmpty().isString().trim().escape(),
    body('firstName').notEmpty().isString().trim().escape(),
    body('address').notEmpty(),
    body('type').notEmpty(),
    // body('lessons').notEmpty(),
    validation,
];

module.exports.rate = [body('rate').isInt(), validation];

module.exports.destroy = () => {
    return true;
};
