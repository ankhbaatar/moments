/* eslint-disable no-unused-vars */
const Mongoose = require('mongoose');
const { ObjectId } = require('mongodb');
const { Op } = require('sequelize');
const { encStr } =require('../../../helpers/globalFunction');

const moment = require('moment');
const { pick } = require('lodash');

// models
const { postByUser } = require('../../../models/mongo');

const postByUserModel = postByUser.model;

// services
const post = require('../../../services/post');
const plus = require('../../../services/plus');
const viewUsers = require('../../../services/viewUsers');

// user errors
const userErrors = {
    EXIST: 'rules_exist',
};

module.exports.create = async (req, res, next) => {
    const now = moment().unix();
    const { itemId, type, pro, mod } =req.body;
    let fields ='`item_id`, `user_id`, `type`, `date`',
        value ="'"+ itemId +"','"+ req.auth.myHID +"','"+ type +"','"+ now +"'",
        where ='`item_id`='+ itemId +' AND `type`="'+ type +'"';
    if(type =='post')  {
        fields +=', `pro`, `mod`';
        value +=",'"+ pro +"','"+ mod +"'";
        where +=' AND `pro`="'+ pro +'" AND `mod`="'+ mod +'"';
    }
    const insertResult =await plus.insert(fields, value, req);
    const result ={};
    if(type =='post')  {
        const viewUsersResult =await viewUsers.findAll('`pro`="'+ pro +'" AND `mod`="'+ mod +'" AND itemId LIKE "%'+ itemId +'%" AND `comId`='+ req.auth.myComId +' AND `userId`!='+ req.auth.myHID, req);
        const users =[];
        if(req.body.authorId !=req.auth.myHID) users.push(req.body.authorId);
        for(let i= 0; i <viewUsersResult.length; i++)  {
            if(!users.includes(viewUsersResult[i].userId))  users.push(viewUsersResult[i].userId);
        }
        let notifyItemId =null;
        if(pro =='public' && mod =='gallery')  notifyItemId =itemId.split(',')[0];
        else notifyItemId =itemId;
        result['notify'] ={
            users: users,
            itemId: notifyItemId,
            pro: pro,
            mod: mod,
            headText: 'дэмжлээ',
            title: '',
            content: '',
            url: 'main.php?p=cHiVbGlj&lisMod='+ encStr(mod) +'&lisId='+ itemId,
            hidden: 0,
            type: 'plus',
            actionKey: 'addPlus',
            actionId: insertResult.insertId
        };
    }
    const plusResult =await plus.findAll(where, req);
    result['length'] =plusResult.length;
    res.success.Created('Created', result);
};

module.exports.destroy = async (req, res, next) => {
    const { itemId, type, pro, mod } =req.body;
    let delWhere ='`item_id`='+ itemId +' AND `user_id`='+ req.auth.myHID +' AND `type`="'+ type +'"',
        findWhere ='`item_id`='+ itemId +' AND `type`="'+ type +'"';
    let postUserWhere ={};
    if(type == 'post')  {
        delWhere +=' AND `pro`="'+ pro +'" AND `mod`="'+ mod +'"';
        findWhere +=' AND `pro`="'+ pro +'" AND `mod`="'+ mod +'"';
        const plusDelResult =await plus.findOne(delWhere, req),
              postResult =await post.findOne('`itemId` LIKE "%'+ itemId +'%" AND `pro`="'+ pro +'" AND `module`="'+ mod +'"', req.auth.dbKey);

        postUserWhere ={
            "postId": postResult[0].id,
            "lastAction.actionKey": 'addPlus',
            "lastAction.actionItemId": plusDelResult[0].id
        };
    }
    await plus.destroy(delWhere, req);
    await postByUserModel.updateMany(postUserWhere, 
        { $set: {
            lastAction: null
        }
    });
    const plusResult =await plus.findAll(findWhere, req);
    const result ={
        length: plusResult.length
    }
    res.success.OK('success', result);
};
