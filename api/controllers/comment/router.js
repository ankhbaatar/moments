const Router = require('express').Router();
const { authentication } = require('../../middlewares');
const authorization =require('./authorizations');
// const validations = require('./validations.js');
const asyncRouteWrapper = require('../../../helpers/asyncRouteWrapper');
const controller = require('./controller.js');

Router.get.apply(Router, ['',
    authentication,
    asyncRouteWrapper(controller.list)]);

Router.post.apply(Router, ['',
    authentication,
    asyncRouteWrapper(controller.create)]);

Router.put.apply(Router, ['',
    authentication,
    // authorization.update,
    asyncRouteWrapper(controller.update)]);

Router.delete.apply(Router, ['',
    authentication,
    asyncRouteWrapper(controller.destroy)]);

module.exports = Router;
