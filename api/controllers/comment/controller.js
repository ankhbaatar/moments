/* eslint-disable no-unused-vars */
const Mongoose = require('mongoose');
const { ObjectId } = require('mongodb');
const { Op } = require('sequelize');
const { encStr } =require('../../../helpers/globalFunction');

const moment = require('moment');

// models
const { postByUser } = require('../../../models/mongo');

const postByUserModel = postByUser.model;

// responses
const commentResponse = require('../../responses/comment_response');

// services
const comment = require('../../../services/comment');
const plus = require('../../../services/plus');
const viewUsers = require('../../../services/viewUsers');
const { getCommentInfo } = require('../../../services/getCommentInfo');
const { getUserInfo } = require('../../../services/getUserInfo');

// user errors
const userErrors = {
    EXIST: 'rules_exist',
};

module.exports.show = async (req, res, next) => {};

module.exports.list = async (req, res, next) => {
    const { pro, module, itemId, parentId, guideCommId, way, notIn} =req.query;
    const order =(way == 'up' ? 'ASC' : 'DESC');
    const operator = (way =='up' ? '>' : '<');
    const commentResult =await comment.findOne('`mid` IN ('+ itemId +') AND '+(notIn ? '`id` NOT IN ('+ notIn +') AND' : '')+' `reply_id`='+ parentId +' AND '+ (guideCommId !=0 ? '`id` '+ operator +''+ guideCommId +' AND' : '') +' `pro`="'+ pro +'" AND `module`="'+ module +'" ORDER BY `date` '+ order +' LIMIT 0,3', req);
    // const commentResult =await comment.findOne('`mid` IN ('+ itemId +') AND `reply_id`='+ parentId +' AND '+ (guideCommId !=0 ? '`id` <'+ guideCommId +' AND' : '') +' `pro`="'+ pro +'" AND `module`="'+ module +'" ORDER BY `date` DESC LIMIT 0,3', req);

    const comments =await getCommentInfo(commentResult, req);
    res.success.OK('comment.list', comments);
};

module.exports.create = async (req, res, next) => {
    const commentText = req.body.comment,
          { itemId, parentId, pro, mod, authorId } =req.body
    const now = moment().unix();
    const fields ='`mid`, `reply_id`, `user_id`, `pro`, `module`, `comment`, `date`',
          value ="'"+ itemId +"','"+ parentId +"','"+ req.auth.myHID +"','"+ pro +"','"+ mod +"','"+ commentText +"','"+ now +"'";
    const result =await comment.insert(fields, value, req);

    const commentResult =await comment.findOne('`mid` IN ('+ itemId +') AND `reply_id`='+ parentId +' AND `id`='+ result.insertId +' AND `pro`="'+ pro +'" AND `module`="'+ mod+'"', req);
    
    const comments =await getCommentInfo(commentResult, req);
    
    // const userResult =await comment.findOne('`mid` IN ('+ itemId +') AND `pro`="'+ pro +'" AND `module`="'+ mod+'" AND `user_id`!='+ req.auth.myHID, req);
    const userResult =await viewUsers.findAll('`pro`="'+ pro +'" AND `mod`="'+ mod +'" AND itemId LIKE "%'+ itemId +'%" AND `comId`='+ req.auth.myComId +' AND `userId`!='+ req.auth.myHID, req);
    const users =[];
    if(authorId !=req.auth.myHID) users.push(authorId);
    for(let i= 0; i <userResult.length; i++)  {
        if(!users.includes(userResult[i].userId))  users.push(userResult[i].userId);
    }

    let notifyItemId =null;
    if(pro =='public' && mod =='gallery')  notifyItemId =itemId.split(',')[0];
    else notifyItemId =itemId;

    const notify ={
        users: users,
        itemId: notifyItemId,
        pro: pro,
        mod: mod,
        headText: 'сэтгэгдэл нэмлээ',
        title: commentText,
        content: commentText,
        url: 'main.php?p=cHiVbGlj&lisMod='+ encStr(mod) +'&lisId='+ itemId,
        hidden: commentResult[0].is_hidden,
        type: 'comment',
        actionKey: 'addComment',
        actionId: result.insertId
    };
    
    res.success.OK('Created', {
        comment: comments,
        notify: notify
    });
};

module.exports.update = async (req, res, next) => {
    const set ='`comment`="'+ req.body.comment +'"',
          where ='`id`='+ req.body.id;
    await comment.update(set, where, req);

    const commentResult =await comment.findOne('`id`='+ req.body.id, req);

    const comments =await getCommentInfo(commentResult, req);
    res.success.OK('success', {comment: comments});
};

module.exports.destroy = async (req, res, next) => {
    let isLastChild =false,
        commentIds =[],
        commentId = req.body.commentId.toString();

    while(!isLastChild)  {
        let result =await comment.findAll('`reply_id` IN ('+ commentId +')', req);
        let resultIds =[];
        for(let i =0; i<result.length; i++)  {
            resultIds.push(result[i].id);
        }

        let newIds =commentId.split(',');
        commentIds =commentIds.concat(newIds);
        commentId =resultIds.join();

        if(!result.length)  isLastChild =true;
    }

    commentId =commentIds.join();
    await comment.destroy('`id` IN ('+ commentId +')', req);
    await plus.destroy('`item_id` IN ('+ commentId +') AND `type`="comment"', req);
    let newCommentIds =[];
    for(let j =0; j <commentIds.length; j++)  {
        newCommentIds.push(parseInt(commentIds[j]));
    }
    await postByUserModel.updateMany({
        "lastAction.actionKey": 'addComment',
        "lastAction.actionItemId": { $in: newCommentIds }
    }, 
        { $set: {
            lastAction: null
        }
    });
    res.success.OK('success');
};
