/* eslint-disable no-unused-vars */
const Mongoose = require('mongoose');
const { ObjectId } = require('mongodb');
const { Op } = require('sequelize');
const { encStr } =require('../../../helpers/globalFunction');
const config = require('../../../config/config');

// responses

// models

// services
const post = require('../../../services/post');
const news = require('../../../services/news');
const forum = require('../../../services/forum');
const vote = require('../../../services/vote');
const gallery = require('../../../services/gallery');

// user errors
const userErrors = {
    EXIST: 'rules_exist',
};

module.exports.list = async (req, res, next) => {
    const { pro, mod, itemId} =req.query;
    let images =[];
    const result = await post.findOne("`itemId` LIKE '%"+ itemId +"%' AND `pro`='"+ pro +"' AND `module`='"+ mod +"'", req.auth.dbKey);
    if(pro =='public')  {
        const postData =result[0];
        let modData =null;
        if(mod == 'news')  modData =await news.findOne('`id` ='+ postData.itemId, req);
        if(mod == 'forum')  modData =await forum.findOne('`id` ='+ postData.itemId, req);
        if(mod == 'vote')  modData =await vote.findOne('`id` ='+ postData.itemId, req);
        if(mod == 'gallery')  modData =await gallery.findOne('`id` IN ('+ postData.itemId +')', req);

        for(let i =0; i <modData.length; i++)  {
            let image =modData[i].img;
            if(mod == 'gallery')  image =modData[i].large;
            const dataDir = encStr(req.auth.dbKey),
                  base64Str =encStr(`${pro}/${mod}/${image}`),
                  imagePath =`${config.storageHost}/main.php?g=bG9ZhEltYWdl&path=${base64Str}&accessToken=${req.auth.accessToken}&dataDir=${dataDir}`;

            let imageInfo ={
                itemId: modData[i].id,
                path: imagePath
            };

            if(mod == 'gallery')  {
                imageInfo['width'] =modData[i].l_width,
                imageInfo['height'] =modData[i].l_height
            }

            images.push(imageInfo);
        }
    }

    res.success.OK('slide.list', images);
};
