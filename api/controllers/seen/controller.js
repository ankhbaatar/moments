/* eslint-disable no-unused-vars */
const Mongoose = require('mongoose');
const { ObjectId } = require('mongodb');
const { Op } = require('sequelize');

const moment = require('moment');

// services
const newItem = require('../../../services/newItem');
const viewUsers = require('../../../services/viewUsers');
const mailByUser = require('../../../services/mailByUser');
const complainCardShift = require('../../../services/complainCardShift');
const receivedCardShift = require('../../../services/receivedCardShift');

// user errors
const userErrors = {
    EXIST: 'rules_exist',
};

module.exports.show = async (req, res, next) => {};

module.exports.list = async (req, res, next) => {
    res.success.OK('comment.list');
};

module.exports.create = async (req, res, next) => {
    const { itemId, pro, mod } =req.body;
    if(pro == 'public')  await newItem.destroy('`item_id` IN ('+ itemId +') AND `pro`="'+ pro +'" AND `mod`="'+ mod +'" AND `user_id`='+ req.auth.myHID, req);
    // else if(pro == 'mail')  await mailByUser.update('`is_new`=0, `date`='+ moment().unix(),'`mail_id`='+ itemId +' AND `user_id`='+ req.auth.myHID, req);
    else if(pro =='docs')  {
        const cardShiftTable =(mod =='complain' ? complainCardShift : receivedCardShift);
        await cardShiftTable.update('`date`='+ moment().unix(), '`doc_id`='+ itemId +' ORDER BY `id` DESC LIMIT 1', req); 
    }

    const viewInfo =await viewUsers.findOne('`userId`='+ req.auth.myHID +' AND `comId`='+ req.auth.myComId +' AND `pro`="'+ pro +'" AND `mod`="'+ mod +'" AND `itemId`="'+ itemId +'"', req);

    if(!viewInfo.length)  {
        const field ='`userId`, `comId`, `pro`, `mod`, `itemId`, `date`',
              value =req.auth.myHID +','+ req.auth.myComId +',"'+ pro +'","'+ mod +'","'+ itemId +'",'+ moment().unix();
        await viewUsers.insert(field, value, req);
    }

    const newItemCnt =await newItem.count('`pro`="'+ pro +'" AND `user_id`='+ req.auth.myHID, req);
    res.success.OK('Created', {newCnt: newItemCnt});
};

module.exports.update = async (req, res, next) => {
    res.success.OK('success');
};

module.exports.destroy = async (req, res, next) => {
    res.success.OK('success');
};
