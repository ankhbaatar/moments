// ---- Үүсгэх
/**
 * @swagger
 * /rules:
 *   post:
 *     tags: ["Rules"]
 *     summary: Шинэ бичлэг үүсгэх api
 *     security: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Review develop"
 *               path:
 *                 type: string
 *                 example: "roomIconPath"
 *               isActive:
 *                 type: integer
 *                 example: "1"
 *               users:
 *                 type: array
 *                 items:
 *                       type: object
 *                       properties:
 *                          userId:
 *                              type: string
 *                              example: "Review develop"
 *                          name:
 *                              type: string
 *                              example: "Review"
 *                          lastSeen:
 *                              type: integer
 *                              example: "1"
 *                          isOnline:
 *                              type: integer
 *                              example: "1"
 *               myHID:
 *                 type: string
 *                 example: "1006315"
 *               authorId:
 *                 type: string
 *                 example: "1006315"
 *     responses:
 *       422:
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  message:
 *                         type: string
 *                         description: Error message.
 *                         example: validation.error
 *                  errors:
 *                         type: array
 *                         description: Error message.
 *                         example: '{"password": "Invalid value"}'
 *                  code:
 *                         type: integer
 *                         description: Error code.
 *                         example: 422
 *                  status:
 *                         type: string
 *                         description: status.
 *                         example: error
 *       404:
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  message:
 *                         type: string
 *                         description: Error message.
 *                         example: user duplicated
 *                  code:
 *                         type: integer
 *                         description: Error code.
 *                         example: 404
 *                  status:
 *                         type: string
 *                         description: status.
 *                         example: error
 *       201:
 *         description: success
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  token:
 *                      type: string
 *                      example: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDA5NWUxYWRiYWJjZWNkNTRlZDRjNzUiLCJwaG9uZSI6Ijg2MDg4ODA2IiwidWFnZW50IjoiUG9zdG1hblJ1bnRpbWUvNy4yNi4xMCIsImlhdCI6MTYxMTM4MzYyMX0.7Fgyh14UbIlS9fmmlk22A97tmE9jSRojfSD9Jyp1Xwk
 *                  status:
 *                         type: string
 *                         description: status
 *                         example: success
 */