/* eslint-disable no-unused-vars */

module.exports.clearRedisUser = async (req, res, next) => {
    try {
        const { redis } =req.app.get('redis');
        const { userId } = req.query;
        const redisWorker = await redis.get(`_able_worker_i-${userId}`);
        if (redisWorker) {
            await redis.set(`_able_worker_i-${userId}`, null);
            await redis.set(`_user_i-${redisWorker.user_id}`, null);
            await redis.set(`_human_i-${redisWorker.user_id}`, null);
            await redis.set(`_user_status_i-${userId}`, null);
        }
        res.success.OK('redisCleanUser', userId);
    } catch (e) {
        console.log(e);
        return { error: e };
    }
};

module.exports.clearRedisUserNew = async (req, res, next) => {
    try {
        const { redis } =req.app.get('redis');
        const { userId } = req.query;
        const redisWorker = await redis.get(`_able_worker_i-${userId}`);
        if (redisWorker) {
            await redis.set(`_able_worker_i-${userId}`, null);
            await redis.set(`_user_i-${userId}`, null);
            await redis.set(`_human_i-${userId}`, null);
            await redis.set(`_user_status_i-${userId}`, null);
        }
        res.success.OK('redisCleanUser', userId);
    } catch (e) {
        console.log(e);
        return { error: e };
    }
};
