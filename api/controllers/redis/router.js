const Router = require('express').Router();
const { authentication } = require('../../middlewares');
const authorization =require('./authorizations');
// const validations = require('./validations.js');
const asyncRouteWrapper = require('../../../helpers/asyncRouteWrapper');
const controller = require('./controller.js');

Router.get.apply(Router, ['/clearRedisUser',
    authentication,
    asyncRouteWrapper(controller.clearRedisUser)]);

Router.get.apply(Router, ['/clearRedisUserNew',
    authentication,
    asyncRouteWrapper(controller.clearRedisUserNew)]);

module.exports = Router;
