const mysql = require('mysql2/promise');
const config = require('../config/config');

module.exports.connection = async (dbName) => {
    const conn = await mysql.createConnection({
        host: config.db.mysql.host,
        user: config.db.mysql.user,
        password: config.db.mysql.pass,
        waitForConnections: true,
        connectionLimit: 10,
        queueLimit: 0,
        database: dbName
    });
    return conn;
}
