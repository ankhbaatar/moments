const { htmlToText } = require('html-to-text');
const moment = require('moment');
const axios = require('axios');
const config = require('../config/config');

function showText(str) {
    str = str.replace(/-ΩΩ-/gi, '"');
    str = str.replace(/-Ω-/gi, ' ');
    str = str.replace(/-»-/gi, '"');
    str = str.replace(/-^-/gi, "'");
    str = str.replace(/-§-/gi, '&');
    str = str.replace(/-ŧ-/gi, '+');
    str = str.replace(/-±-/gi, '#');
    str = str.replace(/-≤-/gi, '&lt;');
    str = str.replace(/-≥-/gi, '&gt;');
    str = str.replace(/-≤≤-/gi, '<');
    str = str.replace(/-≥≥-/gi, '>');
    str = htmlToText(str);
    return str;
};

function showTextAnother(str) {
    str = str.replace(/-ΩΩ-/gi, '"');
    str = str.replace(/-Ω-/gi, ' ');
    str = str.replace(/-»-/gi, '"');
    str = str.replace(/-^-/gi, "'");
    str = str.replace(/-§-/gi, '&');
    str = str.replace(/-ŧ-/gi, '+');
    str = str.replace(/-±-/gi, '#');
    str = str.replace(/-≤-/gi, '&lt;');
    str = str.replace(/-≥-/gi, '&gt;');
    str = str.replace(/-≤≤-/gi, '<');
    str = str.replace(/-≥≥-/gi, '>');
    return str;
};

function getAgeFromRegister(value) {
    const first = value.substring(2, 10);
    let year = parseInt(first.substring(0, 2), 10);
    let month = parseInt(first.substring(2, 4), 10);
    const day = parseInt(first.substring(4, 6), 10);

    if (month > 20) {
        month -= 20;
        year += 2000;
    } else {
        year += 1900;
    }
    const date = new Date();
    date.setFullYear(year);
    date.setMonth(month - 1);
    date.setDate(day);
    const ageDifMs = Date.now() - Date.parse(date);
    const ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

function replaceAt(str, index, ch) {
    return str.replace(/./g, (c, i) => (i === index ? ch : c));
};

function encStr(Str) {
    const value = new Buffer.from(Str);

    const value1 = value.toString('base64');
    const len = value1.length;
    const a = Math.ceil(len / 3);
    const b = Math.ceil(len / 4);
    const cut1 = value1.substr(a, 1);
    const cut2 = value1.substr(b, 1);

    const paste1 = replaceAt(value1, b, cut1);
    const paste2 = replaceAt(paste1, a, cut2);
    return paste2;
};

function mkEasyDate(myDate)  {  //Өгөгдсөн unixtime -ийг үгээр илэрхийлж буцаана
    let now =moment().unix(),
        diffTime =now -myDate,
        myDateFormat =moment(myDate*1000),
        result =myDateFormat.format('MM/DD') +' '+ myDateFormat.format('HH:mm');
    
    if(diffTime <60)  result ='Дөнгөж сая';  else
    if(diffTime <3600)  result =Math.floor(diffTime/60) +' мин';  else
    if(diffTime <86400)  {
        let hourCnt =Math.floor(diffTime/3600);
        result =(hourCnt >myDateFormat.hour()) ? 'Өчигдөр' : hourCnt +' цаг';
    }else
    if(diffTime <7*86400)  {
        let year =myDateFormat.year(),
            month =myDateFormat.month(),
            day =myDateFormat.date(),
            dayFirstSecond =moment(new Date(year, month, day, 0, 0, 1)).unix(),
            dayCnt =Math.floor((now -dayFirstSecond)/86400);
        result =(dayCnt ==1) ? 'Өчигдөр '+ myDateFormat.format('HH:mm') : (dayCnt ==2 ? 'Уржигдар '+ myDateFormat.format('HH:mm') : dayCnt +' өдөр');
    };
    return result;
};

async function getGroupName(groupId, dbKey, result =[])  {
    const group =require('../services/group');

    const groupRow =await group.findOne('`id`='+ groupId, dbKey);
    result.push(groupRow[0].name);
    // if(groupRow[0].mid)  await getGroupName(groupRow[0].mid, dbKey, result);
    // else return result;
    return result;
};

async function getSpecialComment(commentId, req, result =null) {  /*Заагдсан сэтгэгдлийн замыг олж буцаах рекурсив функц. Өгөгдсөн сэтгэгдэлийн Root эцэг хүртэл ажилна*/
    const { getCommentInfo } = require('../services/getCommentInfo');
    const comment = require('../services/comment');

    return new Promise(async (resolve, reject) => {
        const commentInfo =await comment.findOne('`id`='+ commentId, req),
              replyInfo =await getCommentInfo(commentInfo, req),
              upReplyCnt =await comment.count('`mid`='+ commentInfo[0].mid +' AND `reply_id`='+ commentInfo[0].reply_id +' AND `id` > '+ commentInfo[0].id +' AND `pro`="'+ commentInfo[0].pro +'" AND `module`="'+ commentInfo[0].module +'"', req);

        if(result)  {  //Анхны сэтгэгдэл биш бол
            replyInfo[0]['replies'] =result.replies;
            replyInfo[0]['upReplyCnt'] =result.upReplyCnt;
        };

        result ={
            replies: replyInfo,
            upReplyCnt: upReplyCnt
        };

        if(commentInfo[0].reply_id)  {
            resolve(await getSpecialComment(commentInfo[0].reply_id, req, result));
        }
        else resolve(result);
    });
};

module.exports = {
    showText,
    encStr,
    mkEasyDate,
    getGroupName,
    getSpecialComment,
    getAgeFromRegister,
	showTextAnother
};
