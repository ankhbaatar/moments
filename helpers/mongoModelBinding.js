const { forOwn } = require('lodash');
const models = require('../models/mongo');

module.exports = (req, res, next) => {
    let isModelBinded = false;

    forOwn(req.params, (value, key) => {
        if (!models[`${key}s`]) {
            return;
        }
        isModelBinded = true;
        const populate = req.query.populates ? JSON.parse(req.query.populates) : '';
        models[`${key}s`].model
            .findById(value)
            .populate(populate)
            .then(result => {
                req[key] = result;
                next();
            })
            .catch(err => {
                // eslint-disable-next-line no-console
                console.error(err);
                res.error.NotFound('not_found.error');
            });
    });

    if (!isModelBinded) {
        next();
    }
};
