/**
 * https://www.npmjs.com/package/express-response-handler
 *
 * example:
 *    res.success.OK('message', data);
 *    res.success.Created('message', data);
 *    res.error.NotFound('not_found.error', error);
 *    res.error.UnprocessableEntity('validation.error', error);
 */

module.exports = [
    ['Unauthorized', 'error', 401],
    ['Auth', 'error', 403],
    ['User', 'error', 404],
    ['Validate', 'error', 422],
    ['ServerError', 'error', 500],
    ['OK', 'success', 200],
    ['Created', 'success', 201],
];
