const swaggerDefinition = {
    openapi: '3.0.0',
    info: {
        title: 'REVIEW project api document',
        version: '1.0.0',
        description: '',
        license: {
            name: 'ABLESOFT',
            url: '',
        },
        contact: {
            name: 'REVIEW TEAM',
            url: '',
        },
    },
    components: {
        securitySchemes: {
            bearerAuth: {
                type: 'http',
                scheme: 'bearer',
                bearerFormat: 'JWT',
            },
        },
    },
    security: {
        '- bearerAuth': '[]',
    },
    tags: [
        {
            name: 'Rules',
            description: 'Rules endPoint',
        },
    ],
    consumes: ['application/json'],
    produces: ['application/json'],
    servers: [
        {
            url: 'http://localhost:5000/api',
            description: 'Development server',
        },
        {
            url: 'http://183.81.168.104:8002/api',
            description: 'Staging server',
        },
    ],
};

const options = {
    swaggerDefinition,
    apis: ['api/controllers/rules/_docs.js', 'api/controllers/manage/_docs.js'],
};

module.exports = options;
