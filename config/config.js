require('dotenv').config();

const config = {
    app: {
        port: parseInt(process.env.APP_PORT, 10) || 5000,
    },
    user: {
        // authkey: process.env.AUTH_KEY || 'bytelinks!@#',
    },
    db: {
        mysql: {
            host: process.env.DB_HOST || 'develop.able.mn',
            port: parseInt(process.env.DB_PORT, 10) || 3306,
            name: process.env.DB_NAME || '',
            user: process.env.DB_USER || 'root',
            pass: process.env.DB_PASS || 'AbleSoft2018',
        },
        mongo: {
            host: process.env.MONGO_HOST,
            user: process.env.MONGO_USER,
            pass: process.env.MONGO_PASS,
        },
        redis: {
            host: process.env.REDIS_HOST,
            port: 6379,
        },
    },
    storageHost: process.env.STORAGE_HOST,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    dialect: 'mysql',
};

module.exports = config;
