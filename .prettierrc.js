module.exports = {
    semi: true,
    tabWidth: 4,
    jsxBracketSameLine: true,
    singleQuote: true,
    trailingComma: 'es5', // all
    endOfLine: 'auto',
    printWidth: 100,
    bracketSpacing: true,
    arrowParens: 'avoid'
};